<<<<<<< HEAD

// REAL TIME IMPLEMENTATION
//#include <pthread.h>
//#include <unistd.h>
#include <iostream>
#include <windows.h>
#include <fstream>
#include <math.h>
#include "model.h"
//#include "SOOP.h"
#include "testerART.h"

using namespace std;
/*
void  * soopCalc(void * argument);
void  * modelControl(void * argument);
*/
//======== SOLVER PARAMETERS ========
double gamma = 0.9;                                // Discount factor
double L_lp = 1.11;									// Lipschitz constant
double alpha = 1.0 / L_lp;                                 // precision of a dimention smtg like that :P
unsigned int budget = 10000;                          // Number of actions calculated in advanced
//======== MODEL PARAMETERS ========
double qReward = 1.0;
double qRewardd = 0.005;
double rReward = 0.05;
double qRewardTheta = 0.05;
double qRewarddTheta = 0.005;
double maxVoltage = 14.0;							// maximum volatege on motor (SRV0 can be 15[V])
unsigned int eulerStep = 10;                        // # of steps per sample time
unsigned int K= 1; //original nr 5;					//Number of actions that can be applied in a stateE
double timeStep = 0.05;
unsigned int nbHiddenstateEVariables = 4;			// # of hidden stateE variables
unsigned int nbHiddenActionVariables = 1;			// # of hidden action variables
//======== REAL APP PARAMETERS ========
unsigned int Ulenght = 1;							// commnad sequence lenght - sent while 1 sampling time
DWORD sleepTime = 50;								// sending command with x milliseconds period - threading
#define DEBUG_TIME

int main( void )
{
    /* // threading section
    pthread_t t1, t2 ; // declare 2 threads.
    pthread_create( &t1, NULL, function1,NULL); // create a thread running function1
    pthread_create( &t2, NULL, function2,NULL); // create a thread running function2
    Sleep(1);
    */

    ofstream myfile;
    myfile.open ("modelTest.txt");
    stateE current_state; // = new stateE;
    initstateE(current_state);
    current_state.variables[0] = 0.0;
   // cout <<  "program init" << endl;
    // ==============Model testing

//    if (!current_state) cout<< "empty"<< endl;
  //  else cout<< "NOT empty"<< endl;
    current_state.variables[0] = -3.0*PI/2.0;
    cout <<  "program init" << endl;
   // ==============Model testing
    myfile << current_state.variables[0] <<", "<< current_state.variables[2] <<", "<<current_state.reward << ", "<< fmod((current_state.variables[0])+ PI, 2.0 * PI) -PI <<endl;
    double utmp = -10.0;
    for (int i=0;i<3;i++)
    {
        current_state.variables[0] = -3.0*PI/2.0;
        copyStateE(current_state, nextstateE(current_state, utmp));
        utmp = utmp+10.0;
        myfile << current_state.variables[0] <<", "<< current_state.variables[2] <<", "<<current_state.reward << ", "<< fmod((current_state.variables[0])+ PI, 2.0 * PI) -PI <<endl;


    }
	myfile.close();

    // ==============SOOP testing
	myfile.open("sopcTest.txt");
    initstateE(current_state);
    current_state.variables[0] = -3.0*PI/2.0;

#ifdef DEBUG_TIME
	double timeBoxSelect = 0.0, timeDimSelect = 0.0, timeSplit = 0.0, maxDuration = 0.0; 
	int nrTimeSamples = 0;
	__int64 frequencyBS, startBS, endtBS, totalBS;
	QueryPerformanceFrequency((LARGE_INTEGER *)&frequencyBS);
#endif

    double commnadSignal[initlen];
    for(int i=0; i<100; i++)
    {
        //measure current state
        cout<< "in loop" << i <<endl;
        myfile << current_state.variables[0] <<", "<< current_state.variables[2] <<", "<<current_state.reward<<", " << commnadSignal[0] <<endl;
#ifdef DEBUG_TIME	
		nrTimeSamples++;
		QueryPerformanceCounter((LARGE_INTEGER *)&startBS);
#endif
        memcpy(commnadSignal,sopc(current_state),sizeof(double)*initlen);

#ifdef DEBUG_TIME
		QueryPerformanceCounter((LARGE_INTEGER *)&endtBS);
		timeBoxSelect = timeBoxSelect + (endtBS - startBS) * 1000.0 / frequencyBS;
		if (maxDuration < (endtBS - startBS) * 1000.0 / frequencyBS)
			maxDuration = (endtBS - startBS) * 1000.0 / frequencyBS;
#endif

        copyStateE(current_state,nextstateE(current_state, commnadSignal[0]));
    }

    cout <<  "program out of loop" << endl;
    myfile.close();

#ifdef DEBUG_TIME
	cout << "Duration for U_sequence = " << timeBoxSelect / nrTimeSamples << " ms  max duration = "<< maxDuration  <<endl<<" for budget = " << budget << " loop nr = " << nrTimeSamples << endl;
#endif
    return 0;
}
/*
void * function1(void * argument)
{
    cout << " hello " << endl ;
    Sleep(2); // fall alseep here for 2 seconds...
    return 0;
}

void * function2(void * argument)
{
    cout << " world "  << endl ;
    return 0;
}

*/
=======

// REAL TIME IMPLEMENTATION
//#include <pthread.h>
//#include <unistd.h>
#include <iostream>
#include <windows.h>
#include <fstream>
#include <math.h>
#include "model.h"
//#include "SOOP.h"
#include "testerART.h"

using namespace std;
/*
void  * soopCalc(void * argument);
void  * modelControl(void * argument);
*/
//======== SOLVER PARAMETERS ========
double gamma = 0.9;                                // Discount factor
double L_lp = 1.11;									// Lipschitz constant
double alpha = 1.0 / L_lp;                                 // precision of a dimention smtg like that :P
unsigned int budget = 2000;                          // Number of actions calculated in advanced
//======== MODEL PARAMETERS ========
double qReward = 1.0;
double rReward = 0.03;
double qRewardTheta = 0.07;
double qRewarddTheta = 0.000;
double maxVoltage = 15.0;							// maximum volatege on motor (SRV0 can be 15[V])
unsigned int eulerStep = 10;                        // # of steps per sample time
unsigned int K= 1; //original nr 5;					//Number of actions that can be applied in a stateE
double timeStep = 0.05;
unsigned int nbHiddenstateEVariables = 4;			// # of hidden stateE variables
unsigned int nbHiddenActionVariables = 1;			// # of hidden action variables
//======== REAL APP PARAMETERS ========
unsigned int Ulenght = 1;							// commnad sequence lenght - sent while 1 sampling time
DWORD sleepTime = 50;								// sending command with x milliseconds period - threading
#define DEBUG_TIME

int main( void )
{
    /* // threading section
    pthread_t t1, t2 ; // declare 2 threads.
    pthread_create( &t1, NULL, function1,NULL); // create a thread running function1
    pthread_create( &t2, NULL, function2,NULL); // create a thread running function2
    Sleep(1);
    */

    ofstream myfile;
    myfile.open ("modelTest.txt");
    stateE current_state; // = new stateE;
    initstateE(current_state);
    current_state.variables[0] = 0.0;
   // cout <<  "program init" << endl;
    // ==============Model testing

//    if (!current_state) cout<< "empty"<< endl;
  //  else cout<< "NOT empty"<< endl;
    current_state.variables[0] = -3.0*PI/2.0;
    cout <<  "program init" << endl;
   // ==============Model testing
    myfile << current_state.variables[0] <<", "<< current_state.variables[2] <<", "<<current_state.reward << ", "<< fmod((current_state.variables[0])+ PI, 2.0 * PI) -PI <<endl;
    double utmp = -10.0;
    for (int i=0;i<3;i++)
    {
        current_state.variables[0] = -3.0*PI/2.0;
        copyStateE(current_state, nextstateE(current_state, utmp));
        utmp = utmp+10.0;
        myfile << current_state.variables[0] <<", "<< current_state.variables[2] <<", "<<current_state.reward << ", "<< fmod((current_state.variables[0])+ PI, 2.0 * PI) -PI <<endl;


    }
	myfile.close();

    // ==============SOOP testing
	myfile.open("sopcTest.txt");
    initstateE(current_state);
    current_state.variables[0] = -3.0*PI/2.0;

#ifdef DEBUG_TIME
	double timeBoxSelect = 0.0, timeDimSelect = 0.0, timeSplit = 0.0;
	int nrTimeSamples = 0;
	__int64 frequencyBS, startBS, endtBS, totalBS;
	QueryPerformanceFrequency((LARGE_INTEGER *)&frequencyBS);
#endif

    double commnadSignal[initlen];
    for(int i=0; i<100; i++)
    {
        //measure current state
        cout<< "in loop" << i <<endl;
        myfile << current_state.variables[0] <<", "<< current_state.variables[2] <<", "<<current_state.reward<<", " << commnadSignal[0] <<endl;
#ifdef DEBUG_TIME	
		nrTimeSamples++;
		QueryPerformanceCounter((LARGE_INTEGER *)&startBS);
#endif
        memcpy(commnadSignal,sopc(current_state),sizeof(double)*initlen);

#ifdef DEBUG_TIME
		QueryPerformanceCounter((LARGE_INTEGER *)&endtBS);
		timeBoxSelect = timeBoxSelect + (endtBS - startBS) * 1000.0 / frequencyBS;
		//cout << "sopc solve time =  " << timeBoxSelect << endl;
#endif

        copyStateE(current_state,nextstateE(current_state, commnadSignal[0]));
    }

    cout <<  "program out of loop" << endl;
    myfile.close();

#ifdef DEBUG_TIME
	cout << "Duration for U_sequence = " << timeBoxSelect / nrTimeSamples << " ms for budget = " << budget << " loop nr = " << nrTimeSamples << endl;
#endif
    return 0;
}
/*
void * function1(void * argument)
{
    cout << " hello " << endl ;
    Sleep(2); // fall alseep here for 2 seconds...
    return 0;
}

void * function2(void * argument)
{
    cout << " world "  << endl ;
    return 0;
}

*/
>>>>>>> 732637a9a341cf88aaa5fd50228eb4cf9e105093
