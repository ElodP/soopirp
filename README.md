# README #

This repository contains the implementation of 3 algorithms: SOOP, OPC, and SOPC
Also recently added the OPD and OASP algorithm by Koppany Mate. The reference paper for this algorithms is not yet published.
These controllers are applied on a rotary inverted pendulum both in simulation and in real experiments.

**Ref to:** Lucian Busoniu, Elod P�ll, Remi Munos, "Discounted near-optimal control of general nonlinear systems using optimistic planning",  American Control Conference (ACC-16) 2016


### What is this repository for? ###

Details about the project can be found [here](https://sites.google.com/site/timecontroll/home/inverted-rotary-pendulum).
The hardware for the real experiments is a [Quanser Rotary Inverted Pendulum](http://www.quanser.com/products/rotary_pendulum).

The OPC algorithm is going to be presented at the ACC16 conference, the link to the document will be made available here too.  

The software requirements are: 

* OS - Windows 7
* Visual Studio 12 or greater (VS13 was used for **RIPsimulator** project) 
* pthread library
* [Quanser C API](http://www.quanser.com/Products/quarc/documentation/hardware_reference_c.html)

### License ###

This project is released under BSD license, see the License file.

### Contact ###

Elod Pall

[website](https://www.elodpall.de)