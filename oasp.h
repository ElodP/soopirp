#pragma once

#ifndef OASP_H
#define OASP_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "testerART.h"

const unsigned short M = 3;		//--number of children, i.e. distinct actions in action space

extern double gamma;// = 0.95;                     //Discount factor
extern double beta;// = 0.95;                     //OASP tuning factor
extern unsigned short S;						//--number switches allowed initially
extern unsigned int budget;// = 20;                //Number of actions calculated in advanced

//const unsigned short initlen = 50;//--length of returned action sequence

struct node{
	stateE X_and_r;			//--includes state X and reward r
	struct node* parent;		    //--pointer to parent node
	struct node* children[M];	    //--pointers to children, filled up only if isLeaf below is false, i.e. node is expanded
	unsigned short u;					//--the index of action that lead to this state
	unsigned short	depth;				//--the depth reached in the tree
	bool isLeaf; 			//--means it was not yet expanded, i.e. has no children
	double R;				//--the nuValue, i.e. partial return, i.e. cumulative rewards obtained until this node
	double bVal;			//--the bValue, calculated based on the depth of the node and the discount factor
	unsigned short S;					//--the number of action switches consumed until this node
};

node* initMyNode( stateE& X_and_r, node* parent, unsigned short u, unsigned short depth, bool isLeaf, double R, unsigned short S = 0 );

void findNodeWithMaxB( node* currNode, bool searchSpace );

void findMaxR( node* currNode );

void findMaxRleaves( node* currNode );

void findMaxDepth( node* currNode );

double* oasp(stateE& stateE0);

#endif
