//////////////////////////////////////////////////////////////////
//
//			INVERTED ROTARY PENDUL experiment
//
//
//
//////////////////////////////////////////////////////////////////

#include "testerART.h"



// CRLT-C var
static int stop = 0;

///////////////////////
// SOOP related data //
///////////////////////
unsigned int nbHiddenstateEVariables = 4;			// # of hidden stateE variables
unsigned int nbHiddenActionVariables = 1;			// # of hidden action variables
double maxVoltage = 9.0;							// maximum volatege on motor (SRV0 can be 15[V])
unsigned int eulerStep = 10;                        // # of steps per sample time
double gamma = 0.9;                                // Discount factor
double L_lp = 1.11;									// Lipschitz constant
double alpha = 1.0/L_lp;                                 // precision of a dimention smtg like that :P
unsigned int budget = 550;                          // Number of actions calculated in advanced
double qReward= 1.0;
double rReward= 0.03;
double qRewardTheta = 0.07;
double qRewarddTheta = 0.000;
unsigned int Ulenght = 1;							// commnad sequence lenght
DWORD sleepTime = 50;								// sending command with x milliseconds period





// THREADING vars
double * Ucmd;
stateE currentState;
pthread_barrier_t barrierBegni, barrierEnd;
double timeStep = sleepTime/1000.0;

// HARDWARE vars
t_card board;
int encoderResolution = 4096;

#define DEBUG_//TIME_D
#define DEBUG_//TIME
#define DEBUG_//CONTROL

void getCurrentState(t_int32 base, t_int32 lastBase, t_int32 pendul, t_int32 lastPendul, t_double frequency);

/* /////////////////////////////
 // Threads for DATA LOGGING //
///////////////////////////// */

void * dataLoggerEncoder(void *)
{

	std::ofstream myfile;
    myfile.open("data.txt");

#ifdef DEBUG_TIME_D
	__int64 frequencyTMP, startTMP, endTMP = 0;
	double elapsedTime = 0.0, max_time = 0.0;	
	QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyTMP); 	
	int j = 0, max_j=0;	
#endif //DEBUG_TIME

	while (stop == 0)
	{
		pthread_barrier_wait(&barrierBegni);
#ifdef DEBUG_TIME_D
		j++;
		QueryPerformanceCounter ( (LARGE_INTEGER *)&startTMP);			
#endif //DEBUG_TIME
		myfile << currentState.variables[0] << ", "<< currentState.variables[1]<< ", "<<currentState.variables[2] <<", "<<currentState.variables[3] << std::endl;
#ifdef DEBUG_TIME_D
		QueryPerformanceCounter ( (LARGE_INTEGER *)&endTMP);	 
		elapsedTime =elapsedTime + (endTMP - startTMP) * 1000.0 / frequencyTMP;	
		if (max_time < (endTMP - startTMP) * 1000.0 / frequencyTMP)
		{
			max_time = (endTMP - startTMP) * 1000.0 / frequencyTMP;
			printf("%f %d \n", max_time, j);
		}
#endif //DEBUG_TIME
	}

	myfile.close();

#ifdef DEBUG_TIME_D
	printf("D db = %d, AVRtime = %f. MAXtime = %f \n", j, elapsedTime/j, max_time);
#endif //DEBUG_TIME
	printf("\n endD \n");
	return (NULL);
}


/* /////////////////////////////
	// Threads for write //
///////////////////////////// */

void *sendingCMD(void * )
{
	printf("inS\n");
	double * U_tmp;
	U_tmp = (double*)malloc(sizeof(double)*Ulenght);
	t_uint32 channels[] = { 0 };
	t_error result;
	static char message[512];
	
	t_uint32 samples_to_read   = 2;
	t_uint32 encoders[]       = { 0, 1, };	
	t_double frequency         = 40;	
	t_uint32 samples_in_buffer = (sleepTime*Ulenght)/(t_uint32)((1/frequency)*1000) + 10;
	t_uint32 samples           = -1;
	
	t_task taskToRead;
	t_int32  counts[ARRAY_LENGTH(encoders)*4];

	hil_task_create_encoder_reader(board, samples_in_buffer, encoders, ARRAY_LENGTH(encoders), &taskToRead);

	t_int samples_read;

#ifdef DEBUG_TIME
	__int64 frequencyTMP, startTMP, endTMP = 0;
	double elapsedTime = 0.0;	
	QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyTMP); 	
	QueryPerformanceCounter ( (LARGE_INTEGER *)&startTMP);	
	int j = 0;
#endif //DEBUG_TIME	
	

	// MAIN OF THREAD //////////////////////
	while (stop == 0)
	{
		
		//get U		
		memcpy(U_tmp,Ucmd,sizeof(double)*Ulenght);
		//sync with second task
		pthread_barrier_wait(&barrierBegni);

#ifdef 	DEBUG_TIME	
		j++;
		QueryPerformanceCounter ( (LARGE_INTEGER *)&startTMP);			
#endif // DEBUG_TIME	

		// satrt reading encoders
		hil_task_start(taskToRead, SYSTEM_CLOCK_1, frequency, samples);	
				
		for (unsigned int i = 0; i<Ulenght; i++)
		{	
			//send CMD signal			
#ifndef DEBUG_TIME
			result = hil_write_analog(board, channels, ARRAY_LENGTH(channels), &(t_double)U_tmp[i] );
#else
			result = 0;
#endif
			if (result !=0 )
			{
				msg_get_error_message(NULL, result, message, ARRAY_LENGTH(message));
				printf("Unable to write analog channels. %s Error %lu.\n", message, -result);
			}

#ifdef  DEBUG_CONTROL
			else
			{
				printf(" %f", U_tmp[i]);
				if (i == Ulenght -1) printf("\n");
			}
#endif //DEBUG_CONTROL

			Sleep(sleepTime);			// miliseconds 
		}
		samples_read = hil_task_read_encoder(taskToRead, samples_to_read, counts);
		if (samples_read < 0)
        {
                    msg_get_error_message(NULL, samples_read, message, ARRAY_LENGTH(message));
                    printf("Unable to read channels. %s Error %lu.\n", message, -samples_read);
        }

		//printf("raw: %d, %d, %d, %d \n",counts[0],counts[1],counts[2],counts[3]);
        getCurrentState(counts[0],counts[2],counts[1],counts[3],frequency);        
		
		//sync with second task
		pthread_barrier_wait(&barrierEnd);
		
#ifdef 	DEBUG_TIME	
		QueryPerformanceCounter ( (LARGE_INTEGER *)&endTMP);	
		elapsedTime =elapsedTime + (endTMP - startTMP) * 1000.0 / frequencyTMP;
#endif // DEBUG_TIME
		hil_task_stop(taskToRead);
	}

	// sending stop signal to motor	
	U_tmp[0] = 0;
	result = hil_write_analog(board, channels, ARRAY_LENGTH(channels), &(t_double)U_tmp[0] );
	

#ifdef DEBUG_TIME	
	//printf("S db = %d, time = %f. \n", j, elapsedTime/j);
#endif //DEBUG_TIME

	printf("Task S terminated. \n");
	return (NULL);
}

/* /////////////////////////////
	// Threads for read //
///////////////////////////// */
void *computingU(void *)
{
	double * U_tmp;
	U_tmp = (double*)malloc(sizeof(double)*Ulenght);
	stateE state;
	initstateE(state);
	printf("inU \n");

#ifdef DEBUG_TIME
	__int64 frequencyTMP, startTMP, endTMP = 0;
	double elapsedTime = 0.0, max_time = 0.0;	
	QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyTMP); 	
	int j = 0, max_j=0;	
#endif //DEBUG_TIME

	bool fisrtLoop = true;	
	memcpy(Ucmd,soop(state),sizeof(double)*Ulenght);

	// MAIN OF THREAD //////////////////////
	while (stop == 0)
	{		
		
		// get	current state		
		copyStateE(state, currentState);
		//printf("a= %f, ad= %f, t= %f, td= %f \n", state.variables[0],state.variables[1],state.variables[2],state.variables[3]);
		pthread_barrier_wait(&barrierBegni);
		
	
#ifdef DEBUG_TIME
		j++;
		QueryPerformanceCounter ( (LARGE_INTEGER *)&startTMP);			
#endif //DEBUG_TIME
			
		// compute future state
		for (unsigned int i=0;i<Ulenght;i++)
		{
			//printf("in tC simulation i= %d ui = %f \n",i,  U_tmp[i]);
			copyStateE(state,nextstateE(state, Ucmd[i]));
		}
		// compute U for future state
		memcpy(Ucmd,soop_soo(state),sizeof(double)*Ulenght);

#ifdef DEBUG_TIME
		QueryPerformanceCounter ( (LARGE_INTEGER *)&endTMP);	 
		elapsedTime =elapsedTime + (endTMP - startTMP) * 1000.0 / frequencyTMP;	
		if (max_time < (endTMP - startTMP) * 1000.0 / frequencyTMP)
		{
			max_time = (endTMP - startTMP) * 1000.0 / frequencyTMP;
			printf("%f %d \n", max_time, j);
		}
#endif //DEBUG_TIME
		pthread_barrier_wait(&barrierEnd);
	
	}

#ifdef DEBUG_TIME
	printf("U db = %d, AVRtime = %f. MAXtime = %f \n", j, elapsedTime/j, max_time);
#endif //DEBUG_TIME

		printf(" Task U terminated ");
	return (NULL);
}


void signal_handler(int signal)
{
	stop = 1;
}

double encoderCountToRad(int reading)
{
	int position = (reading%encoderResolution);	

	if (position>=0) return ((double)position/(double)encoderResolution)*2*PI-PI;
	else return ((double)position/(double)encoderResolution)*2.0*PI+PI;
	//return  (double)position/(double)encoderResolution;
}

void getCurrentState(t_int32 base, t_int32 lastBase, t_int32 pendul, t_int32 lastPendul, t_double frequency)
{
	static char message[512];
//	t_int result;
	t_int32 values[2] = { 0, 0 };
	t_uint32 encoder_channel[]  = {0, 1};

	stateE state; 
	initstateE(state);
	double elapsedTime = 1.0/(double) frequency;

	//printf("%d, %d, %d, %d \n", base, lastBase, pendul, lastPendul);

	//if (result >= 0)
	{	
		state.variables[0]=-encoderCountToRad(pendul);
		state.variables[1]=-((double)(lastPendul- pendul)*2.0*PI/(double)encoderResolution)/elapsedTime;
		state.variables[2]=-encoderCountToRad(base);
		state.variables[3]=-((double)(lastBase- base)*2.0*PI/(double)encoderResolution)/elapsedTime;
	}
	
	copyStateE(currentState,state);
	//return current_state;
}



int main(int argc, char* argv[])
{
	// Q4 aquisition board setup
	static const char board_type[]       = "q4";
	static const char board_identifier[] = "0";
	static char       message[512];

	qsigaction_t action;	
	t_int  result;

	initstateE(currentState);

	/* Catch Ctrl+C so application may be shut down cleanly */
	action.sa_handler = signal_handler;
	action.sa_flags   = 0;
	qsigemptyset(&action.sa_mask);
	qsigaction(SIGINT, &action, NULL);

#ifndef DEBUG_TIME
	Sleep(3000);
	printf("Cables in hend!!! \n\n");
#endif

	

	result = hil_open(board_type, board_identifier, &board);
	if (result == 0)
	{
		printf("\n\n Press CTRL-C to stop the algorithm.\n\n");
		// input output communication chanel setup
		const t_uint32   encoder_channel[]	  = {0,1};
#define NUM_ENCODER_CHANNELS    ARRAY_LENGTH(encoder_channel)
		t_int32	  counts[NUM_ENCODER_CHANNELS]  = { 0, 0 };

		//initializing output buffer to 0[V]
		Ucmd= (double*)malloc(sizeof(double)*initlen);
		for (unsigned int i=0;i<Ulenght;i++) 
			Ucmd[i] = 0;

		// initializating encoders to 0
		t_error result = hil_set_encoder_counts(board, encoder_channel, NUM_ENCODER_CHANNELS, counts);

		//init threads 
		int rc1, rc2, rc3;	
		pthread_barrier_init(&barrierBegni, NULL, 3);
        pthread_barrier_init(&barrierEnd, NULL, 2);
		pthread_t threadU, threadS, threadD;
		/* Create independent threads each of which will execute functionC */
		if( (rc1=pthread_create( &threadU, NULL, &computingU, NULL)) )   {
			printf("ThreadU creation failed: %d\n", rc1);
		}

		if( (rc2=pthread_create( &threadS, NULL, &sendingCMD, NULL)) )
		{
			printf("ThreadS creation failed: %d\n", rc2);
		}
		
		if( (rc3=pthread_create( &threadD, NULL, &dataLoggerEncoder, NULL)) )
		{
			printf("ThreadS creation failed: %d\n", rc3);
		}
		

		while (stop == 0);
		
		printf("Main terminated, closing board in 50ms. \n");
		Sleep(5000);
		hil_close(board);
	}
	else
	{
		msg_get_error_message(NULL, result, message, ARRAY_LENGTH(message));
		printf("Unable to open board. %s Error %lu.\n", message, -result);
	}
	getchar(); /* absorb Ctrl+C */
	
	return 0;
}


