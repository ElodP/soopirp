#ifndef MODEL_H
#define MODEL_H

// NOTE: comment line below to compile as controller, uncomment to compile as Te measurement
//#define MEASURE_Te

/* Represent a stateE of the model */
extern double qReward;// = 1.0;
extern double qRewardd;
extern double qRewarddTheta;
extern double qRewardTheta;
extern double rReward;// = 1.0;
extern double maxVoltage;// = 3.0;
extern double veloSaturation;	// max angular velos used for saturating states /*Koppany*/
extern unsigned int eulerStep;// = 10;                        // # of steps per sample time
extern unsigned int K ;//= 1; //original nr 5;				//Number of actions that can be applied in a stateE
extern double timeStep ;//= 0.05;
extern unsigned int nbHiddenstateEVariables;			//Number of hidden stateE variables
extern unsigned int nbHiddenActionVariables;		//Number of hidden action variables
const int stateVariablesNumber = nbHiddenActionVariables;

const double PI = 3.1415926535897931;


//+++++++++++++ this must be changed to be generic not 4.
//++++++++++++++
typedef struct {
		double variables[4];                        //State variables 4.
		double reward;
		unsigned int isTerminal;
}	stateE;
//+++++++++++++
//++++++++++++++



extern unsigned int K;								//Number of actions that can be applied in a stateE
extern double timeStep;								//Time step between two stateE


//extern action* actions;								//Array of action

extern double* parameters;							//Model's parameters
extern unsigned int nbParameters;					//Number of model's parameters


/* Returns an allocated initial stateE of the model. */
void initstateE(stateE& s);

/* Returns a triplet containing the next stateE, the applied action and the reward given the current stateE and action. */
stateE nextstateE(stateE s, double votlageSignal);
//double reward(stateE* s, action* a);

void copyStateE(stateE& destination, stateE original);

double normalizeAngle(double a);

#endif
