<<<<<<< HEAD
//#include "stdafx.h"
#include "testerART.h"

//#ifndef DEBUG_H
#define DEBUG_//TIME
#define DEBUG_//TO_EXTAND
#define DEBUG_//EXTANDED
#define DEBUG_//DIM_SELECT
#define DEBUG_//SPLIT
#define DEBUG_//COMMAND
#define DEBUG_//ALGORITHM

using namespace std;

unsigned int initsize = (unsigned int)floor(1.1 * budget + 0.5);           //Rough upper bound on #seq
//discount factor for dimselect='disc'

typedef struct {
	double startPoint [initlen];       //starting points of U domains, for each depth and sequence
	unsigned int splits [initlen];     //number of splits in each sequences element
	stateE trajectory [initlen];       // stateE trajectory for center action samples: dimx X and # of sequences also,
	unsigned int sequenceLenght ;//= 0;                                   // rewards RESULTING from center action samples: # of sequences
	//double reward [initlen];         // rewards RESULTING from center action samples: # of sequences
	double rewardSum;                  // the sum of rewards in the sequence
	int splitsSum;						// the sum of splits denoted by "h"	
}soopDataSet;

list<soopDataSet> data;

#ifdef DEBUG_TIME
//long frequency;        // ticks per second
//long t1, t2;           // ticks
double elapsedTime, sumTime = 0.0, sumDim = 0.0, sumSplit = 0.0;
#endif // DEBUG_TIME

struct compareRewardSum {
	bool operator()(soopDataSet  a, soopDataSet  b) {
		return a.rewardSum > b.rewardSum;
	}
};
// ========================= END declarations =========================


// Unnormalize control signal from [0..1] to [minU..maxU]
double unnormU(double u) //OK
{
	//double maxU=maxVoltage, minU=-maxVoltage;
	return (u-0.5)*2.0*maxVoltage; //u*((double)maxVoltage*2.0)-(double)maxVoltage;
}

void freeSoopDataList()
{
	//std::list<soopDataSet>::iterator it = data.begin();
	data.erase(data.begin(),data.end());
}

void initSoopData(soopDataSet& newData) //OK
{
	//soopDataSet*
	//    newData = new soopDataSet; //(soopDataSet*)malloc(sizeof(soopDataSet));
	//fill_n(newData.reward, initlen, 0);
	fill_n(newData.splits, initlen, 0);
	fill_n(newData.startPoint, initlen, 0);
	newData.sequenceLenght= 0;
	newData.rewardSum = 0.0;
	for (int i=0;i<initlen;i++)
	{
		//newData.trajectory[i] = new stateE;
		//        newData.trajectory[i].variables = (double*)malloc(sizeof(double) * nbHiddenstateEVariables);
		newData.trajectory[i].isTerminal = 0;
		newData.trajectory[i].reward = 0;
	}
	//return newData;
}

void fillSoopData(soopDataSet& destination, double *sP, unsigned int *s, stateE t[],/* double *r,*/ double rS, int sequneceLenght)
{
	double newRewardSum = 0.0;
	int splitSum = 0;
	initSoopData(destination);

	for (int i=0;i<sequneceLenght;i++)
	{
		splitSum += s[i];
		destination.splits[i] = s[i];
		destination.startPoint[i] = sP[i];
		memcpy(destination.trajectory[i].variables, t[i].variables, sizeof(double) * nbHiddenstateEVariables);
		destination.trajectory[i].reward =  t[i].reward;
		newRewardSum = newRewardSum + pow(gamma,(double)i)*t[i].reward;
		
	}
	destination.splitsSum = splitSum;
	destination.rewardSum = newRewardSum;
}

bool partialyGraterBox(soopDataSet a, soopDataSet b)  //OK for 1 dim check
{ // is Ua partially grater then Ub ?
	bool partialyGrater = true;


	for(unsigned int i=0; i<a.sequenceLenght; i++)
	{
		//cout << i << endl;
		if (a.splits[i] > b.splits[i] )
		{
			//cout << "a " << a.splits[i] << " b " << b.splits[i] << endl;
			partialyGrater = false;
			return partialyGrater;
		}
		//if (a.splits[i] == 0 && b.splits[i] == 0 ) break;
	}
	return partialyGrater;
}

double recalculateRewardSum(stateE t[], int nr)
{
	double newRewardSum = 0.0;
	for (int i = 0; i< nr;i++)
	{

		newRewardSum = newRewardSum + pow(gamma,(double)i)*t[i].reward;
	}
	return newRewardSum;
}

void printStateE(stateE s) //OK
{
	cout <<"alp " << s.variables[0] << " th " << s.variables[1] <<  " R " << s.reward << endl;
}

void printStatusToFile(stateE s0) //, list<soopDataSet>::iterator itmax, double commandSequence, double currentBudget, double maxLengthSequence, int seqNr) //OK  tester
{
	ofstream myfile;
	myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
	myfile  << "x " << ((s0).variables[0]) << " , " << ((s0).variables[1]) << " R " << s0.reward << endl; //" u " << commandSequence << " n " << (currentBudget) << " len_ret= " << ((*itmax).sequenceLenght) << " longest= " << (maxLengthSequence) <<" sq Nr. " << seqNr<< endl;
	myfile.close();
}

void printBool(bool *b, int listLenght) //OK tester
{
	for (int iTmp =0;iTmp<listLenght;iTmp++)
	{
		cout << boolalpha << b[iTmp] << " ";
	}
	cout << endl;
}

double middleOfBox(double startPoint, int splits) //OK
{	
	return startPoint + pow(1.0/3.0,((int)splits)) / 2.0;
}

// = alpha^dimention * (1/3)^#splits
double dimentionScore(int dimention, int splits) //OK
{
	return  pow(alpha,(int)dimention)*pow((1.0/3.0),(int)splits);
}

// = gamma^dimention * (1/3)^#splits
double dimentionScore_gamma(int dimention, int splits) //OK
{
	return  pow(gamma, (int)dimention)*pow((1.0 / 3.0), (int)splits);
}

// Cj = L_lp * gamma^i * (1/3)^#splits *(1/2) * min(1, 1-(gamma*L_lp)^(T-i)/(1-gamma*L_lp) )
double calcualteCj(int splits, int T, int index)
{
	double Cj = 0.0;
	int i = index;
	double partialCjKapping = min(( 1.0-pow(gamma*L_lp,(int)(T-i)) )/ (1.0-gamma*L_lp),1);
	Cj = L_lp * pow(gamma,(double)i) * pow(1.0/3.0,(int)splits)/2.0 * partialCjKapping;// min(partialCjKapping,1);//partialCjKapping;

	return Cj;
}

// = sum_i^T (gamma^i * r_i + Cj_i) + gamma^T/(1-gamma)
double calcualteB(soopDataSet d)
{
	double b = 0.0, ct = 0.0;
	int T  = d.sequenceLenght;

	ct = pow(gamma,T)/(1-gamma);

	for(int i = 0; i<T; i++)
	{
		b = b + pow(gamma,i)*d.trajectory[i].reward + calcualteCj(d.splits[i], T, i); //
	}

	b = b + ct;

	return b;
}

double calcualteB_OPC(soopDataSet d)
{
	double b = 0.0, ct = 0.0, cj = 0.0;
	int T  = d.sequenceLenght;

	ct = pow(gamma,T)/(1-gamma);

	for(int i = 0; i<T; i++)
	{
		if (d.splits[i] == 0)
		{
			ct = pow(gamma,i)/(1-gamma);
			d.sequenceLenght = i;
			perror("sequenceLength was longher than real length");			
			break;
		}

		cj = 0.0;
		for (int j = 0; j<i ; j++)
		{
			cj+=pow(L_lp,i-j+1)*pow(1.0/3.0,(int)d.splits[j])/2.0;
		}		

		b = b + pow(gamma,i)*min(1, (d.trajectory[i].reward) + cj);			
	}
	b = b + ct;
	return b;
}

int dimentionSelectionOPC(int length, unsigned int * splits)
{
	int pozMaxDimSc = 0;
	double MaxDimScore = 0.0;
	int current_Dim = 0;

	while (current_Dim < length)
	{
		if (current_Dim  >=  initlen)
		{
			perror("Current dimnetion exceeded the limit of the vector");

		}

#ifdef DEBUG_FN
		cout << dimentionScore(current_Dim, splits[current_Dim]) << ",  ";
#endif

		if (dimentionScore(current_Dim, splits[current_Dim])  >  MaxDimScore)
		{
			MaxDimScore = dimentionScore(current_Dim, splits[current_Dim]);
			pozMaxDimSc = current_Dim;
		}
		current_Dim++;

	}

	if (current_Dim >= initlen) // take into consideration the next empty dimension
	{
		perror("Current dimnetion exceeded the limit of the vector");

	}

#ifdef DEBUG_FN
	cout << dimentionScore(current_Dim, splits[current_Dim]) << endl;
#endif

	if (dimentionScore(current_Dim, splits[current_Dim])>  MaxDimScore)
	{
		MaxDimScore = dimentionScore(current_Dim, splits[current_Dim]);
		pozMaxDimSc = current_Dim;
	}		
#ifdef DEBUG_FN
	cout << " score: " <<  MaxDimScore << " poz= " <<pozMaxDimSc ;
#endif
	return pozMaxDimSc;
}

int dimentionSelectionSOPC(int length, unsigned int * splits)
{
	int pozMaxDimSc = 0;
	double MaxDimScore = 0.0;
	int current_Dim = 0;

	while (current_Dim < length)
	{
		if (current_Dim >= initlen)
		{
			perror("Current dimnetion exceeded the limit of the vector");

		}

#ifdef DEBUG_FN
		cout << dimentionScore(current_Dim, splits[current_Dim]) << ",  ";
#endif

		if (dimentionScore_gamma(current_Dim, splits[current_Dim])  >  MaxDimScore)
		{
			MaxDimScore = dimentionScore_gamma(current_Dim, splits[current_Dim]);
			pozMaxDimSc = current_Dim;
		}
		current_Dim++;

	}

	if (current_Dim >= initlen) // take into consideration the next empty dimension
	{
		perror("Current dimnetion exceeded the limit of the vector");

	}

#ifdef DEBUG_FN
	cout << dimentionScore(current_Dim, splits[current_Dim]) << endl;
#endif

	if (dimentionScore_gamma(current_Dim, splits[current_Dim])>  MaxDimScore)
	{
		MaxDimScore = dimentionScore(current_Dim, splits[current_Dim]);
		pozMaxDimSc = current_Dim;
	}
#ifdef DEBUG_FN
	cout << " score: " << MaxDimScore << " poz= " << pozMaxDimSc;
#endif
	return pozMaxDimSc;
}

int dimentionSelectionOPC_lp(int length, unsigned int * splits)
{
	int pozMaxDimSc = 0;
	double MaxDimScore = 0.0, cj_tmp = 0.0;	

	for (int i = 0; i <= length; i++)
	{
		cj_tmp = calcualteCj(splits[i],  length, i);
		if (MaxDimScore < cj_tmp)
		{
			MaxDimScore = cj_tmp;
			pozMaxDimSc = i;
		}
	}	

	return pozMaxDimSc;
}

double * soop(stateE& s0)
{ // SOOP algorithm, first element of data list has the highest rewardSum


	bool stopFlag = false;
	unsigned int counter = 0;
	unsigned int currentBudget = 0;	
	unsigned int listLenght = 0;
	unsigned int maxLengthSequence = 0;
	
	/*==========================================================
	=====================INIT BOXES=============================
	*///========================================================
	for (int i=0;i<3;i++)
	{
		double sPtmp[initlen] = {0};
		unsigned int stmp[initlen] = {0};
		stateE newstateE[initlen];
		sPtmp[0] = ((double)i)/3.0;
		stmp[0] = 1;
		newstateE[0] = nextstateE(s0, unnormU(middleOfBox(sPtmp[0], stmp[0])) );
		soopDataSet newData;
		fillSoopData(newData,sPtmp, stmp, newstateE,newstateE[0].reward, 1);
		newData.sequenceLenght = 1;
		data.push_front(newData);
	}
	bool newDimensionAdded = false;

#ifdef DEBUG_TIME
	double timeBoxSelect = 0.0, timeDimSelect = 0.0, timeSplit = 0.0;
	int nrTimeSamples = 0;		
#endif

	while (! stopFlag)         // main loop
	{
		ofstream myfile;
		counter++;
		/*==========================================================
		=====================Select BOXES to EXP====================
		*///========================================================
#ifdef DEBUG_TIME	
		nrTimeSamples++;
		__int64 frequencyBS, startBS, endtBS, totalBS;
		QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyBS);
		QueryPerformanceCounter ( (LARGE_INTEGER *)&startBS);  
#endif

		listLenght = data.size();
		list<soopDataSet> toExpand;

		list <list<soopDataSet>::iterator> toExpandIterator;
		bool reject;
		for (std::list<soopDataSet>::iterator Ui = data.begin(); Ui != data.end(); ++Ui)
		{

			//(*Ui).rewardSum = recalculateRewardSum((*Ui).trajectory, (*Ui).sequenceLenght);
			reject = false;

			for ( list<soopDataSet>::iterator Uj = data.begin();  Uj != data.end(); ++Uj)
			{
				//(*Uj).rewardSum = recalculateRewardSum((*Uj).trajectory, (*Ui).sequenceLenght);

				if (Uj != Ui)
				{
					if ((*Uj).sequenceLenght == (*Ui).sequenceLenght)
					{
						if (partialyGraterBox((*Uj),(*Ui)))
						{
							if ((*Uj).rewardSum > (*Ui).rewardSum ) //+1e-5 in case of faulty calculus
							{
								reject = true;
								break;
							}
						}
					}
				}
			}

			if (!reject)
			{
				toExpandIterator.push_back(Ui);
			}

		}

		for (list <list<soopDataSet>::iterator>::iterator it = toExpandIterator.begin(); it!= toExpandIterator.end(); ++it)
		{
			toExpand.splice(toExpand.end(),data,(*it));
		}

		toExpandIterator.erase(toExpandIterator.begin(),toExpandIterator.end());



#ifdef DEBUG_TIME
		QueryPerformanceCounter((LARGE_INTEGER *)&endtBS);
		timeBoxSelect = timeBoxSelect + (endtBS - startBS) * 1000.0 / frequencyBS;
		//cour << "boxSelect=  " << timeBoxSelect << endl;
#endif
		/*==========================================================
		=====================Select dim of BOXES & EXP==============
		*///========================================================


		for (std::list<soopDataSet>::iterator it=toExpand.begin(); it != toExpand.end(); ++it)
		{
#ifdef DEBUG_TIME		
			__int64 frequencyDS, startDS, endtDS, totalDS;
			QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyDS);
			QueryPerformanceCounter ( (LARGE_INTEGER *)&startDS);  
#endif
			//dimension selection
			unsigned int pozMaxDimSc =  dimentionSelectionOPC((*it).sequenceLenght, (*it).splits);


			if (pozMaxDimSc > maxLengthSequence )
			{
				newDimensionAdded = true;

			}
			//ofstream myfile;
			//myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
			//myfile << "while NR "<< counter <<" pozMAx " << pozMaxDimSc <<" size toEx " << toExpand.size() << endl;
			//myfile.close();
#ifdef DEBUG_TIME
			QueryPerformanceCounter((LARGE_INTEGER *)&endtDS);
			timeDimSelect = timeDimSelect + (endtDS - startDS) * 1000.0 / frequencyDS;
			//cour << "boxSelect=  " << timeBoxSelect << endl;
#endif
			/*==========================================================
			=====================SPLIT DIM==============================
			*///========================================================
#ifdef DEBUG_TIME		
			__int64 frequencyS, startS, endtS, totalS;
			QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyS);
			QueryPerformanceCounter ( (LARGE_INTEGER *)&startS);  
#endif

			soopDataSet leftSegment;// = new soopDataSet;
			soopDataSet rightSegment;// = new soopDataSet;
			fillSoopData(leftSegment, (*it).startPoint,(*it).splits,(*it).trajectory,(*it).rewardSum,(*it).sequenceLenght);
			fillSoopData(rightSegment,(*it).startPoint,(*it).splits,(*it).trajectory,(*it).rewardSum,(*it).sequenceLenght);

			(*it).splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc] + 1;
			leftSegment.splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc];
			rightSegment.splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc];
			// startPoint setup L/R/O
			leftSegment.startPoint[pozMaxDimSc] = (*it).startPoint[pozMaxDimSc];
			(*it).startPoint[pozMaxDimSc] = leftSegment.startPoint[pozMaxDimSc] + pow(1.0/3.0, (int)leftSegment.splits[pozMaxDimSc]);
			rightSegment.startPoint[pozMaxDimSc] = (*it).startPoint[pozMaxDimSc] + pow(1.0/3.0, (int)leftSegment.splits[pozMaxDimSc]);
			leftSegment.sequenceLenght = (*it).sequenceLenght;
			rightSegment.sequenceLenght = (*it).sequenceLenght;

			if (pozMaxDimSc >= (*it).sequenceLenght -1 )//((*it).splits[pozMaxDimSc] == 1 || (*it).splits[pozMaxDimSc+1] == 0) //split last or new dimension
			{
				//setup L/R/O splits count

				if (pozMaxDimSc == (*it).sequenceLenght) // split new dim
				{   // model simulate LRO

					currentBudget = currentBudget + 3;

					double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
					stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
					stateE  newRstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;

					uMidle = middleOfBox((*it).startPoint[pozMaxDimSc], (*it).splits[pozMaxDimSc]);
					stateE  newCstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy((*it).trajectory[pozMaxDimSc].variables, newCstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					(*it).trajectory[pozMaxDimSc].reward =  newCstateE.reward;

					(*it).sequenceLenght++;
					leftSegment.sequenceLenght = (*it).sequenceLenght;
					rightSegment.sequenceLenght = (*it).sequenceLenght;


				}
				else // split last dim.
				{ // model simulate LR
					currentBudget = currentBudget + 2;

					if (pozMaxDimSc == 0 )
					{ // split first dim => simulate with s0 new state // setup trajectory  L/R, O is the same
						double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
						stateE newLstateE = nextstateE(s0, unnormU(uMidle));
						memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

						uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
						stateE  newRstateE =nextstateE(s0,unnormU(uMidle));
						memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;

					}
					else
					{ // split last dim,  it is not the first dim.
						double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
						stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
						memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

						uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
						stateE  newRstateE =nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
						memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
					}
				}
			}
			else // split dimension and recalculate trajectories
			{

				double uLmidle, uRmidle, uCmidle;
				uLmidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc],leftSegment.splits[pozMaxDimSc]);
				uCmidle = middleOfBox((*it).startPoint[pozMaxDimSc],(*it).splits[pozMaxDimSc]);
				uRmidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc],rightSegment.splits[pozMaxDimSc]);

				if (pozMaxDimSc == 0)
				{
					stateE newLstateE = nextstateE(s0, unnormU(uLmidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					stateE  newRstateE =nextstateE(s0,unnormU(uRmidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
				}
				else
				{
					stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uLmidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					stateE  newRstateE =nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uRmidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
				}
				currentBudget = currentBudget +2;
				unsigned int stepTmp=pozMaxDimSc + 1;

				while (stepTmp < (*it).sequenceLenght)//(*it).splits[stepTmp]!=0 || stepTmp>=initlen) //recalculate trajectory for left, right, center segments
				{
					//cout << "while2 at: " << stepTmp << " "<< (*it).sequenceLenght <<endl;
					stateE newLstateE;// = new stateE;
					stateE newRstateE;// = new stateE;
					uLmidle = middleOfBox((*it).startPoint[stepTmp],(*it).splits[stepTmp]);
					uRmidle = middleOfBox((*it).startPoint[stepTmp],(*it).splits[stepTmp ]);
					// stateE newCstateE;// = new stateE;

					//{
					newLstateE =  nextstateE(leftSegment.trajectory[stepTmp-1], unnormU(uLmidle));
					newRstateE =  nextstateE(rightSegment.trajectory[stepTmp-1], unnormU(uRmidle));
					//newCstateE =  nextstateE((*it).trajectory[stepTmp-1], unnormU(uCmidle));
					//}
					memcpy(leftSegment.trajectory[stepTmp].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[stepTmp].reward = newLstateE.reward;

					memcpy(rightSegment.trajectory[stepTmp].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[stepTmp].reward = newRstateE.reward;

					stepTmp++;
					currentBudget = currentBudget + 2;


				}
				//currentBudget=currentBudget+(stepTmp-pozMaxDimSc)*2;

			}
#ifdef DEBUG_TIME
			QueryPerformanceCounter((LARGE_INTEGER *)&endtS);
			timeSplit = timeSplit + (endtS - startS) * 1000.0 / frequencyS;
			//cour << "boxSelect=  " << timeBoxSelect << endl;
#endif
			(*it).rewardSum = recalculateRewardSum((*it).trajectory, (*it).sequenceLenght);
			leftSegment.rewardSum = recalculateRewardSum(leftSegment.trajectory, leftSegment.sequenceLenght);
			rightSegment.rewardSum = recalculateRewardSum(rightSegment.trajectory, rightSegment.sequenceLenght);

			data.push_back(leftSegment);
			data.push_back(rightSegment);


		}
		data.splice(data.end(),toExpand);
		toExpand.erase(toExpand.begin(), toExpand.end());
		if (currentBudget > budget) stopFlag = true;


		if (newDimensionAdded) maxLengthSequence++;
	}       //END while loop

#ifdef DEBUG_TIME
	cout << "box select= " << timeBoxSelect/nrTimeSamples << " ,dimSelect= " << timeDimSelect/nrTimeSamples << " ,split= " << timeSplit/nrTimeSamples << " ,n= " << nrTimeSamples;

	__int64 frequencyS, startS, endtS, totalS;
	QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyS);
	QueryPerformanceCounter ( (LARGE_INTEGER *)&startS);  
#endif
	//update max length of sequences



	double maxRewardSum = 0.0, commandSignalToSend = 0.0;
	for (std::list<soopDataSet>::iterator itTMP=data.begin(); itTMP != data.end(); ++itTMP)
	{
		if ((*itTMP).rewardSum > maxRewardSum)
		{
			commandSignalToSend = unnormU(middleOfBox((*itTMP).startPoint[0], (*itTMP).splits[0]));
			maxRewardSum = (*itTMP).rewardSum;
		}
	}
	//cout << "myserach RS max " << maxRewardSum << " U " << commandSignalToSend << endl;


	data.sort(compareRewardSum());
	list<soopDataSet>::iterator itmax = data.begin();

	double * commandSequence = (double*)malloc(sizeof(double)*initlen);
	
	for (int i=0;i<initlen;i++)
	{
		commandSequence[i] = unnormU(middleOfBox((*itmax).startPoint[i], (*itmax).splits[i]));
	}

	freeSoopDataList();

#ifdef DEBUG_TIME
	QueryPerformanceCounter((LARGE_INTEGER *)&endtS);
	double timeEnd =  (endtS - startS) * 1000.0 / frequencyS;
	cout << " ,end=" << timeEnd << endl;
#endif

	return commandSequence;
}


double * soop_soo(stateE& s0)
 { // SOOP algorithm, first element of data list has the highest rewardSum


	bool stopFlag = false;
	unsigned int counter = 0;
	unsigned int currentBudget = 0;
	unsigned int listLenght = 0;
	unsigned int maxLengthSequence = 0;

	/*==========================================================
	=====================INIT BOXES=============================
	*///========================================================
	for (int i=0;i<3;i++)
	{
		double sPtmp[initlen] = {0};
		unsigned int stmp[initlen] = {0};
		stateE newstateE[initlen];
		sPtmp[0] = ((double)i)/3.0;
		stmp[0] = 1;
		newstateE[0] = nextstateE(s0, unnormU(middleOfBox(sPtmp[0], stmp[0])) );
		soopDataSet newData;
		fillSoopData(newData,sPtmp, stmp, newstateE,newstateE[0].reward, 1);
		newData.sequenceLenght = 1;
		newData.splitsSum = 1;
		data.push_front(newData);
	}
	bool newDimensionAdded = false;

#ifdef DEBUG_TIME
	double timeBoxSelect = 0.0, timeDimSelect = 0.0, timeSplit = 0.0;
	int nrTimeSamples = 0;		
#endif

	while (! stopFlag)         // main loop
	{
		ofstream myfile;
		counter++;
		/*==========================================================
		=====================Select BOXES to EXP====================
		*///========================================================
#ifdef DEBUG_TIME	
		nrTimeSamples++;
		__int64 frequencyBS, startBS, endtBS, totalBS;
		QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyBS);
		QueryPerformanceCounter ( (LARGE_INTEGER *)&startBS);  
#endif

		listLenght = data.size();
		list<soopDataSet> toExpand;

		list <list<soopDataSet>::iterator> toExpandIterator;
		bool reject;
		for (std::list<soopDataSet>::iterator Ui = data.begin(); Ui != data.end(); ++Ui)
		{

			//(*Ui).rewardSum = recalculateRewardSum((*Ui).trajectory, (*Ui).sequenceLenght);
			reject = false;

			for ( list<soopDataSet>::iterator Uj = data.begin();  Uj != data.end(); ++Uj)
			{
				//(*Uj).rewardSum = recalculateRewardSum((*Uj).trajectory, (*Ui).sequenceLenght);

				if (Uj != Ui)
				{
					if ((*Uj).sequenceLenght == (*Ui).sequenceLenght)
					{
						if ((Uj->splitsSum) >= (Ui->splitsSum))
						{
							if ((*Uj).rewardSum > (*Ui).rewardSum ) //+1e-5 in case of faulty calculus
							{
								reject = true;
								break;
							}
						}
					}
				}
			}

			if (!reject)
			{
				toExpandIterator.push_back(Ui);
			}

		}

		for (list <list<soopDataSet>::iterator>::iterator it = toExpandIterator.begin(); it!= toExpandIterator.end(); ++it)
		{
			toExpand.splice(toExpand.end(),data,(*it));
		}

		toExpandIterator.erase(toExpandIterator.begin(),toExpandIterator.end());



#ifdef DEBUG_TIME
		QueryPerformanceCounter((LARGE_INTEGER *)&endtBS);
		timeBoxSelect = timeBoxSelect + (endtBS - startBS) * 1000.0 / frequencyBS;
		//cour << "boxSelect=  " << timeBoxSelect << endl;
#endif
		/*==========================================================
		=====================Select dim of BOXES & EXP==============
		*///========================================================


		for (std::list<soopDataSet>::iterator it=toExpand.begin(); it != toExpand.end(); ++it)
		{
#ifdef DEBUG_TIME		
			__int64 frequencyDS, startDS, endtDS, totalDS;
			QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyDS);
			QueryPerformanceCounter ( (LARGE_INTEGER *)&startDS);  
#endif
			//dimension selection
			unsigned int pozMaxDimSc = pozMaxDimSc = dimentionSelectionOPC((*it).sequenceLenght, (*it).splits);


			if (pozMaxDimSc > maxLengthSequence )
			{
				newDimensionAdded = true;

			}
			//ofstream myfile;
			//myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
			//myfile << "while NR "<< counter <<" pozMAx " << pozMaxDimSc <<" size toEx " << toExpand.size() << endl;
			//myfile.close();
#ifdef DEBUG_TIME
			QueryPerformanceCounter((LARGE_INTEGER *)&endtDS);
			timeDimSelect = timeDimSelect + (endtDS - startDS) * 1000.0 / frequencyDS;
			//cour << "boxSelect=  " << timeBoxSelect << endl;
#endif
			/*==========================================================
			=====================SPLIT DIM==============================
			*///========================================================
#ifdef DEBUG_TIME		
			__int64 frequencyS, startS, endtS, totalS;
			QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyS);
			QueryPerformanceCounter ( (LARGE_INTEGER *)&startS);  
#endif

			soopDataSet leftSegment;// = new soopDataSet;
			soopDataSet rightSegment;// = new soopDataSet;
			fillSoopData(leftSegment, (*it).startPoint,(*it).splits,(*it).trajectory,(*it).rewardSum,(*it).sequenceLenght);
			fillSoopData(rightSegment,(*it).startPoint,(*it).splits,(*it).trajectory,(*it).rewardSum,(*it).sequenceLenght);

			(*it).splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc] + 1;
			leftSegment.splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc];
			rightSegment.splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc];
			// startPoint setup L/R/O
			leftSegment.startPoint[pozMaxDimSc] = (*it).startPoint[pozMaxDimSc];
			(*it).startPoint[pozMaxDimSc] = leftSegment.startPoint[pozMaxDimSc] + pow(1.0/3.0, (int)leftSegment.splits[pozMaxDimSc]);
			rightSegment.startPoint[pozMaxDimSc] = (*it).startPoint[pozMaxDimSc] + pow(1.0/3.0, (int)leftSegment.splits[pozMaxDimSc]);
			leftSegment.sequenceLenght = (*it).sequenceLenght;
			rightSegment.sequenceLenght = (*it).sequenceLenght;

			it->splitsSum += 1;
			rightSegment.splitsSum = it->splitsSum;
			leftSegment.splitsSum = it->splitsSum;

			if (pozMaxDimSc >= (*it).sequenceLenght -1 )//((*it).splits[pozMaxDimSc] == 1 || (*it).splits[pozMaxDimSc+1] == 0) //split last or new dimension
			{
				//setup L/R/O splits count

				if (pozMaxDimSc == (*it).sequenceLenght) // split new dim
				{   // model simulate LRO

					currentBudget = currentBudget + 3;

					double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
					stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
					stateE  newRstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;

					uMidle = middleOfBox((*it).startPoint[pozMaxDimSc], (*it).splits[pozMaxDimSc]);
					stateE  newCstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy((*it).trajectory[pozMaxDimSc].variables, newCstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					(*it).trajectory[pozMaxDimSc].reward =  newCstateE.reward;

					(*it).sequenceLenght++;
					leftSegment.sequenceLenght = (*it).sequenceLenght;
					rightSegment.sequenceLenght = (*it).sequenceLenght;


				}
				else // split last dim.
				{ // model simulate LR
					currentBudget = currentBudget + 2;

					if (pozMaxDimSc == 0 )
					{ // split first dim => simulate with s0 new state // setup trajectory  L/R, O is the same
						double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
						stateE newLstateE = nextstateE(s0, unnormU(uMidle));
						memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

						uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
						stateE  newRstateE =nextstateE(s0,unnormU(uMidle));
						memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;

					}
					else
					{ // split last dim,  it is not the first dim.
						double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
						stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
						memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

						uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
						stateE  newRstateE =nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
						memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
					}
				}
			}
			else // split dimension and recalculate trajectories
			{

				double uLmidle, uRmidle, uCmidle;
				uLmidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc],leftSegment.splits[pozMaxDimSc]);
				uCmidle = middleOfBox((*it).startPoint[pozMaxDimSc],(*it).splits[pozMaxDimSc]);
				uRmidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc],rightSegment.splits[pozMaxDimSc]);

				if (pozMaxDimSc == 0)
				{
					stateE newLstateE = nextstateE(s0, unnormU(uLmidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					stateE  newRstateE =nextstateE(s0,unnormU(uRmidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
				}
				else
				{
					stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uLmidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					stateE  newRstateE =nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uRmidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
				}
				currentBudget = currentBudget +2;
				unsigned int stepTmp=pozMaxDimSc + 1;

				while (stepTmp < (*it).sequenceLenght)//(*it).splits[stepTmp]!=0 || stepTmp>=initlen) //recalculate trajectory for left, right, center segments
				{
					//cout << "while2 at: " << stepTmp << " "<< (*it).sequenceLenght <<endl;
					stateE newLstateE;// = new stateE;
					stateE newRstateE;// = new stateE;
					uLmidle = middleOfBox((*it).startPoint[stepTmp],(*it).splits[stepTmp]);
					uRmidle = middleOfBox((*it).startPoint[stepTmp],(*it).splits[stepTmp ]);
					// stateE newCstateE;// = new stateE;

					//{
					newLstateE =  nextstateE(leftSegment.trajectory[stepTmp-1], unnormU(uLmidle));
					newRstateE =  nextstateE(rightSegment.trajectory[stepTmp-1], unnormU(uRmidle));
					//newCstateE =  nextstateE((*it).trajectory[stepTmp-1], unnormU(uCmidle));
					//}
					memcpy(leftSegment.trajectory[stepTmp].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[stepTmp].reward = newLstateE.reward;

					memcpy(rightSegment.trajectory[stepTmp].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[stepTmp].reward = newRstateE.reward;

					stepTmp++;
					currentBudget = currentBudget + 2;


				}
				//currentBudget=currentBudget+(stepTmp-pozMaxDimSc)*2;

			}
#ifdef DEBUG_TIME
			QueryPerformanceCounter((LARGE_INTEGER *)&endtS);
			timeSplit = timeSplit + (endtS - startS) * 1000.0 / frequencyS;
			//cour << "boxSelect=  " << timeBoxSelect << endl;
#endif
			(*it).rewardSum = recalculateRewardSum((*it).trajectory, (*it).sequenceLenght);
			leftSegment.rewardSum = recalculateRewardSum(leftSegment.trajectory, leftSegment.sequenceLenght);
			rightSegment.rewardSum = recalculateRewardSum(rightSegment.trajectory, rightSegment.sequenceLenght);

			data.push_back(leftSegment);
			data.push_back(rightSegment);


		}
		data.splice(data.end(),toExpand);
		toExpand.erase(toExpand.begin(), toExpand.end());
		if (currentBudget > budget) stopFlag = true;

#ifdef DEBUG_SPLIT
		//data.sort(compareRewardSum());
		//ofstream myfile;
		myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
		myfile << "list of SQ NOT EXP:" << data.size() << " loop "<< counter <<endl;
		myfile.close();
		for (std::list<soopDataSet>::iterator itTMP=data.begin(); itTMP != data.end(); ++itTMP)
		{
			myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
			(*itTMP).rewardSum = recalculateRewardSum((*itTMP).trajectory, (*itTMP).sequenceLenght);
			// cout << "RS " << (*itTMP).rewardSum << " ";
			myfile << "RS " << (*itTMP).rewardSum <<" sqLen " << (*itTMP).sequenceLenght;
			myfile.close();
			for (int itmp = 0; itmp<(*itTMP).sequenceLenght;itmp++ )
			{
				//if ((*itTMP).splits[itmp] != 0.0)
				{
					myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
					//cout  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp] <<" ";
					myfile  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp]  ;
					myfile.close();
					// printStateE((*itTMP).trajectory[itmp]);
					printStatusToFile((*itTMP).trajectory[itmp]);
				}
			}
			//cout<<endl;
		}
#endif

		if (newDimensionAdded) maxLengthSequence++;
	}       //END while loop

#ifdef DEBUG_TIME
	cout << "box select= " << timeBoxSelect/nrTimeSamples << " ,dimSelect= " << timeDimSelect/nrTimeSamples << " ,split= " << timeSplit/nrTimeSamples << " ,n= " << nrTimeSamples;

	__int64 frequencyS, startS, endtS, totalS;
	QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyS);
	QueryPerformanceCounter ( (LARGE_INTEGER *)&startS);  
#endif
	//update max length of sequences



	double maxRewardSum = 0.0, commandSignalToSend = 0.0;
	for (std::list<soopDataSet>::iterator itTMP=data.begin(); itTMP != data.end(); ++itTMP)
	{
		if ((*itTMP).rewardSum > maxRewardSum)
		{
			commandSignalToSend = unnormU(middleOfBox((*itTMP).startPoint[0], (*itTMP).splits[0]));
			maxRewardSum = (*itTMP).rewardSum;
		}
	}
	//cout << "myserach RS max " << maxRewardSum << " U " << commandSignalToSend << endl;


	data.sort(compareRewardSum());
	list<soopDataSet>::iterator itmax = data.begin();

	double * commandSequence = (double*)malloc(sizeof(double)*initlen);
	//commandSequence[0] = (*itmax).sequenceLenght;

	for (int i=0;i<initlen;i++)
	{
		commandSequence[i] = unnormU(middleOfBox((*itmax).startPoint[i], (*itmax).splits[i]));
	}

	//printf("%d, ", (*itmax).sequenceLenght);
#ifdef DEBUG_COMMAND
	cout<< "RS " <<(*itmax).rewardSum<< " start= "<< (*itmax).startPoint[0] <<" split " << (*itmax).splits[0]<< " seqLen " << (*itmax).sequenceLenght  <<" command= " << middleOfBox((*itmax).startPoint[0], (*itmax).splits[0]) << " U= " << commandSequence[0]<< endl;
	//cout << "MidBox " << (middleOfBox((*itmax).startPoint[0], (*itmax).splits[0])) << " for SP " << (*itmax).startPoint[0] << " spilts " << (*itmax).splits[0] <<endl;
	cout << "SOOP main return done max length seq: "<< maxLengthSequence<< endl;
#endif

	freeSoopDataList();
#ifdef DEBUG_TIME
	QueryPerformanceCounter((LARGE_INTEGER *)&endtS);
	double timeEnd =  (endtS - startS) * 1000.0 / frequencyS;
	cout << " ,end=" << timeEnd << endl;
#endif

	return commandSequence;
}

double * OPC_lp(stateE& s0)
{ // SOOP algorithm, first element of data list has the highest rewardSum


	bool stopFlag = false;
	unsigned int counter = 0;
	unsigned int currentBudget = 0;
	unsigned int listLenght = 0;
	unsigned int maxLengthSequence = 0;
	double * commandSequence = (double*)malloc(sizeof(double)*initlen);

	/*==========================================================
	=====================INIT BOXES=============================
	*///========================================================
	//cout<<s0.variables[0]<<endl;
	for (int i=0;i<3;i++)
	{
		double sPtmp[initlen] = {0};
		unsigned int stmp[initlen] = {0};
		stateE newstateE[initlen];
		sPtmp[0] = ((double)i)/3.0;
		stmp[0] = 1;
		newstateE[0] = nextstateE(s0, unnormU(middleOfBox(sPtmp[0], stmp[0])) );
		soopDataSet newData;
		fillSoopData(newData,sPtmp, stmp, newstateE,newstateE[0].reward, 1);
		newData.sequenceLenght = 1;
		data.push_front(newData);
	}



	bool newDimensionAdded = false;

	while (! stopFlag)         // main loop
	{

		ofstream myfile;
		counter++;
		/*==========================================================
		=====================Select BOXES to EXP====================
		*///========================================================

		listLenght = data.size();
		list<soopDataSet> toExpand;

		list <list<soopDataSet>::iterator> toExpandIterator;
		//bool reject;
		double B_max = 0.0, B_current = 0.0;
		for (std::list<soopDataSet>::iterator Ui = data.begin(); Ui != data.end(); ++Ui)
		{
			B_current = calcualteB((*Ui));
			if ( B_max < B_current )
			{
				toExpandIterator.clear();
				toExpandIterator.push_back(Ui);
				B_max = B_current;
			}
		}

		for (list <list<soopDataSet>::iterator>::iterator it = toExpandIterator.begin(); it!= toExpandIterator.end(); ++it)
		{
			toExpand.splice(toExpand.end(),data,(*it));
		}

		toExpandIterator.erase(toExpandIterator.begin(),toExpandIterator.end());


#ifdef DEBUG_SPLIT
		//data.sort(compareRewardSum());
		//ofstream myfile;
		myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
		myfile << "list of SQ NOT EXP:" << data.size() <<endl;
		myfile.close();
		for (std::list<soopDataSet>::iterator itTMP=data.begin(); itTMP != data.end(); ++itTMP)
		{
			myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
			(*itTMP).rewardSum = recalculateRewardSum((*itTMP).trajectory, (*itTMP).sequenceLenght);
			// cout << "RS " << (*itTMP).rewardSum << " ";
			myfile << "RS " << (*itTMP).rewardSum << " ";
			myfile.close();
			for (int itmp = 0; itmp<(*itTMP).sequenceLenght;itmp++ )
			{
				//if ((*itTMP).splits[itmp] != 0.0)
				{
					myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
					//cout  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp] <<" ";
					myfile  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp] <<" ";
					myfile.close();
					// printStateE((*itTMP).trajectory[itmp]);
					printStatusToFile((*itTMP).trajectory[itmp]);
				}
			}
			//cout<<endl;
		}
#endif
#ifdef DEBUG_SPLIT
		//data.sort(compareRewardSum());
		//ofstream myfile;
		myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
		myfile << "list of SQ to EXP:" << toExpand.size() <<endl;
		myfile.close();
		for (std::list<soopDataSet>::iterator itTMP=toExpand.begin(); itTMP != toExpand.end(); ++itTMP)
		{
			myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
			(*itTMP).rewardSum = recalculateRewardSum((*itTMP).trajectory, (*itTMP).sequenceLenght);
			// cout << "RS " << (*itTMP).rewardSum << " ";
			myfile << "RS " << (*itTMP).rewardSum << " ";
			myfile.close();
			for (int itmp = 0; itmp<(*itTMP).sequenceLenght;itmp++ )
			{
				if ((*itTMP).splits[itmp] != 0.0)
				{
					myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
					//cout  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp] <<" ";
					myfile  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp] <<" ";
					myfile.close();
					// printStateE((*itTMP).trajectory[itmp]);
					printStatusToFile((*itTMP).trajectory[itmp]);
				}
			}
			//cout<<endl;
		}
#endif


		/*==========================================================
		=====================Select dim of BOXES & EXP==============
		*///========================================================


		for (std::list<soopDataSet>::iterator it=toExpand.begin(); it != toExpand.end(); ++it)
		{

			//dimension selection
			unsigned int pozMaxDimSc = dimentionSelectionOPC_lp((*it).sequenceLenght, (*it).splits);

			if (pozMaxDimSc > maxLengthSequence )
			{
				newDimensionAdded = true;
			}
			//ofstream myfile;
			//myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
			//myfile << "while NR "<< counter <<" pozMAx " << pozMaxDimSc <<" size toEx " << toExpand.size() << endl;
			//myfile.close();
			/*==========================================================
			=====================SPLIT DIM==============================
			*///========================================================

			soopDataSet leftSegment;// = new soopDataSet;
			soopDataSet rightSegment;// = new soopDataSet;
			fillSoopData(leftSegment, (*it).startPoint,(*it).splits,(*it).trajectory,(*it).rewardSum,(*it).sequenceLenght);
			fillSoopData(rightSegment,(*it).startPoint,(*it).splits,(*it).trajectory,(*it).rewardSum,(*it).sequenceLenght);

			(*it).splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc] + 1;
			leftSegment.splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc];
			rightSegment.splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc];
			// startPoint setup L/R/O
			leftSegment.startPoint[pozMaxDimSc] = (*it).startPoint[pozMaxDimSc];
			(*it).startPoint[pozMaxDimSc] = leftSegment.startPoint[pozMaxDimSc] + pow(1.0/3.0, (int)leftSegment.splits[pozMaxDimSc]);
			rightSegment.startPoint[pozMaxDimSc] = (*it).startPoint[pozMaxDimSc] + pow(1.0/3.0, (int)leftSegment.splits[pozMaxDimSc]);
			leftSegment.sequenceLenght = (*it).sequenceLenght;
			rightSegment.sequenceLenght = (*it).sequenceLenght;

			if (pozMaxDimSc >= (*it).sequenceLenght -1 )//((*it).splits[pozMaxDimSc] == 1 || (*it).splits[pozMaxDimSc+1] == 0) //split last or new dimension
			{
				//setup L/R/O splits count

				if (pozMaxDimSc == (*it).sequenceLenght) // split new dim
				{   // model simulate LRO

					currentBudget = currentBudget + 3;

					double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
					stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
					stateE  newRstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;

					uMidle = middleOfBox((*it).startPoint[pozMaxDimSc], (*it).splits[pozMaxDimSc]);
					stateE  newCstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy((*it).trajectory[pozMaxDimSc].variables, newCstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					(*it).trajectory[pozMaxDimSc].reward =  newCstateE.reward;

					(*it).sequenceLenght++;
					leftSegment.sequenceLenght = (*it).sequenceLenght;
					rightSegment.sequenceLenght = (*it).sequenceLenght;


				}
				else // split last dim.
				{ // model simulate LR
					currentBudget = currentBudget + 2;

					if (pozMaxDimSc == 0 )
					{ // split first dim => simulate with s0 new state // setup trajectory  L/R, O is the same
						double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
						stateE newLstateE = nextstateE(s0, unnormU(uMidle));
						memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

						uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
						stateE  newRstateE =nextstateE(s0,unnormU(uMidle));
						memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;

					}
					else
					{ // split last dim,  it is not the first dim.
						double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
						stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
						memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

						uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
						stateE  newRstateE =nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
						memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
					}
				}
			}
			else // split dimension and recalculate trajectories
			{

				double uLmidle, uRmidle, uCmidle;
				uLmidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc],leftSegment.splits[pozMaxDimSc]);
				uCmidle = middleOfBox((*it).startPoint[pozMaxDimSc],(*it).splits[pozMaxDimSc]);
				uRmidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc],rightSegment.splits[pozMaxDimSc]);

				if (pozMaxDimSc == 0)
				{
					stateE newLstateE = nextstateE(s0, unnormU(uLmidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					stateE  newRstateE =nextstateE(s0,unnormU(uRmidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
				}
				else
				{
					stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uLmidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					stateE  newRstateE =nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uRmidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
				}
				currentBudget = currentBudget +2;
				unsigned int stepTmp=pozMaxDimSc + 1;

				while (stepTmp < (*it).sequenceLenght)//(*it).splits[stepTmp]!=0 || stepTmp>=initlen) //recalculate trajectory for left, right, center segments
				{
					//cout << "while2 at: " << stepTmp << " "<< (*it).sequenceLenght <<endl;
					stateE newLstateE;// = new stateE;
					stateE newRstateE;// = new stateE;
					uLmidle = middleOfBox((*it).startPoint[stepTmp],(*it).splits[stepTmp]);
					uRmidle = middleOfBox((*it).startPoint[stepTmp],(*it).splits[stepTmp ]);
					// stateE newCstateE;// = new stateE;

					//{
					newLstateE =  nextstateE(leftSegment.trajectory[stepTmp-1], unnormU(uLmidle));
					newRstateE =  nextstateE(rightSegment.trajectory[stepTmp-1], unnormU(uRmidle));
					//newCstateE =  nextstateE((*it).trajectory[stepTmp-1], unnormU(uCmidle));
					//}
					memcpy(leftSegment.trajectory[stepTmp].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[stepTmp].reward = newLstateE.reward;

					memcpy(rightSegment.trajectory[stepTmp].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[stepTmp].reward = newRstateE.reward;

					stepTmp++;
					currentBudget = currentBudget + 2;


				}
				//currentBudget=currentBudget+(stepTmp-pozMaxDimSc)*2;

			}
			leftSegment.rewardSum = recalculateRewardSum(leftSegment.trajectory, leftSegment.sequenceLenght);
			rightSegment.rewardSum = recalculateRewardSum(rightSegment.trajectory, rightSegment.sequenceLenght);

			data.push_back(leftSegment);
			data.push_back(rightSegment);


		}
		data.splice(data.end(),toExpand);
		toExpand.erase(toExpand.begin(), toExpand.end());
		if (currentBudget > budget) stopFlag = true;

#ifdef DEBUG_SPLIT
		//data.sort(compareRewardSum());
		//ofstream myfile;
		myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
		myfile << "list of SQ NOT EXP:" << data.size() << " loop "<< counter <<endl;
		myfile.close();
		for (std::list<soopDataSet>::iterator itTMP=data.begin(); itTMP != data.end(); ++itTMP)
		{
			myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
			(*itTMP).rewardSum = recalculateRewardSum((*itTMP).trajectory, (*itTMP).sequenceLenght);
			// cout << "RS " << (*itTMP).rewardSum << " ";
			myfile << "RS " << (*itTMP).rewardSum <<" sqLen " << (*itTMP).sequenceLenght;
			myfile.close();
			for (int itmp = 0; itmp<(*itTMP).sequenceLenght;itmp++ )
			{
				//if ((*itTMP).splits[itmp] != 0.0)
				{
					myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
					//cout  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp] <<" ";
					myfile  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp]  ;
					myfile.close();
					// printStateE((*itTMP).trajectory[itmp]);
					printStatusToFile((*itTMP).trajectory[itmp]);
				}
			}
			//cout<<endl;
		}
#endif
		if (newDimensionAdded) maxLengthSequence++;
	}       //END while loop

	//update max length of sequences

	data.sort(compareRewardSum());
	list<soopDataSet>::iterator itmax = data.begin();


	//double  commandSequence[initlen];
	//commandSequence[0] = (*itmax).sequenceLenght;

	for (int i=0;i<initlen;i++)
	{
		commandSequence[i] = unnormU(middleOfBox((*itmax).startPoint[i], (*itmax).splits[i]));
	}

	//printf("%d, ", (*itmax).sequenceLenght);
#ifdef DEBUG_COMMAND
	cout<< "RS " <<(*itmax).rewardSum<< " start= "<< (*itmax).startPoint[0] <<" split " << (*itmax).splits[0]<< " seqLen " << (*itmax).sequenceLenght  <<" command= " << middleOfBox((*itmax).startPoint[0], (*itmax).splits[0]) << " U= " << commandSequence[0]<< endl;
	//cout << "MidBox " << (middleOfBox((*itmax).startPoint[0], (*itmax).splits[0])) << " for SP " << (*itmax).startPoint[0] << " spilts " << (*itmax).splits[0] <<endl;
	cout << "SOOP main return done max length seq: "<< maxLengthSequence<< endl;
#endif

	freeSoopDataList();
	return commandSequence;
}

void printSoopData()
{

	ofstream myfile;
	myfile.open ("soopAllSimulationC.txt", std::ofstream::out | std::ofstream::app);
	myfile  << "------------" << endl;
	myfile.precision(4);
	myfile.setf( std::ios::fixed, std:: ios::floatfield );

	for (std::list<soopDataSet>::iterator Ui = data.begin(); Ui != data.end(); ++Ui)
	{
		unsigned int T = (*Ui).sequenceLenght;
		// splits
		for (unsigned int i=0 ; i < T; i++)
		{
			myfile << (*Ui).splits[i] << " ";
		}
		myfile << endl;
		// centerpoint
		for (unsigned int i=0 ; i < T; i++)
		{
			myfile <<  (*Ui).startPoint[i] << " ";
		}
		myfile << endl;

		// reward
		myfile.precision(8);
		for (unsigned int i=0 ; i < T; i++)
		{
			myfile << (*Ui).trajectory[i].reward << " ";
		}
		myfile << endl;
		myfile.precision(4);
		
		//commnad
		/*
		for (int i=0 ; i < T; i++)
		{
		myfile <<unnormU(middleOfBox((*Ui).startPoint[i], (*Ui).splits[i])) << " ";
		}
		myfile << endl;
		*/


		for (unsigned int i=0 ; i < T; i++)
		{
			for (unsigned int j=0 ; j < 4; j++)
			{
				myfile << (*Ui).trajectory[i].variables[j] << " ";
			}
			myfile << endl;
		}
		myfile  << endl;
	}
	myfile  << endl;		
	myfile.close();
}

double * OPC(stateE& s0)
{ // SOOP algorithm, first element of data list has the highest rewardSum


	bool stopFlag = false;
	unsigned int counter = 0;
	unsigned int currentBudget = 0;
	unsigned int listLenght = 0;
	unsigned int maxLengthSequence = 0;
	bool newDimensionAdded = false;
	//	bool reject;
	double B_max = 0.0, B_current = 0.0, uMidle = 0.0;
	double uLmidle, uRmidle, uCmidle;
	unsigned int stepTmp, pozMaxDimSc = 0;;
	double * commandSequence = (double*)malloc(sizeof(double)*initlen);
	stateE newLstateE, newRstateE, newCstateE;

	list <list<soopDataSet>::iterator> toExpand;
	//list<soopDataSet> toExpand;
	//list <list<soopDataSet>::iterator> toExpandIterator;

	/*==========================================================
	=====================INIT BOXES=============================
	*///========================================================

	for (int i=0;i<3;i++)
	{
		double sPtmp[initlen] = {0};
		unsigned int stmp[initlen] = {0};
		stateE newstateE[initlen];
		sPtmp[0] = ((double)i)/3.0;
		stmp[0] = 1;
		copyStateE(newstateE[0], nextstateE(s0, unnormU(middleOfBox(sPtmp[0], stmp[0])) ));
		soopDataSet newData;
		fillSoopData(newData,sPtmp, stmp, newstateE,newstateE[0].reward, 1);
		newData.sequenceLenght = 1;
		data.push_back(newData);

		//cout << endl << i << ": " << s0.variables[0] << " " << s0.variables[1] << " " << s0.variables[2] << " " << s0.variables[3] << endl;
		//cout << sPtmp[0] << " " << stmp[0] << " " <<unnormU(middleOfBox(sPtmp[0], stmp[0]))  << endl;
		//cout << newstateE[0].variables[0] << " " << newstateE[0].variables[1] << " " <<  newstateE[0].variables[2] << " " << newstateE[0].variables[3] << endl<< endl;

		//printSoopData();
		//delete[] newstateE;
	}	

	while (! stopFlag)         // main loop
	{

#ifdef DEBUG_ALGORITHM
		printSoopData();
#endif // DEBUG_ALGORITHM		

		ofstream myfile;
		counter++;
		/*==========================================================
		=====================Select BOXES to EXP====================
		*///========================================================
		listLenght = data.size();

		std::list<soopDataSet>::iterator toExpand;
		B_max = 0.0, 
			B_current = 0.0;
		for (std::list<soopDataSet>::iterator Ui = data.begin(); Ui != data.end(); ++Ui)
		{
			B_current = calcualteB_OPC((*Ui));
			if ( B_max < B_current )
			{
				toExpand = Ui;
				B_max = B_current;				
			}
		}

		/*==========================================================
		=====================Select dim of BOXES & EXP==============
		*///========================================================		
				
		pozMaxDimSc = dimentionSelectionOPC((*toExpand).sequenceLenght, (*toExpand).splits);
		if (pozMaxDimSc > maxLengthSequence )
		{
			newDimensionAdded = true;
		}

		/*==========================================================
		=====================SPLIT DIM==============================
		*///========================================================

		soopDataSet leftSegment;// = new soopDataSet;
		soopDataSet rightSegment;// = new soopDataSet;
		fillSoopData(leftSegment, (*toExpand).startPoint,(*toExpand).splits,(*toExpand).trajectory,(*toExpand).rewardSum,(*toExpand).sequenceLenght);
		fillSoopData(rightSegment,(*toExpand).startPoint,(*toExpand).splits,(*toExpand).trajectory,(*toExpand).rewardSum,(*toExpand).sequenceLenght);

		(*toExpand).splits[pozMaxDimSc] = (*toExpand).splits[pozMaxDimSc] + 1;
		leftSegment.splits[pozMaxDimSc] = (*toExpand).splits[pozMaxDimSc];
		rightSegment.splits[pozMaxDimSc] = (*toExpand).splits[pozMaxDimSc];
		// startPoint setup L/R/O
		leftSegment.startPoint[pozMaxDimSc] = (*toExpand).startPoint[pozMaxDimSc];
		(*toExpand).startPoint[pozMaxDimSc] = leftSegment.startPoint[pozMaxDimSc] + pow(1.0/3.0, (int)leftSegment.splits[pozMaxDimSc]);
		rightSegment.startPoint[pozMaxDimSc] = (*toExpand).startPoint[pozMaxDimSc] + pow(1.0/3.0, (int)leftSegment.splits[pozMaxDimSc]);
		leftSegment.sequenceLenght = (*toExpand).sequenceLenght;
		rightSegment.sequenceLenght = (*toExpand).sequenceLenght;

		if (pozMaxDimSc >= (*toExpand).sequenceLenght -1 )//((*it).splits[pozMaxDimSc] == 1 || (*it).splits[pozMaxDimSc+1] == 0) //split last or new dimension
		{
			//setup L/R/O splits count

			if (pozMaxDimSc == (*toExpand).sequenceLenght) // split new dim
			{   // model simulate LRO

				currentBudget = currentBudget + 3;

				//double 
				uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
				//stateE 
				newLstateE = nextstateE((*toExpand).trajectory[pozMaxDimSc-1], unnormU(uMidle));
				memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

				uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
				//stateE  
				newRstateE = nextstateE((*toExpand).trajectory[pozMaxDimSc-1], unnormU(uMidle));
				memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;

				uMidle = middleOfBox((*toExpand).startPoint[pozMaxDimSc], (*toExpand).splits[pozMaxDimSc]);
				//stateE  
				newCstateE = nextstateE((*toExpand).trajectory[pozMaxDimSc-1], unnormU(uMidle));
				memcpy((*toExpand).trajectory[pozMaxDimSc].variables, newCstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				(*toExpand).trajectory[pozMaxDimSc].reward =  newCstateE.reward;

				(*toExpand).sequenceLenght++;
				leftSegment.sequenceLenght = (*toExpand).sequenceLenght;
				rightSegment.sequenceLenght = (*toExpand).sequenceLenght;


			}
			else // split last dim.
			{ // model simulate LR
				currentBudget = currentBudget + 2;

				if (pozMaxDimSc == 0 )
				{ // split first dim => simulate with s0 new state // setup trajectory  L/R, O is the same
					//double 
					uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
					//stateE 
					newLstateE = nextstateE(s0, unnormU(uMidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
					//stateE  
					newRstateE =nextstateE(s0,unnormU(uMidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;

				}
				else
				{ // split last dim,  it is not the first dim.
					//double 
					uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
					//stateE 
					newLstateE = nextstateE((*toExpand).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
					//stateE  
					newRstateE =nextstateE((*toExpand).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
				}
			}
		}
		else // split dimension and recalculate trajectories
		{

			//double uLmidle, uRmidle, uCmidle;
			uLmidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc],leftSegment.splits[pozMaxDimSc]);
			uCmidle = middleOfBox((*toExpand).startPoint[pozMaxDimSc],(*toExpand).splits[pozMaxDimSc]);
			uRmidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc],rightSegment.splits[pozMaxDimSc]);

			if (pozMaxDimSc == 0)
			{
				//stateE 
				newLstateE = nextstateE(s0, unnormU(uLmidle));
				memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

				//stateE  
				newRstateE =nextstateE(s0,unnormU(uRmidle));
				memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
			}
			else
			{
				//stateE 
				newLstateE = nextstateE((*toExpand).trajectory[pozMaxDimSc-1], unnormU(uLmidle));
				memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

				//stateE  
				newRstateE =nextstateE((*toExpand).trajectory[pozMaxDimSc-1], unnormU(uRmidle));
				memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
			}
			currentBudget = currentBudget +2;
			//int
			stepTmp=pozMaxDimSc + 1;

			while (stepTmp < (*toExpand).sequenceLenght)//(*it).splits[stepTmp]!=0 || stepTmp>=initlen) //recalculate trajectory for left, right, center segments
			{
				//cout << "while2 at: " << stepTmp << " "<< (*it).sequenceLenght <<endl;
				stateE newLstateE;// = new stateE;
				stateE newRstateE;// = new stateE;
				uLmidle = middleOfBox((*toExpand).startPoint[stepTmp],(*toExpand).splits[stepTmp]);
				uRmidle = middleOfBox((*toExpand).startPoint[stepTmp],(*toExpand).splits[stepTmp ]);
				// stateE newCstateE;// = new stateE;

				newLstateE =  nextstateE(leftSegment.trajectory[stepTmp-1], unnormU(uLmidle));
				newRstateE =  nextstateE(rightSegment.trajectory[stepTmp-1], unnormU(uRmidle));
				//newCstateE =  nextstateE((*it).trajectory[stepTmp-1], unnormU(uCmidle));

				memcpy(leftSegment.trajectory[stepTmp].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				leftSegment.trajectory[stepTmp].reward = newLstateE.reward;

				memcpy(rightSegment.trajectory[stepTmp].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				rightSegment.trajectory[stepTmp].reward = newRstateE.reward;

				stepTmp++;
				currentBudget = currentBudget + 2;


			}
			//currentBudget=currentBudget+(stepTmp-pozMaxDimSc)*2;

		}
		leftSegment.rewardSum = recalculateRewardSum(leftSegment.trajectory, leftSegment.sequenceLenght);
		rightSegment.rewardSum = recalculateRewardSum(rightSegment.trajectory, rightSegment.sequenceLenght);

		data.push_back(leftSegment);
		data.push_back(rightSegment);




		if (currentBudget > budget) stopFlag = true;

#ifdef DEBUG_SPLIT
		//data.sort(compareRewardSum());
		//ofstream myfile;
		myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
		myfile << "list of SQ NOT EXP:" << data.size() << " loop "<< counter <<endl;
		myfile.close();
		for (std::list<soopDataSet>::iterator itTMP=data.begin(); itTMP != data.end(); ++itTMP)
		{
			myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
			(*itTMP).rewardSum = recalculateRewardSum((*itTMP).trajectory, (*itTMP).sequenceLenght);
			// cout << "RS " << (*itTMP).rewardSum << " ";
			myfile << "RS " << (*itTMP).rewardSum <<" sqLen " << (*itTMP).sequenceLenght;
			myfile.close();
			for (int itmp = 0; itmp<(*itTMP).sequenceLenght;itmp++ )
			{
				//if ((*itTMP).splits[itmp] != 0.0)
				{
					myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
					//cout  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp] <<" ";
					myfile  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp]  ;
					myfile.close();
					// printStateE((*itTMP).trajectory[itmp]);
					printStatusToFile((*itTMP).trajectory[itmp]);
				}
			}
			//cout<<endl;
		}
#endif
		if (newDimensionAdded) maxLengthSequence++;
	}       //END while loop

	//update max length of sequences


	data.sort(compareRewardSum());
	list<soopDataSet>::iterator itmax = data.begin();


	//double  commandSequence[initlen];
	//commandSequence[0] = (*itmax).sequenceLenght;

	for (int i=0;i<initlen;i++)
	{
		commandSequence[i] = unnormU(middleOfBox((*itmax).startPoint[i], (*itmax).splits[i]));
	}

	//printf("%d, ", (*itmax).sequenceLenght);
#ifdef DEBUG_COMMAND
	cout<< "RS " <<(*itmax).rewardSum<< " start= "<< (*itmax).startPoint[0] <<" split " << (*itmax).splits[0]<< " seqLen " << (*itmax).sequenceLenght  <<" command= " << middleOfBox((*itmax).startPoint[0], (*itmax).splits[0]) << " U= " << commandSequence[0]<< endl;
	//cout << "MidBox " << (middleOfBox((*itmax).startPoint[0], (*itmax).splits[0])) << " for SP " << (*itmax).startPoint[0] << " spilts " << (*itmax).splits[0] <<endl;
	cout << "SOOP main return done max length seq: "<< maxLengthSequence<< endl;
#endif

	freeSoopDataList();
	return commandSequence;
}

// SOPC variables --- need to bemoved to the top!

list<soopDataSet>::iterator depthLinks[1000];  // the vector elements point to the first element at depth = array index; note the list is order based on the depths; depth  =  sum of all splits 

// init to the end of data 
void init_depthLinks_array()
{
	for (int i = 0; i < 1000; i++)
	{
		depthLinks[i] = data.end();
	}
}

// = H_max(ns) = ns^0.45
int H_max(int ns)
{		
	return (int)pow((double)ns, 0.45);
}

int H_max_n(int ns) //ns not used
{		
	return (int)pow((double)budget, 1.0/3.0);
}

double * sopc(stateE& s0)
{ 
	//TODO:: check if true: SOPC algorithm, first element of data list has the highest rewardSum ???

	// maybe not nececarry, check before REAL experiments	
	unsigned int H_0 = 2;	// general starting depth for searching node to expand

	bool stopFlag = false;
	unsigned int counter = 0;
	unsigned int currentBudget = 3;
	unsigned int currentMaxDepth = 1;
	unsigned int listLenght = 0;
	unsigned int maxLengthSequence = 0;

	/*==========================================================
	=====================INIT BOXES=============================
	*///========================================================
	for (int i = 0; i<3; i++)
	{
		double sPtmp[initlen] = { 0 };
		unsigned int stmp[initlen] = { 0 };
		stateE newstateE[initlen];
		sPtmp[0] = ((double)i) / 3.0;
		stmp[0] = 1;
		newstateE[0] = nextstateE(s0, unnormU(middleOfBox(sPtmp[0], stmp[0])));
		soopDataSet newData;
		fillSoopData(newData, sPtmp, stmp, newstateE, newstateE[0].reward, 1);
		newData.sequenceLenght = 1;
		data.push_front(newData);
	}
	bool newDimensionAdded = false;
	init_depthLinks_array();
	depthLinks[1] = data.begin();
	

#ifdef DEBUG_TIME
	double timeBoxSelect = 0.0, timeDimSelect = 0.0, timeSplit = 0.0;
	int nrTimeSamples = 0;
#endif

	while (!stopFlag)         // main loop
	{
		ofstream myfile;
		counter++;

		if (currentMaxDepth == 1)
		{
			H_0 = 1;				
		}
		else
			H_0 = 2;
		
		list<soopDataSet>::iterator currentDepth_;
		// for each depth select a box to expand 
		for (int depthIter = H_0; depthIter <= H_max(currentBudget); depthIter++)
		{
			if (depthLinks[depthIter] == data.end())
			{
				//cerr << "Non exisiting dept link accessed at " << depthIter << endl;
				continue;
				//exit(-1);
			}
	
			currentDepth_ = depthLinks[depthIter];
			soopDataSet toExpand;
			list<soopDataSet>::iterator maxIter_ = currentDepth_;
			double maxR_ = currentDepth_->rewardSum;
			++currentDepth_;
			//=====================Select 1 BOXES at a DEPTH to EXP====================
			
			for (list<soopDataSet>::iterator it = currentDepth_; it != data.end(); ++it)
			{
				if (it->splitsSum > depthIter)
					break;
				if (maxR_ < it->rewardSum)
				{
					maxIter_ = it;
					maxR_ = it->rewardSum;
				}
			}

			toExpand = (*maxIter_);

			if (maxIter_ == depthLinks[depthIter])
			{
				depthLinks[depthIter] = ++maxIter_;

				if (maxIter_->splitsSum > depthIter)
					depthLinks[depthIter] = data.end();

				data.erase(--maxIter_);
			}
			else
				data.erase(maxIter_);

			//=====================SELECT DIMENTION to exp====================
			unsigned int pozMaxDimSc = dimentionSelectionSOPC(toExpand.sequenceLenght, toExpand.splits);			
			if (pozMaxDimSc > maxLengthSequence)
				newDimensionAdded = true;
			
			//=====================EXPAND a box====================
			soopDataSet leftSegment;// = new soopDataSet;
			soopDataSet rightSegment;// = new soopDataSet;
			fillSoopData(leftSegment, toExpand.startPoint, toExpand.splits, toExpand.trajectory, toExpand.rewardSum, toExpand.sequenceLenght);
			fillSoopData(rightSegment, toExpand.startPoint, toExpand.splits, toExpand.trajectory, toExpand.rewardSum, toExpand.sequenceLenght);

			toExpand.splits[pozMaxDimSc] = toExpand.splits[pozMaxDimSc] + 1;
			leftSegment.splits[pozMaxDimSc] = toExpand.splits[pozMaxDimSc];
			rightSegment.splits[pozMaxDimSc] = toExpand.splits[pozMaxDimSc];
			// startPoint setup L/R/O
			leftSegment.startPoint[pozMaxDimSc] = toExpand.startPoint[pozMaxDimSc];
			toExpand.startPoint[pozMaxDimSc] = leftSegment.startPoint[pozMaxDimSc] + pow(1.0 / 3.0, (int)leftSegment.splits[pozMaxDimSc]);
			rightSegment.startPoint[pozMaxDimSc] = toExpand.startPoint[pozMaxDimSc] + pow(1.0 / 3.0, (int)leftSegment.splits[pozMaxDimSc]);
			leftSegment.sequenceLenght = toExpand.sequenceLenght;
			rightSegment.sequenceLenght = toExpand.sequenceLenght;

			if (pozMaxDimSc >= toExpand.sequenceLenght - 1)//(toExpand.splits[pozMaxDimSc] == 1 || toExpand.splits[pozMaxDimSc+1] == 0) //split last or new dimension
			{
				//setup L/R/O splits count

				if (pozMaxDimSc == toExpand.sequenceLenght) // split new dim
				{   // model simulate LRO

					currentBudget = currentBudget + 3;

					double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
					stateE newLstateE = nextstateE(toExpand.trajectory[pozMaxDimSc - 1], unnormU(uMidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward = newLstateE.reward;

					uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
					stateE  newRstateE = nextstateE(toExpand.trajectory[pozMaxDimSc - 1], unnormU(uMidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward = newRstateE.reward;

					uMidle = middleOfBox(toExpand.startPoint[pozMaxDimSc], toExpand.splits[pozMaxDimSc]);
					stateE  newCstateE = nextstateE(toExpand.trajectory[pozMaxDimSc - 1], unnormU(uMidle));
					memcpy(toExpand.trajectory[pozMaxDimSc].variables, newCstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					toExpand.trajectory[pozMaxDimSc].reward = newCstateE.reward;

					toExpand.sequenceLenght++;
					leftSegment.sequenceLenght = toExpand.sequenceLenght;
					rightSegment.sequenceLenght = toExpand.sequenceLenght;


				}
				else // split last dim.
				{ // model simulate LR
					currentBudget = currentBudget + 2;

					if (pozMaxDimSc == 0)
					{ // split first dim => simulate with s0 new state // setup trajectory  L/R, O is the same
						double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
						stateE newLstateE = nextstateE(s0, unnormU(uMidle));
						memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						leftSegment.trajectory[pozMaxDimSc].reward = newLstateE.reward;

						uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
						stateE  newRstateE = nextstateE(s0, unnormU(uMidle));
						memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						rightSegment.trajectory[pozMaxDimSc].reward = newRstateE.reward;

					}
					else
					{ // split last dim,  it is not the first dim.
						double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
						stateE newLstateE = nextstateE(toExpand.trajectory[pozMaxDimSc - 1], unnormU(uMidle));
						memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						leftSegment.trajectory[pozMaxDimSc].reward = newLstateE.reward;

						uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
						stateE  newRstateE = nextstateE(toExpand.trajectory[pozMaxDimSc - 1], unnormU(uMidle));
						memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						rightSegment.trajectory[pozMaxDimSc].reward = newRstateE.reward;
					}
				}
			}
			else // split dimension and recalculate trajectories
			{

				double uLmidle, uRmidle, uCmidle;
				uLmidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
				uCmidle = middleOfBox(toExpand.startPoint[pozMaxDimSc], toExpand.splits[pozMaxDimSc]);
				uRmidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);

				if (pozMaxDimSc == 0)
				{
					stateE newLstateE = nextstateE(s0, unnormU(uLmidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward = newLstateE.reward;

					stateE  newRstateE = nextstateE(s0, unnormU(uRmidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward = newRstateE.reward;
				}
				else
				{
					stateE newLstateE = nextstateE(toExpand.trajectory[pozMaxDimSc - 1], unnormU(uLmidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward = newLstateE.reward;

					stateE  newRstateE = nextstateE(toExpand.trajectory[pozMaxDimSc - 1], unnormU(uRmidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward = newRstateE.reward;
				}
				currentBudget = currentBudget + 2;
				unsigned int stepTmp = pozMaxDimSc + 1;

				while (stepTmp < toExpand.sequenceLenght)//toExpand.splits[stepTmp]!=0 || stepTmp>=initlen) //recalculate trajectory for left, right, center segments
				{
					//cout << "while2 at: " << stepTmp << " "<< toExpand.sequenceLenght <<endl;
					stateE newLstateE;// = new stateE;
					stateE newRstateE;// = new stateE;
					uLmidle = middleOfBox(toExpand.startPoint[stepTmp], toExpand.splits[stepTmp]);
					uRmidle = middleOfBox(toExpand.startPoint[stepTmp], toExpand.splits[stepTmp]);
					// stateE newCstateE;// = new stateE;

					//{
					newLstateE = nextstateE(leftSegment.trajectory[stepTmp - 1], unnormU(uLmidle));
					newRstateE = nextstateE(rightSegment.trajectory[stepTmp - 1], unnormU(uRmidle));
					//newCstateE =  nextstateE(toExpand.trajectory[stepTmp-1], unnormU(uCmidle));
					//}
					memcpy(leftSegment.trajectory[stepTmp].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[stepTmp].reward = newLstateE.reward;

					memcpy(rightSegment.trajectory[stepTmp].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[stepTmp].reward = newRstateE.reward;

					stepTmp++;
					currentBudget = currentBudget + 2;
				}
			}
			
			toExpand.rewardSum = recalculateRewardSum(toExpand.trajectory, toExpand.sequenceLenght);
			toExpand.splitsSum += 1;
			leftSegment.rewardSum = recalculateRewardSum(leftSegment.trajectory, leftSegment.sequenceLenght);
			leftSegment.splitsSum += 1;
			rightSegment.rewardSum = recalculateRewardSum(rightSegment.trajectory, rightSegment.sequenceLenght);
			rightSegment.splitsSum += 1;
			//=====================INSERT the expanded box at good depth range!====================
			
			

			if (depthLinks[toExpand.splitsSum] != data.end() )
			{
				list<soopDataSet>::iterator it_tmp = depthLinks[toExpand.splitsSum];
				++it_tmp;
				data.insert(it_tmp, toExpand);
				data.insert(it_tmp, leftSegment);
				data.insert(it_tmp, rightSegment);				
			}
			else {
				if (toExpand.splitsSum > currentMaxDepth)
				{
					data.push_back(toExpand);
					depthLinks[toExpand.splitsSum] = --data.end();
					data.push_back(leftSegment);
					data.push_back(rightSegment);
				}
				else
				{
					list<soopDataSet>::iterator it_tmp = depthLinks[toExpand.splitsSum + 1];
					data.insert(it_tmp, toExpand);
					data.insert(it_tmp, leftSegment);
					data.insert(it_tmp, rightSegment);
				}
			}
			// update max depth counter 
			if (currentMaxDepth < toExpand.splitsSum)
				currentMaxDepth = toExpand.splitsSum;
		}
		
		if (currentBudget > budget) stopFlag = true;
		if (newDimensionAdded) maxLengthSequence++;
	}       //END while loop

#ifdef DEBUG_TIME
	cout << "box select= " << timeBoxSelect / nrTimeSamples << " ,dimSelect= " << timeDimSelect / nrTimeSamples << " ,split= " << timeSplit / nrTimeSamples << " ,n= " << nrTimeSamples;

	__int64 frequencyS, startS, endtS, totalS;
	QueryPerformanceFrequency((LARGE_INTEGER *)&frequencyS);
	QueryPerformanceCounter((LARGE_INTEGER *)&startS);
#endif
	//update max length of sequences



	double maxRewardSum = 0.0, commandSignalToSend = 0.0;
	for (std::list<soopDataSet>::iterator itTMP = data.begin(); itTMP != data.end(); ++itTMP)
	{
		if ((*itTMP).rewardSum > maxRewardSum)
		{
			commandSignalToSend = unnormU(middleOfBox((*itTMP).startPoint[0], (*itTMP).splits[0]));
			maxRewardSum = (*itTMP).rewardSum;
		}
	}
	//cout << "myserach RS max " << maxRewardSum << " U " << commandSignalToSend << endl;


	data.sort(compareRewardSum());
	list<soopDataSet>::iterator itmax = data.begin();

	double * commandSequence = (double*)malloc(sizeof(double)*initlen);

	for (int i = 0; i<initlen; i++)
	{
		commandSequence[i] = unnormU(middleOfBox((*itmax).startPoint[i], (*itmax).splits[i]));
	}

	freeSoopDataList();

#ifdef DEBUG_TIME
	QueryPerformanceCounter((LARGE_INTEGER *)&endtS);
	double timeEnd = (endtS - startS) * 1000.0 / frequencyS;
	cout << " ,end=" << timeEnd << endl;
#endif

	return commandSequence;
}

=======
//#include "stdafx.h"
#include "testerART.h"

//#ifndef DEBUG_H
#define DEBUG_//TIME
#define DEBUG_//TO_EXTAND
#define DEBUG_//EXTANDED
#define DEBUG_//DIM_SELECT
#define DEBUG_//SPLIT
#define DEBUG_//COMMAND
#define DEBUG_//ALGORITHM

using namespace std;

unsigned int initsize = (unsigned int)floor(1.1 * budget + 0.5);           //Rough upper bound on #seq
//discount factor for dimselect='disc'

typedef struct {
	double startPoint [initlen];       //starting points of U domains, for each depth and sequence
	unsigned int splits [initlen];     //number of splits in each sequences element
	stateE trajectory [initlen];       // stateE trajectory for center action samples: dimx X and # of sequences also,
	unsigned int sequenceLenght ;//= 0;                                   // rewards RESULTING from center action samples: # of sequences
	//double reward [initlen];         // rewards RESULTING from center action samples: # of sequences
	double rewardSum;                  // the sum of rewards in the sequence
	int splitsSum;						// the sum of splits denoted by "h"	
}soopDataSet;

list<soopDataSet> data;

#ifdef DEBUG_TIME
//long frequency;        // ticks per second
//long t1, t2;           // ticks
double elapsedTime, sumTime = 0.0, sumDim = 0.0, sumSplit = 0.0;
#endif // DEBUG_TIME

struct compareRewardSum {
	bool operator()(soopDataSet  a, soopDataSet  b) {
		return a.rewardSum > b.rewardSum;
	}
};
// ========================= END declarations =========================


// Unnormalize control signal from [0..1] to [minU..maxU]
double unnormU(double u) //OK
{
	//double maxU=maxVoltage, minU=-maxVoltage;
	return (u-0.5)*2.0*maxVoltage; //u*((double)maxVoltage*2.0)-(double)maxVoltage;
}

void freeSoopDataList()
{
	//std::list<soopDataSet>::iterator it = data.begin();
	data.erase(data.begin(),data.end());
}

void initSoopData(soopDataSet& newData) //OK
{
	//soopDataSet*
	//    newData = new soopDataSet; //(soopDataSet*)malloc(sizeof(soopDataSet));
	//fill_n(newData.reward, initlen, 0);
	fill_n(newData.splits, initlen, 0);
	fill_n(newData.startPoint, initlen, 0);
	newData.sequenceLenght= 0;
	newData.rewardSum = 0.0;
	for (int i=0;i<initlen;i++)
	{
		//newData.trajectory[i] = new stateE;
		//        newData.trajectory[i].variables = (double*)malloc(sizeof(double) * nbHiddenstateEVariables);
		newData.trajectory[i].isTerminal = 0;
		newData.trajectory[i].reward = 0;
	}
	//return newData;
}

void fillSoopData(soopDataSet& destination, double *sP, unsigned int *s, stateE t[],/* double *r,*/ double rS, int sequneceLenght)
{
	double newRewardSum = 0.0;
	int splitSum = 0;
	initSoopData(destination);

	for (int i=0;i<sequneceLenght;i++)
	{
		splitSum += s[i];
		destination.splits[i] = s[i];
		destination.startPoint[i] = sP[i];
		memcpy(destination.trajectory[i].variables, t[i].variables, sizeof(double) * nbHiddenstateEVariables);
		destination.trajectory[i].reward =  t[i].reward;
		newRewardSum = newRewardSum + pow(gamma,(double)i)*t[i].reward;
		
	}
	destination.splitsSum = splitSum;
	destination.rewardSum = newRewardSum;
}

bool partialyGraterBox(soopDataSet a, soopDataSet b)  //OK for 1 dim check
{ // is Ua partially grater then Ub ?
	bool partialyGrater = true;


	for(unsigned int i=0; i<a.sequenceLenght; i++)
	{
		//cout << i << endl;
		if (a.splits[i] > b.splits[i] )
		{
			//cout << "a " << a.splits[i] << " b " << b.splits[i] << endl;
			partialyGrater = false;
			return partialyGrater;
		}
		//if (a.splits[i] == 0 && b.splits[i] == 0 ) break;
	}
	return partialyGrater;
}

double recalculateRewardSum(stateE t[], int nr)
{
	double newRewardSum = 0.0;
	for (int i = 0; i< nr;i++)
	{

		newRewardSum = newRewardSum + pow(gamma,(double)i)*t[i].reward;
	}
	return newRewardSum;
}

void printStateE(stateE s) //OK
{
	cout <<"alp " << s.variables[0] << " th " << s.variables[1] <<  " R " << s.reward << endl;
}

void printStatusToFile(stateE s0) //, list<soopDataSet>::iterator itmax, double commandSequence, double currentBudget, double maxLengthSequence, int seqNr) //OK  tester
{
	ofstream myfile;
	myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
	myfile  << "x " << ((s0).variables[0]) << " , " << ((s0).variables[1]) << " R " << s0.reward << endl; //" u " << commandSequence << " n " << (currentBudget) << " len_ret= " << ((*itmax).sequenceLenght) << " longest= " << (maxLengthSequence) <<" sq Nr. " << seqNr<< endl;
	myfile.close();
}

void printBool(bool *b, int listLenght) //OK tester
{
	for (int iTmp =0;iTmp<listLenght;iTmp++)
	{
		cout << boolalpha << b[iTmp] << " ";
	}
	cout << endl;
}

double middleOfBox(double startPoint, int splits) //OK
{	
	return startPoint + pow(1.0/3.0,((int)splits)) / 2.0;
}

// = alpha^dimention * (1/3)^#splits
double dimentionScore(int dimention, int splits) //OK
{
	return  pow(alpha,(int)dimention)*pow((1.0/3.0),(int)splits);
}

// = gamma^dimention * (1/3)^#splits
double dimentionScore_gamma(int dimention, int splits) //OK
{
	return  pow(gamma, (int)dimention)*pow((1.0 / 3.0), (int)splits);
}

// Cj = L_lp * gamma^i * (1/3)^#splits *(1/2) * min(1, 1-(gamma*L_lp)^(T-i)/(1-gamma*L_lp) )
double calcualteCj(int splits, int T, int index)
{
	double Cj = 0.0;
	int i = index;
	double partialCjKapping = min(( 1.0-pow(gamma*L_lp,(int)(T-i)) )/ (1.0-gamma*L_lp),1);
	Cj = L_lp * pow(gamma,(double)i) * pow(1.0/3.0,(int)splits)/2.0 * partialCjKapping;// min(partialCjKapping,1);//partialCjKapping;

	return Cj;
}

// = sum_i^T (gamma^i * r_i + Cj_i) + gamma^T/(1-gamma)
double calcualteB(soopDataSet d)
{
	double b = 0.0, ct = 0.0;
	int T  = d.sequenceLenght;

	ct = pow(gamma,T)/(1-gamma);

	for(int i = 0; i<T; i++)
	{
		b = b + pow(gamma,i)*d.trajectory[i].reward + calcualteCj(d.splits[i], T, i); //
	}

	b = b + ct;

	return b;
}

double calcualteB_OPC(soopDataSet d)
{
	double b = 0.0, ct = 0.0, cj = 0.0;
	int T  = d.sequenceLenght;

	ct = pow(gamma,T)/(1-gamma);

	for(int i = 0; i<T; i++)
	{
		if (d.splits[i] == 0)
		{
			ct = pow(gamma,i)/(1-gamma);
			d.sequenceLenght = i;
			perror("sequenceLength was longher than real length");			
			break;
		}

		cj = 0.0;
		for (int j = 0; j<i ; j++)
		{
			cj+=pow(L_lp,i-j+1)*pow(1.0/3.0,(int)d.splits[j])/2.0;
		}		

		b = b + pow(gamma,i)*min(1, (d.trajectory[i].reward) + cj);			
	}
	b = b + ct;
	return b;
}

int dimentionSelectionOPC(int length, unsigned int * splits)
{
	int pozMaxDimSc = 0;
	double MaxDimScore = 0.0;
	int current_Dim = 0;

	while (current_Dim < length)
	{
		if (current_Dim  >=  initlen)
		{
			perror("Current dimnetion exceeded the limit of the vector");

		}

#ifdef DEBUG_FN
		cout << dimentionScore(current_Dim, splits[current_Dim]) << ",  ";
#endif

		if (dimentionScore(current_Dim, splits[current_Dim])  >  MaxDimScore)
		{
			MaxDimScore = dimentionScore(current_Dim, splits[current_Dim]);
			pozMaxDimSc = current_Dim;
		}
		current_Dim++;

	}

	if (current_Dim >= initlen) // take into consideration the next empty dimension
	{
		perror("Current dimnetion exceeded the limit of the vector");

	}

#ifdef DEBUG_FN
	cout << dimentionScore(current_Dim, splits[current_Dim]) << endl;
#endif

	if (dimentionScore(current_Dim, splits[current_Dim])>  MaxDimScore)
	{
		MaxDimScore = dimentionScore(current_Dim, splits[current_Dim]);
		pozMaxDimSc = current_Dim;
	}		
#ifdef DEBUG_FN
	cout << " score: " <<  MaxDimScore << " poz= " <<pozMaxDimSc ;
#endif
	return pozMaxDimSc;
}

int dimentionSelectionSOPC(int length, unsigned int * splits)
{
	int pozMaxDimSc = 0;
	double MaxDimScore = 0.0;
	int current_Dim = 0;

	while (current_Dim < length)
	{
		if (current_Dim >= initlen)
		{
			perror("Current dimnetion exceeded the limit of the vector");

		}

#ifdef DEBUG_FN
		cout << dimentionScore(current_Dim, splits[current_Dim]) << ",  ";
#endif

		if (dimentionScore_gamma(current_Dim, splits[current_Dim])  >  MaxDimScore)
		{
			MaxDimScore = dimentionScore_gamma(current_Dim, splits[current_Dim]);
			pozMaxDimSc = current_Dim;
		}
		current_Dim++;

	}

	if (current_Dim >= initlen) // take into consideration the next empty dimension
	{
		perror("Current dimnetion exceeded the limit of the vector");

	}

#ifdef DEBUG_FN
	cout << dimentionScore(current_Dim, splits[current_Dim]) << endl;
#endif

	if (dimentionScore_gamma(current_Dim, splits[current_Dim])>  MaxDimScore)
	{
		MaxDimScore = dimentionScore(current_Dim, splits[current_Dim]);
		pozMaxDimSc = current_Dim;
	}
#ifdef DEBUG_FN
	cout << " score: " << MaxDimScore << " poz= " << pozMaxDimSc;
#endif
	return pozMaxDimSc;
}

int dimentionSelectionOPC_lp(int length, unsigned int * splits)
{
	int pozMaxDimSc = 0;
	double MaxDimScore = 0.0, cj_tmp = 0.0;	

	for (int i = 0; i <= length; i++)
	{
		cj_tmp = calcualteCj(splits[i],  length, i);
		if (MaxDimScore < cj_tmp)
		{
			MaxDimScore = cj_tmp;
			pozMaxDimSc = i;
		}
	}	

	return pozMaxDimSc;
}

double * soop(stateE& s0)
{ // SOOP algorithm, first element of data list has the highest rewardSum


	bool stopFlag = false;
	unsigned int counter = 0;
	unsigned int currentBudget = 0;	
	unsigned int listLenght = 0;
	unsigned int maxLengthSequence = 0;
	
	/*==========================================================
	=====================INIT BOXES=============================
	*///========================================================
	for (int i=0;i<3;i++)
	{
		double sPtmp[initlen] = {0};
		unsigned int stmp[initlen] = {0};
		stateE newstateE[initlen];
		sPtmp[0] = ((double)i)/3.0;
		stmp[0] = 1;
		newstateE[0] = nextstateE(s0, unnormU(middleOfBox(sPtmp[0], stmp[0])) );
		soopDataSet newData;
		fillSoopData(newData,sPtmp, stmp, newstateE,newstateE[0].reward, 1);
		newData.sequenceLenght = 1;
		data.push_front(newData);
	}
	bool newDimensionAdded = false;

#ifdef DEBUG_TIME
	double timeBoxSelect = 0.0, timeDimSelect = 0.0, timeSplit = 0.0;
	int nrTimeSamples = 0;		
#endif

	while (! stopFlag)         // main loop
	{
		ofstream myfile;
		counter++;
		/*==========================================================
		=====================Select BOXES to EXP====================
		*///========================================================
#ifdef DEBUG_TIME	
		nrTimeSamples++;
		__int64 frequencyBS, startBS, endtBS, totalBS;
		QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyBS);
		QueryPerformanceCounter ( (LARGE_INTEGER *)&startBS);  
#endif

		listLenght = data.size();
		list<soopDataSet> toExpand;

		list <list<soopDataSet>::iterator> toExpandIterator;
		bool reject;
		for (std::list<soopDataSet>::iterator Ui = data.begin(); Ui != data.end(); ++Ui)
		{

			//(*Ui).rewardSum = recalculateRewardSum((*Ui).trajectory, (*Ui).sequenceLenght);
			reject = false;

			for ( list<soopDataSet>::iterator Uj = data.begin();  Uj != data.end(); ++Uj)
			{
				//(*Uj).rewardSum = recalculateRewardSum((*Uj).trajectory, (*Ui).sequenceLenght);

				if (Uj != Ui)
				{
					if ((*Uj).sequenceLenght == (*Ui).sequenceLenght)
					{
						if (partialyGraterBox((*Uj),(*Ui)))
						{
							if ((*Uj).rewardSum > (*Ui).rewardSum ) //+1e-5 in case of faulty calculus
							{
								reject = true;
								break;
							}
						}
					}
				}
			}

			if (!reject)
			{
				toExpandIterator.push_back(Ui);
			}

		}

		for (list <list<soopDataSet>::iterator>::iterator it = toExpandIterator.begin(); it!= toExpandIterator.end(); ++it)
		{
			toExpand.splice(toExpand.end(),data,(*it));
		}

		toExpandIterator.erase(toExpandIterator.begin(),toExpandIterator.end());



#ifdef DEBUG_TIME
		QueryPerformanceCounter((LARGE_INTEGER *)&endtBS);
		timeBoxSelect = timeBoxSelect + (endtBS - startBS) * 1000.0 / frequencyBS;
		//cour << "boxSelect=  " << timeBoxSelect << endl;
#endif
		/*==========================================================
		=====================Select dim of BOXES & EXP==============
		*///========================================================


		for (std::list<soopDataSet>::iterator it=toExpand.begin(); it != toExpand.end(); ++it)
		{
#ifdef DEBUG_TIME		
			__int64 frequencyDS, startDS, endtDS, totalDS;
			QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyDS);
			QueryPerformanceCounter ( (LARGE_INTEGER *)&startDS);  
#endif
			//dimension selection
			unsigned int pozMaxDimSc =  dimentionSelectionOPC((*it).sequenceLenght, (*it).splits);


			if (pozMaxDimSc > maxLengthSequence )
			{
				newDimensionAdded = true;

			}
			//ofstream myfile;
			//myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
			//myfile << "while NR "<< counter <<" pozMAx " << pozMaxDimSc <<" size toEx " << toExpand.size() << endl;
			//myfile.close();
#ifdef DEBUG_TIME
			QueryPerformanceCounter((LARGE_INTEGER *)&endtDS);
			timeDimSelect = timeDimSelect + (endtDS - startDS) * 1000.0 / frequencyDS;
			//cour << "boxSelect=  " << timeBoxSelect << endl;
#endif
			/*==========================================================
			=====================SPLIT DIM==============================
			*///========================================================
#ifdef DEBUG_TIME		
			__int64 frequencyS, startS, endtS, totalS;
			QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyS);
			QueryPerformanceCounter ( (LARGE_INTEGER *)&startS);  
#endif

			soopDataSet leftSegment;// = new soopDataSet;
			soopDataSet rightSegment;// = new soopDataSet;
			fillSoopData(leftSegment, (*it).startPoint,(*it).splits,(*it).trajectory,(*it).rewardSum,(*it).sequenceLenght);
			fillSoopData(rightSegment,(*it).startPoint,(*it).splits,(*it).trajectory,(*it).rewardSum,(*it).sequenceLenght);

			(*it).splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc] + 1;
			leftSegment.splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc];
			rightSegment.splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc];
			// startPoint setup L/R/O
			leftSegment.startPoint[pozMaxDimSc] = (*it).startPoint[pozMaxDimSc];
			(*it).startPoint[pozMaxDimSc] = leftSegment.startPoint[pozMaxDimSc] + pow(1.0/3.0, (int)leftSegment.splits[pozMaxDimSc]);
			rightSegment.startPoint[pozMaxDimSc] = (*it).startPoint[pozMaxDimSc] + pow(1.0/3.0, (int)leftSegment.splits[pozMaxDimSc]);
			leftSegment.sequenceLenght = (*it).sequenceLenght;
			rightSegment.sequenceLenght = (*it).sequenceLenght;

			if (pozMaxDimSc >= (*it).sequenceLenght -1 )//((*it).splits[pozMaxDimSc] == 1 || (*it).splits[pozMaxDimSc+1] == 0) //split last or new dimension
			{
				//setup L/R/O splits count

				if (pozMaxDimSc == (*it).sequenceLenght) // split new dim
				{   // model simulate LRO

					currentBudget = currentBudget + 3;

					double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
					stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
					stateE  newRstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;

					uMidle = middleOfBox((*it).startPoint[pozMaxDimSc], (*it).splits[pozMaxDimSc]);
					stateE  newCstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy((*it).trajectory[pozMaxDimSc].variables, newCstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					(*it).trajectory[pozMaxDimSc].reward =  newCstateE.reward;

					(*it).sequenceLenght++;
					leftSegment.sequenceLenght = (*it).sequenceLenght;
					rightSegment.sequenceLenght = (*it).sequenceLenght;


				}
				else // split last dim.
				{ // model simulate LR
					currentBudget = currentBudget + 2;

					if (pozMaxDimSc == 0 )
					{ // split first dim => simulate with s0 new state // setup trajectory  L/R, O is the same
						double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
						stateE newLstateE = nextstateE(s0, unnormU(uMidle));
						memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

						uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
						stateE  newRstateE =nextstateE(s0,unnormU(uMidle));
						memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;

					}
					else
					{ // split last dim,  it is not the first dim.
						double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
						stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
						memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

						uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
						stateE  newRstateE =nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
						memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
					}
				}
			}
			else // split dimension and recalculate trajectories
			{

				double uLmidle, uRmidle, uCmidle;
				uLmidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc],leftSegment.splits[pozMaxDimSc]);
				uCmidle = middleOfBox((*it).startPoint[pozMaxDimSc],(*it).splits[pozMaxDimSc]);
				uRmidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc],rightSegment.splits[pozMaxDimSc]);

				if (pozMaxDimSc == 0)
				{
					stateE newLstateE = nextstateE(s0, unnormU(uLmidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					stateE  newRstateE =nextstateE(s0,unnormU(uRmidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
				}
				else
				{
					stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uLmidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					stateE  newRstateE =nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uRmidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
				}
				currentBudget = currentBudget +2;
				unsigned int stepTmp=pozMaxDimSc + 1;

				while (stepTmp < (*it).sequenceLenght)//(*it).splits[stepTmp]!=0 || stepTmp>=initlen) //recalculate trajectory for left, right, center segments
				{
					//cout << "while2 at: " << stepTmp << " "<< (*it).sequenceLenght <<endl;
					stateE newLstateE;// = new stateE;
					stateE newRstateE;// = new stateE;
					uLmidle = middleOfBox((*it).startPoint[stepTmp],(*it).splits[stepTmp]);
					uRmidle = middleOfBox((*it).startPoint[stepTmp],(*it).splits[stepTmp ]);
					// stateE newCstateE;// = new stateE;

					//{
					newLstateE =  nextstateE(leftSegment.trajectory[stepTmp-1], unnormU(uLmidle));
					newRstateE =  nextstateE(rightSegment.trajectory[stepTmp-1], unnormU(uRmidle));
					//newCstateE =  nextstateE((*it).trajectory[stepTmp-1], unnormU(uCmidle));
					//}
					memcpy(leftSegment.trajectory[stepTmp].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[stepTmp].reward = newLstateE.reward;

					memcpy(rightSegment.trajectory[stepTmp].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[stepTmp].reward = newRstateE.reward;

					stepTmp++;
					currentBudget = currentBudget + 2;


				}
				//currentBudget=currentBudget+(stepTmp-pozMaxDimSc)*2;

			}
#ifdef DEBUG_TIME
			QueryPerformanceCounter((LARGE_INTEGER *)&endtS);
			timeSplit = timeSplit + (endtS - startS) * 1000.0 / frequencyS;
			//cour << "boxSelect=  " << timeBoxSelect << endl;
#endif
			(*it).rewardSum = recalculateRewardSum((*it).trajectory, (*it).sequenceLenght);
			leftSegment.rewardSum = recalculateRewardSum(leftSegment.trajectory, leftSegment.sequenceLenght);
			rightSegment.rewardSum = recalculateRewardSum(rightSegment.trajectory, rightSegment.sequenceLenght);

			data.push_back(leftSegment);
			data.push_back(rightSegment);


		}
		data.splice(data.end(),toExpand);
		toExpand.erase(toExpand.begin(), toExpand.end());
		if (currentBudget > budget) stopFlag = true;


		if (newDimensionAdded) maxLengthSequence++;
	}       //END while loop

#ifdef DEBUG_TIME
	cout << "box select= " << timeBoxSelect/nrTimeSamples << " ,dimSelect= " << timeDimSelect/nrTimeSamples << " ,split= " << timeSplit/nrTimeSamples << " ,n= " << nrTimeSamples;

	__int64 frequencyS, startS, endtS, totalS;
	QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyS);
	QueryPerformanceCounter ( (LARGE_INTEGER *)&startS);  
#endif
	//update max length of sequences



	double maxRewardSum = 0.0, commandSignalToSend = 0.0;
	for (std::list<soopDataSet>::iterator itTMP=data.begin(); itTMP != data.end(); ++itTMP)
	{
		if ((*itTMP).rewardSum > maxRewardSum)
		{
			commandSignalToSend = unnormU(middleOfBox((*itTMP).startPoint[0], (*itTMP).splits[0]));
			maxRewardSum = (*itTMP).rewardSum;
		}
	}
	//cout << "myserach RS max " << maxRewardSum << " U " << commandSignalToSend << endl;


	data.sort(compareRewardSum());
	list<soopDataSet>::iterator itmax = data.begin();

	double * commandSequence = (double*)malloc(sizeof(double)*initlen);
	
	for (int i=0;i<initlen;i++)
	{
		commandSequence[i] = unnormU(middleOfBox((*itmax).startPoint[i], (*itmax).splits[i]));
	}

	freeSoopDataList();

#ifdef DEBUG_TIME
	QueryPerformanceCounter((LARGE_INTEGER *)&endtS);
	double timeEnd =  (endtS - startS) * 1000.0 / frequencyS;
	cout << " ,end=" << timeEnd << endl;
#endif

	return commandSequence;
}


double * soop_soo(stateE& s0)
 { // SOOP algorithm, first element of data list has the highest rewardSum


	bool stopFlag = false;
	unsigned int counter = 0;
	unsigned int currentBudget = 0;
	unsigned int listLenght = 0;
	unsigned int maxLengthSequence = 0;

	/*==========================================================
	=====================INIT BOXES=============================
	*///========================================================
	for (int i=0;i<3;i++)
	{
		double sPtmp[initlen] = {0};
		unsigned int stmp[initlen] = {0};
		stateE newstateE[initlen];
		sPtmp[0] = ((double)i)/3.0;
		stmp[0] = 1;
		newstateE[0] = nextstateE(s0, unnormU(middleOfBox(sPtmp[0], stmp[0])) );
		soopDataSet newData;
		fillSoopData(newData,sPtmp, stmp, newstateE,newstateE[0].reward, 1);
		newData.sequenceLenght = 1;
		newData.splitsSum = 1;
		data.push_front(newData);
	}
	bool newDimensionAdded = false;

#ifdef DEBUG_TIME
	double timeBoxSelect = 0.0, timeDimSelect = 0.0, timeSplit = 0.0;
	int nrTimeSamples = 0;		
#endif

	while (! stopFlag)         // main loop
	{
		ofstream myfile;
		counter++;
		/*==========================================================
		=====================Select BOXES to EXP====================
		*///========================================================
#ifdef DEBUG_TIME	
		nrTimeSamples++;
		__int64 frequencyBS, startBS, endtBS, totalBS;
		QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyBS);
		QueryPerformanceCounter ( (LARGE_INTEGER *)&startBS);  
#endif

		listLenght = data.size();
		list<soopDataSet> toExpand;

		list <list<soopDataSet>::iterator> toExpandIterator;
		bool reject;
		for (std::list<soopDataSet>::iterator Ui = data.begin(); Ui != data.end(); ++Ui)
		{

			//(*Ui).rewardSum = recalculateRewardSum((*Ui).trajectory, (*Ui).sequenceLenght);
			reject = false;

			for ( list<soopDataSet>::iterator Uj = data.begin();  Uj != data.end(); ++Uj)
			{
				//(*Uj).rewardSum = recalculateRewardSum((*Uj).trajectory, (*Ui).sequenceLenght);

				if (Uj != Ui)
				{
					if ((*Uj).sequenceLenght == (*Ui).sequenceLenght)
					{
						if ((Uj->splitsSum) >= (Ui->splitsSum))
						{
							if ((*Uj).rewardSum > (*Ui).rewardSum ) //+1e-5 in case of faulty calculus
							{
								reject = true;
								break;
							}
						}
					}
				}
			}

			if (!reject)
			{
				toExpandIterator.push_back(Ui);
			}

		}

		for (list <list<soopDataSet>::iterator>::iterator it = toExpandIterator.begin(); it!= toExpandIterator.end(); ++it)
		{
			toExpand.splice(toExpand.end(),data,(*it));
		}

		toExpandIterator.erase(toExpandIterator.begin(),toExpandIterator.end());



#ifdef DEBUG_TIME
		QueryPerformanceCounter((LARGE_INTEGER *)&endtBS);
		timeBoxSelect = timeBoxSelect + (endtBS - startBS) * 1000.0 / frequencyBS;
		//cour << "boxSelect=  " << timeBoxSelect << endl;
#endif
		/*==========================================================
		=====================Select dim of BOXES & EXP==============
		*///========================================================


		for (std::list<soopDataSet>::iterator it=toExpand.begin(); it != toExpand.end(); ++it)
		{
#ifdef DEBUG_TIME		
			__int64 frequencyDS, startDS, endtDS, totalDS;
			QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyDS);
			QueryPerformanceCounter ( (LARGE_INTEGER *)&startDS);  
#endif
			//dimension selection
			unsigned int pozMaxDimSc = pozMaxDimSc = dimentionSelectionOPC((*it).sequenceLenght, (*it).splits);


			if (pozMaxDimSc > maxLengthSequence )
			{
				newDimensionAdded = true;

			}
			//ofstream myfile;
			//myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
			//myfile << "while NR "<< counter <<" pozMAx " << pozMaxDimSc <<" size toEx " << toExpand.size() << endl;
			//myfile.close();
#ifdef DEBUG_TIME
			QueryPerformanceCounter((LARGE_INTEGER *)&endtDS);
			timeDimSelect = timeDimSelect + (endtDS - startDS) * 1000.0 / frequencyDS;
			//cour << "boxSelect=  " << timeBoxSelect << endl;
#endif
			/*==========================================================
			=====================SPLIT DIM==============================
			*///========================================================
#ifdef DEBUG_TIME		
			__int64 frequencyS, startS, endtS, totalS;
			QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyS);
			QueryPerformanceCounter ( (LARGE_INTEGER *)&startS);  
#endif

			soopDataSet leftSegment;// = new soopDataSet;
			soopDataSet rightSegment;// = new soopDataSet;
			fillSoopData(leftSegment, (*it).startPoint,(*it).splits,(*it).trajectory,(*it).rewardSum,(*it).sequenceLenght);
			fillSoopData(rightSegment,(*it).startPoint,(*it).splits,(*it).trajectory,(*it).rewardSum,(*it).sequenceLenght);

			(*it).splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc] + 1;
			leftSegment.splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc];
			rightSegment.splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc];
			// startPoint setup L/R/O
			leftSegment.startPoint[pozMaxDimSc] = (*it).startPoint[pozMaxDimSc];
			(*it).startPoint[pozMaxDimSc] = leftSegment.startPoint[pozMaxDimSc] + pow(1.0/3.0, (int)leftSegment.splits[pozMaxDimSc]);
			rightSegment.startPoint[pozMaxDimSc] = (*it).startPoint[pozMaxDimSc] + pow(1.0/3.0, (int)leftSegment.splits[pozMaxDimSc]);
			leftSegment.sequenceLenght = (*it).sequenceLenght;
			rightSegment.sequenceLenght = (*it).sequenceLenght;

			it->splitsSum += 1;
			rightSegment.splitsSum = it->splitsSum;
			leftSegment.splitsSum = it->splitsSum;

			if (pozMaxDimSc >= (*it).sequenceLenght -1 )//((*it).splits[pozMaxDimSc] == 1 || (*it).splits[pozMaxDimSc+1] == 0) //split last or new dimension
			{
				//setup L/R/O splits count

				if (pozMaxDimSc == (*it).sequenceLenght) // split new dim
				{   // model simulate LRO

					currentBudget = currentBudget + 3;

					double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
					stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
					stateE  newRstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;

					uMidle = middleOfBox((*it).startPoint[pozMaxDimSc], (*it).splits[pozMaxDimSc]);
					stateE  newCstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy((*it).trajectory[pozMaxDimSc].variables, newCstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					(*it).trajectory[pozMaxDimSc].reward =  newCstateE.reward;

					(*it).sequenceLenght++;
					leftSegment.sequenceLenght = (*it).sequenceLenght;
					rightSegment.sequenceLenght = (*it).sequenceLenght;


				}
				else // split last dim.
				{ // model simulate LR
					currentBudget = currentBudget + 2;

					if (pozMaxDimSc == 0 )
					{ // split first dim => simulate with s0 new state // setup trajectory  L/R, O is the same
						double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
						stateE newLstateE = nextstateE(s0, unnormU(uMidle));
						memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

						uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
						stateE  newRstateE =nextstateE(s0,unnormU(uMidle));
						memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;

					}
					else
					{ // split last dim,  it is not the first dim.
						double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
						stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
						memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

						uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
						stateE  newRstateE =nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
						memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
					}
				}
			}
			else // split dimension and recalculate trajectories
			{

				double uLmidle, uRmidle, uCmidle;
				uLmidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc],leftSegment.splits[pozMaxDimSc]);
				uCmidle = middleOfBox((*it).startPoint[pozMaxDimSc],(*it).splits[pozMaxDimSc]);
				uRmidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc],rightSegment.splits[pozMaxDimSc]);

				if (pozMaxDimSc == 0)
				{
					stateE newLstateE = nextstateE(s0, unnormU(uLmidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					stateE  newRstateE =nextstateE(s0,unnormU(uRmidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
				}
				else
				{
					stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uLmidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					stateE  newRstateE =nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uRmidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
				}
				currentBudget = currentBudget +2;
				unsigned int stepTmp=pozMaxDimSc + 1;

				while (stepTmp < (*it).sequenceLenght)//(*it).splits[stepTmp]!=0 || stepTmp>=initlen) //recalculate trajectory for left, right, center segments
				{
					//cout << "while2 at: " << stepTmp << " "<< (*it).sequenceLenght <<endl;
					stateE newLstateE;// = new stateE;
					stateE newRstateE;// = new stateE;
					uLmidle = middleOfBox((*it).startPoint[stepTmp],(*it).splits[stepTmp]);
					uRmidle = middleOfBox((*it).startPoint[stepTmp],(*it).splits[stepTmp ]);
					// stateE newCstateE;// = new stateE;

					//{
					newLstateE =  nextstateE(leftSegment.trajectory[stepTmp-1], unnormU(uLmidle));
					newRstateE =  nextstateE(rightSegment.trajectory[stepTmp-1], unnormU(uRmidle));
					//newCstateE =  nextstateE((*it).trajectory[stepTmp-1], unnormU(uCmidle));
					//}
					memcpy(leftSegment.trajectory[stepTmp].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[stepTmp].reward = newLstateE.reward;

					memcpy(rightSegment.trajectory[stepTmp].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[stepTmp].reward = newRstateE.reward;

					stepTmp++;
					currentBudget = currentBudget + 2;


				}
				//currentBudget=currentBudget+(stepTmp-pozMaxDimSc)*2;

			}
#ifdef DEBUG_TIME
			QueryPerformanceCounter((LARGE_INTEGER *)&endtS);
			timeSplit = timeSplit + (endtS - startS) * 1000.0 / frequencyS;
			//cour << "boxSelect=  " << timeBoxSelect << endl;
#endif
			(*it).rewardSum = recalculateRewardSum((*it).trajectory, (*it).sequenceLenght);
			leftSegment.rewardSum = recalculateRewardSum(leftSegment.trajectory, leftSegment.sequenceLenght);
			rightSegment.rewardSum = recalculateRewardSum(rightSegment.trajectory, rightSegment.sequenceLenght);

			data.push_back(leftSegment);
			data.push_back(rightSegment);


		}
		data.splice(data.end(),toExpand);
		toExpand.erase(toExpand.begin(), toExpand.end());
		if (currentBudget > budget) stopFlag = true;

#ifdef DEBUG_SPLIT
		//data.sort(compareRewardSum());
		//ofstream myfile;
		myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
		myfile << "list of SQ NOT EXP:" << data.size() << " loop "<< counter <<endl;
		myfile.close();
		for (std::list<soopDataSet>::iterator itTMP=data.begin(); itTMP != data.end(); ++itTMP)
		{
			myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
			(*itTMP).rewardSum = recalculateRewardSum((*itTMP).trajectory, (*itTMP).sequenceLenght);
			// cout << "RS " << (*itTMP).rewardSum << " ";
			myfile << "RS " << (*itTMP).rewardSum <<" sqLen " << (*itTMP).sequenceLenght;
			myfile.close();
			for (int itmp = 0; itmp<(*itTMP).sequenceLenght;itmp++ )
			{
				//if ((*itTMP).splits[itmp] != 0.0)
				{
					myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
					//cout  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp] <<" ";
					myfile  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp]  ;
					myfile.close();
					// printStateE((*itTMP).trajectory[itmp]);
					printStatusToFile((*itTMP).trajectory[itmp]);
				}
			}
			//cout<<endl;
		}
#endif

		if (newDimensionAdded) maxLengthSequence++;
	}       //END while loop

#ifdef DEBUG_TIME
	cout << "box select= " << timeBoxSelect/nrTimeSamples << " ,dimSelect= " << timeDimSelect/nrTimeSamples << " ,split= " << timeSplit/nrTimeSamples << " ,n= " << nrTimeSamples;

	__int64 frequencyS, startS, endtS, totalS;
	QueryPerformanceFrequency( (LARGE_INTEGER *)&frequencyS);
	QueryPerformanceCounter ( (LARGE_INTEGER *)&startS);  
#endif
	//update max length of sequences



	double maxRewardSum = 0.0, commandSignalToSend = 0.0;
	for (std::list<soopDataSet>::iterator itTMP=data.begin(); itTMP != data.end(); ++itTMP)
	{
		if ((*itTMP).rewardSum > maxRewardSum)
		{
			commandSignalToSend = unnormU(middleOfBox((*itTMP).startPoint[0], (*itTMP).splits[0]));
			maxRewardSum = (*itTMP).rewardSum;
		}
	}
	//cout << "myserach RS max " << maxRewardSum << " U " << commandSignalToSend << endl;


	data.sort(compareRewardSum());
	list<soopDataSet>::iterator itmax = data.begin();

	double * commandSequence = (double*)malloc(sizeof(double)*initlen);
	//commandSequence[0] = (*itmax).sequenceLenght;

	for (int i=0;i<initlen;i++)
	{
		commandSequence[i] = unnormU(middleOfBox((*itmax).startPoint[i], (*itmax).splits[i]));
	}

	//printf("%d, ", (*itmax).sequenceLenght);
#ifdef DEBUG_COMMAND
	cout<< "RS " <<(*itmax).rewardSum<< " start= "<< (*itmax).startPoint[0] <<" split " << (*itmax).splits[0]<< " seqLen " << (*itmax).sequenceLenght  <<" command= " << middleOfBox((*itmax).startPoint[0], (*itmax).splits[0]) << " U= " << commandSequence[0]<< endl;
	//cout << "MidBox " << (middleOfBox((*itmax).startPoint[0], (*itmax).splits[0])) << " for SP " << (*itmax).startPoint[0] << " spilts " << (*itmax).splits[0] <<endl;
	cout << "SOOP main return done max length seq: "<< maxLengthSequence<< endl;
#endif

	freeSoopDataList();
#ifdef DEBUG_TIME
	QueryPerformanceCounter((LARGE_INTEGER *)&endtS);
	double timeEnd =  (endtS - startS) * 1000.0 / frequencyS;
	cout << " ,end=" << timeEnd << endl;
#endif

	return commandSequence;
}

double * OPC_lp(stateE& s0)
{ // SOOP algorithm, first element of data list has the highest rewardSum


	bool stopFlag = false;
	unsigned int counter = 0;
	unsigned int currentBudget = 0;
	unsigned int listLenght = 0;
	unsigned int maxLengthSequence = 0;
	double * commandSequence = (double*)malloc(sizeof(double)*initlen);

	/*==========================================================
	=====================INIT BOXES=============================
	*///========================================================
	//cout<<s0.variables[0]<<endl;
	for (int i=0;i<3;i++)
	{
		double sPtmp[initlen] = {0};
		unsigned int stmp[initlen] = {0};
		stateE newstateE[initlen];
		sPtmp[0] = ((double)i)/3.0;
		stmp[0] = 1;
		newstateE[0] = nextstateE(s0, unnormU(middleOfBox(sPtmp[0], stmp[0])) );
		soopDataSet newData;
		fillSoopData(newData,sPtmp, stmp, newstateE,newstateE[0].reward, 1);
		newData.sequenceLenght = 1;
		data.push_front(newData);
	}



	bool newDimensionAdded = false;

	while (! stopFlag)         // main loop
	{

		ofstream myfile;
		counter++;
		/*==========================================================
		=====================Select BOXES to EXP====================
		*///========================================================

		listLenght = data.size();
		list<soopDataSet> toExpand;

		list <list<soopDataSet>::iterator> toExpandIterator;
		//bool reject;
		double B_max = 0.0, B_current = 0.0;
		for (std::list<soopDataSet>::iterator Ui = data.begin(); Ui != data.end(); ++Ui)
		{
			B_current = calcualteB((*Ui));
			if ( B_max < B_current )
			{
				toExpandIterator.clear();
				toExpandIterator.push_back(Ui);
				B_max = B_current;
			}
		}

		for (list <list<soopDataSet>::iterator>::iterator it = toExpandIterator.begin(); it!= toExpandIterator.end(); ++it)
		{
			toExpand.splice(toExpand.end(),data,(*it));
		}

		toExpandIterator.erase(toExpandIterator.begin(),toExpandIterator.end());


#ifdef DEBUG_SPLIT
		//data.sort(compareRewardSum());
		//ofstream myfile;
		myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
		myfile << "list of SQ NOT EXP:" << data.size() <<endl;
		myfile.close();
		for (std::list<soopDataSet>::iterator itTMP=data.begin(); itTMP != data.end(); ++itTMP)
		{
			myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
			(*itTMP).rewardSum = recalculateRewardSum((*itTMP).trajectory, (*itTMP).sequenceLenght);
			// cout << "RS " << (*itTMP).rewardSum << " ";
			myfile << "RS " << (*itTMP).rewardSum << " ";
			myfile.close();
			for (int itmp = 0; itmp<(*itTMP).sequenceLenght;itmp++ )
			{
				//if ((*itTMP).splits[itmp] != 0.0)
				{
					myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
					//cout  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp] <<" ";
					myfile  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp] <<" ";
					myfile.close();
					// printStateE((*itTMP).trajectory[itmp]);
					printStatusToFile((*itTMP).trajectory[itmp]);
				}
			}
			//cout<<endl;
		}
#endif
#ifdef DEBUG_SPLIT
		//data.sort(compareRewardSum());
		//ofstream myfile;
		myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
		myfile << "list of SQ to EXP:" << toExpand.size() <<endl;
		myfile.close();
		for (std::list<soopDataSet>::iterator itTMP=toExpand.begin(); itTMP != toExpand.end(); ++itTMP)
		{
			myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
			(*itTMP).rewardSum = recalculateRewardSum((*itTMP).trajectory, (*itTMP).sequenceLenght);
			// cout << "RS " << (*itTMP).rewardSum << " ";
			myfile << "RS " << (*itTMP).rewardSum << " ";
			myfile.close();
			for (int itmp = 0; itmp<(*itTMP).sequenceLenght;itmp++ )
			{
				if ((*itTMP).splits[itmp] != 0.0)
				{
					myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
					//cout  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp] <<" ";
					myfile  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp] <<" ";
					myfile.close();
					// printStateE((*itTMP).trajectory[itmp]);
					printStatusToFile((*itTMP).trajectory[itmp]);
				}
			}
			//cout<<endl;
		}
#endif


		/*==========================================================
		=====================Select dim of BOXES & EXP==============
		*///========================================================


		for (std::list<soopDataSet>::iterator it=toExpand.begin(); it != toExpand.end(); ++it)
		{

			//dimension selection
			unsigned int pozMaxDimSc = dimentionSelectionOPC_lp((*it).sequenceLenght, (*it).splits);

			if (pozMaxDimSc > maxLengthSequence )
			{
				newDimensionAdded = true;
			}
			//ofstream myfile;
			//myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
			//myfile << "while NR "<< counter <<" pozMAx " << pozMaxDimSc <<" size toEx " << toExpand.size() << endl;
			//myfile.close();
			/*==========================================================
			=====================SPLIT DIM==============================
			*///========================================================

			soopDataSet leftSegment;// = new soopDataSet;
			soopDataSet rightSegment;// = new soopDataSet;
			fillSoopData(leftSegment, (*it).startPoint,(*it).splits,(*it).trajectory,(*it).rewardSum,(*it).sequenceLenght);
			fillSoopData(rightSegment,(*it).startPoint,(*it).splits,(*it).trajectory,(*it).rewardSum,(*it).sequenceLenght);

			(*it).splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc] + 1;
			leftSegment.splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc];
			rightSegment.splits[pozMaxDimSc] = (*it).splits[pozMaxDimSc];
			// startPoint setup L/R/O
			leftSegment.startPoint[pozMaxDimSc] = (*it).startPoint[pozMaxDimSc];
			(*it).startPoint[pozMaxDimSc] = leftSegment.startPoint[pozMaxDimSc] + pow(1.0/3.0, (int)leftSegment.splits[pozMaxDimSc]);
			rightSegment.startPoint[pozMaxDimSc] = (*it).startPoint[pozMaxDimSc] + pow(1.0/3.0, (int)leftSegment.splits[pozMaxDimSc]);
			leftSegment.sequenceLenght = (*it).sequenceLenght;
			rightSegment.sequenceLenght = (*it).sequenceLenght;

			if (pozMaxDimSc >= (*it).sequenceLenght -1 )//((*it).splits[pozMaxDimSc] == 1 || (*it).splits[pozMaxDimSc+1] == 0) //split last or new dimension
			{
				//setup L/R/O splits count

				if (pozMaxDimSc == (*it).sequenceLenght) // split new dim
				{   // model simulate LRO

					currentBudget = currentBudget + 3;

					double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
					stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
					stateE  newRstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;

					uMidle = middleOfBox((*it).startPoint[pozMaxDimSc], (*it).splits[pozMaxDimSc]);
					stateE  newCstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy((*it).trajectory[pozMaxDimSc].variables, newCstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					(*it).trajectory[pozMaxDimSc].reward =  newCstateE.reward;

					(*it).sequenceLenght++;
					leftSegment.sequenceLenght = (*it).sequenceLenght;
					rightSegment.sequenceLenght = (*it).sequenceLenght;


				}
				else // split last dim.
				{ // model simulate LR
					currentBudget = currentBudget + 2;

					if (pozMaxDimSc == 0 )
					{ // split first dim => simulate with s0 new state // setup trajectory  L/R, O is the same
						double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
						stateE newLstateE = nextstateE(s0, unnormU(uMidle));
						memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

						uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
						stateE  newRstateE =nextstateE(s0,unnormU(uMidle));
						memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;

					}
					else
					{ // split last dim,  it is not the first dim.
						double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
						stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
						memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

						uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
						stateE  newRstateE =nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uMidle));
						memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
					}
				}
			}
			else // split dimension and recalculate trajectories
			{

				double uLmidle, uRmidle, uCmidle;
				uLmidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc],leftSegment.splits[pozMaxDimSc]);
				uCmidle = middleOfBox((*it).startPoint[pozMaxDimSc],(*it).splits[pozMaxDimSc]);
				uRmidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc],rightSegment.splits[pozMaxDimSc]);

				if (pozMaxDimSc == 0)
				{
					stateE newLstateE = nextstateE(s0, unnormU(uLmidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					stateE  newRstateE =nextstateE(s0,unnormU(uRmidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
				}
				else
				{
					stateE newLstateE = nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uLmidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					stateE  newRstateE =nextstateE((*it).trajectory[pozMaxDimSc-1], unnormU(uRmidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
				}
				currentBudget = currentBudget +2;
				unsigned int stepTmp=pozMaxDimSc + 1;

				while (stepTmp < (*it).sequenceLenght)//(*it).splits[stepTmp]!=0 || stepTmp>=initlen) //recalculate trajectory for left, right, center segments
				{
					//cout << "while2 at: " << stepTmp << " "<< (*it).sequenceLenght <<endl;
					stateE newLstateE;// = new stateE;
					stateE newRstateE;// = new stateE;
					uLmidle = middleOfBox((*it).startPoint[stepTmp],(*it).splits[stepTmp]);
					uRmidle = middleOfBox((*it).startPoint[stepTmp],(*it).splits[stepTmp ]);
					// stateE newCstateE;// = new stateE;

					//{
					newLstateE =  nextstateE(leftSegment.trajectory[stepTmp-1], unnormU(uLmidle));
					newRstateE =  nextstateE(rightSegment.trajectory[stepTmp-1], unnormU(uRmidle));
					//newCstateE =  nextstateE((*it).trajectory[stepTmp-1], unnormU(uCmidle));
					//}
					memcpy(leftSegment.trajectory[stepTmp].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[stepTmp].reward = newLstateE.reward;

					memcpy(rightSegment.trajectory[stepTmp].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[stepTmp].reward = newRstateE.reward;

					stepTmp++;
					currentBudget = currentBudget + 2;


				}
				//currentBudget=currentBudget+(stepTmp-pozMaxDimSc)*2;

			}
			leftSegment.rewardSum = recalculateRewardSum(leftSegment.trajectory, leftSegment.sequenceLenght);
			rightSegment.rewardSum = recalculateRewardSum(rightSegment.trajectory, rightSegment.sequenceLenght);

			data.push_back(leftSegment);
			data.push_back(rightSegment);


		}
		data.splice(data.end(),toExpand);
		toExpand.erase(toExpand.begin(), toExpand.end());
		if (currentBudget > budget) stopFlag = true;

#ifdef DEBUG_SPLIT
		//data.sort(compareRewardSum());
		//ofstream myfile;
		myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
		myfile << "list of SQ NOT EXP:" << data.size() << " loop "<< counter <<endl;
		myfile.close();
		for (std::list<soopDataSet>::iterator itTMP=data.begin(); itTMP != data.end(); ++itTMP)
		{
			myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
			(*itTMP).rewardSum = recalculateRewardSum((*itTMP).trajectory, (*itTMP).sequenceLenght);
			// cout << "RS " << (*itTMP).rewardSum << " ";
			myfile << "RS " << (*itTMP).rewardSum <<" sqLen " << (*itTMP).sequenceLenght;
			myfile.close();
			for (int itmp = 0; itmp<(*itTMP).sequenceLenght;itmp++ )
			{
				//if ((*itTMP).splits[itmp] != 0.0)
				{
					myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
					//cout  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp] <<" ";
					myfile  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp]  ;
					myfile.close();
					// printStateE((*itTMP).trajectory[itmp]);
					printStatusToFile((*itTMP).trajectory[itmp]);
				}
			}
			//cout<<endl;
		}
#endif
		if (newDimensionAdded) maxLengthSequence++;
	}       //END while loop

	//update max length of sequences

	data.sort(compareRewardSum());
	list<soopDataSet>::iterator itmax = data.begin();


	//double  commandSequence[initlen];
	//commandSequence[0] = (*itmax).sequenceLenght;

	for (int i=0;i<initlen;i++)
	{
		commandSequence[i] = unnormU(middleOfBox((*itmax).startPoint[i], (*itmax).splits[i]));
	}

	//printf("%d, ", (*itmax).sequenceLenght);
#ifdef DEBUG_COMMAND
	cout<< "RS " <<(*itmax).rewardSum<< " start= "<< (*itmax).startPoint[0] <<" split " << (*itmax).splits[0]<< " seqLen " << (*itmax).sequenceLenght  <<" command= " << middleOfBox((*itmax).startPoint[0], (*itmax).splits[0]) << " U= " << commandSequence[0]<< endl;
	//cout << "MidBox " << (middleOfBox((*itmax).startPoint[0], (*itmax).splits[0])) << " for SP " << (*itmax).startPoint[0] << " spilts " << (*itmax).splits[0] <<endl;
	cout << "SOOP main return done max length seq: "<< maxLengthSequence<< endl;
#endif

	freeSoopDataList();
	return commandSequence;
}

void printSoopData()
{

	ofstream myfile;
	myfile.open ("soopAllSimulationC.txt", std::ofstream::out | std::ofstream::app);
	myfile  << "------------" << endl;
	myfile.precision(4);
	myfile.setf( std::ios::fixed, std:: ios::floatfield );

	for (std::list<soopDataSet>::iterator Ui = data.begin(); Ui != data.end(); ++Ui)
	{
		unsigned int T = (*Ui).sequenceLenght;
		// splits
		for (unsigned int i=0 ; i < T; i++)
		{
			myfile << (*Ui).splits[i] << " ";
		}
		myfile << endl;
		// centerpoint
		for (unsigned int i=0 ; i < T; i++)
		{
			myfile <<  (*Ui).startPoint[i] << " ";
		}
		myfile << endl;

		// reward
		myfile.precision(8);
		for (unsigned int i=0 ; i < T; i++)
		{
			myfile << (*Ui).trajectory[i].reward << " ";
		}
		myfile << endl;
		myfile.precision(4);
		
		//commnad
		/*
		for (int i=0 ; i < T; i++)
		{
		myfile <<unnormU(middleOfBox((*Ui).startPoint[i], (*Ui).splits[i])) << " ";
		}
		myfile << endl;
		*/


		for (unsigned int i=0 ; i < T; i++)
		{
			for (unsigned int j=0 ; j < 4; j++)
			{
				myfile << (*Ui).trajectory[i].variables[j] << " ";
			}
			myfile << endl;
		}
		myfile  << endl;
	}
	myfile  << endl;		
	myfile.close();
}

double * OPC(stateE& s0)
{ // SOOP algorithm, first element of data list has the highest rewardSum


	bool stopFlag = false;
	unsigned int counter = 0;
	unsigned int currentBudget = 0;
	unsigned int listLenght = 0;
	unsigned int maxLengthSequence = 0;
	bool newDimensionAdded = false;
	//	bool reject;
	double B_max = 0.0, B_current = 0.0, uMidle = 0.0;
	double uLmidle, uRmidle, uCmidle;
	unsigned int stepTmp, pozMaxDimSc = 0;;
	double * commandSequence = (double*)malloc(sizeof(double)*initlen);
	stateE newLstateE, newRstateE, newCstateE;

	list <list<soopDataSet>::iterator> toExpand;
	//list<soopDataSet> toExpand;
	//list <list<soopDataSet>::iterator> toExpandIterator;

	/*==========================================================
	=====================INIT BOXES=============================
	*///========================================================

	for (int i=0;i<3;i++)
	{
		double sPtmp[initlen] = {0};
		unsigned int stmp[initlen] = {0};
		stateE newstateE[initlen];
		sPtmp[0] = ((double)i)/3.0;
		stmp[0] = 1;
		copyStateE(newstateE[0], nextstateE(s0, unnormU(middleOfBox(sPtmp[0], stmp[0])) ));
		soopDataSet newData;
		fillSoopData(newData,sPtmp, stmp, newstateE,newstateE[0].reward, 1);
		newData.sequenceLenght = 1;
		data.push_back(newData);

		//cout << endl << i << ": " << s0.variables[0] << " " << s0.variables[1] << " " << s0.variables[2] << " " << s0.variables[3] << endl;
		//cout << sPtmp[0] << " " << stmp[0] << " " <<unnormU(middleOfBox(sPtmp[0], stmp[0]))  << endl;
		//cout << newstateE[0].variables[0] << " " << newstateE[0].variables[1] << " " <<  newstateE[0].variables[2] << " " << newstateE[0].variables[3] << endl<< endl;

		//printSoopData();
		//delete[] newstateE;
	}	

	while (! stopFlag)         // main loop
	{

#ifdef DEBUG_ALGORITHM
		printSoopData();
#endif // DEBUG_ALGORITHM		

		ofstream myfile;
		counter++;
		/*==========================================================
		=====================Select BOXES to EXP====================
		*///========================================================
		listLenght = data.size();

		std::list<soopDataSet>::iterator toExpand;
		B_max = 0.0, 
			B_current = 0.0;
		for (std::list<soopDataSet>::iterator Ui = data.begin(); Ui != data.end(); ++Ui)
		{
			B_current = calcualteB_OPC((*Ui));
			if ( B_max < B_current )
			{
				toExpand = Ui;
				B_max = B_current;				
			}
		}

		/*==========================================================
		=====================Select dim of BOXES & EXP==============
		*///========================================================		
				
		pozMaxDimSc = dimentionSelectionOPC((*toExpand).sequenceLenght, (*toExpand).splits);
		if (pozMaxDimSc > maxLengthSequence )
		{
			newDimensionAdded = true;
		}

		/*==========================================================
		=====================SPLIT DIM==============================
		*///========================================================

		soopDataSet leftSegment;// = new soopDataSet;
		soopDataSet rightSegment;// = new soopDataSet;
		fillSoopData(leftSegment, (*toExpand).startPoint,(*toExpand).splits,(*toExpand).trajectory,(*toExpand).rewardSum,(*toExpand).sequenceLenght);
		fillSoopData(rightSegment,(*toExpand).startPoint,(*toExpand).splits,(*toExpand).trajectory,(*toExpand).rewardSum,(*toExpand).sequenceLenght);

		(*toExpand).splits[pozMaxDimSc] = (*toExpand).splits[pozMaxDimSc] + 1;
		leftSegment.splits[pozMaxDimSc] = (*toExpand).splits[pozMaxDimSc];
		rightSegment.splits[pozMaxDimSc] = (*toExpand).splits[pozMaxDimSc];
		// startPoint setup L/R/O
		leftSegment.startPoint[pozMaxDimSc] = (*toExpand).startPoint[pozMaxDimSc];
		(*toExpand).startPoint[pozMaxDimSc] = leftSegment.startPoint[pozMaxDimSc] + pow(1.0/3.0, (int)leftSegment.splits[pozMaxDimSc]);
		rightSegment.startPoint[pozMaxDimSc] = (*toExpand).startPoint[pozMaxDimSc] + pow(1.0/3.0, (int)leftSegment.splits[pozMaxDimSc]);
		leftSegment.sequenceLenght = (*toExpand).sequenceLenght;
		rightSegment.sequenceLenght = (*toExpand).sequenceLenght;

		if (pozMaxDimSc >= (*toExpand).sequenceLenght -1 )//((*it).splits[pozMaxDimSc] == 1 || (*it).splits[pozMaxDimSc+1] == 0) //split last or new dimension
		{
			//setup L/R/O splits count

			if (pozMaxDimSc == (*toExpand).sequenceLenght) // split new dim
			{   // model simulate LRO

				currentBudget = currentBudget + 3;

				//double 
				uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
				//stateE 
				newLstateE = nextstateE((*toExpand).trajectory[pozMaxDimSc-1], unnormU(uMidle));
				memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

				uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
				//stateE  
				newRstateE = nextstateE((*toExpand).trajectory[pozMaxDimSc-1], unnormU(uMidle));
				memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;

				uMidle = middleOfBox((*toExpand).startPoint[pozMaxDimSc], (*toExpand).splits[pozMaxDimSc]);
				//stateE  
				newCstateE = nextstateE((*toExpand).trajectory[pozMaxDimSc-1], unnormU(uMidle));
				memcpy((*toExpand).trajectory[pozMaxDimSc].variables, newCstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				(*toExpand).trajectory[pozMaxDimSc].reward =  newCstateE.reward;

				(*toExpand).sequenceLenght++;
				leftSegment.sequenceLenght = (*toExpand).sequenceLenght;
				rightSegment.sequenceLenght = (*toExpand).sequenceLenght;


			}
			else // split last dim.
			{ // model simulate LR
				currentBudget = currentBudget + 2;

				if (pozMaxDimSc == 0 )
				{ // split first dim => simulate with s0 new state // setup trajectory  L/R, O is the same
					//double 
					uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
					//stateE 
					newLstateE = nextstateE(s0, unnormU(uMidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
					//stateE  
					newRstateE =nextstateE(s0,unnormU(uMidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;

				}
				else
				{ // split last dim,  it is not the first dim.
					//double 
					uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
					//stateE 
					newLstateE = nextstateE((*toExpand).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

					uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
					//stateE  
					newRstateE =nextstateE((*toExpand).trajectory[pozMaxDimSc-1], unnormU(uMidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
				}
			}
		}
		else // split dimension and recalculate trajectories
		{

			//double uLmidle, uRmidle, uCmidle;
			uLmidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc],leftSegment.splits[pozMaxDimSc]);
			uCmidle = middleOfBox((*toExpand).startPoint[pozMaxDimSc],(*toExpand).splits[pozMaxDimSc]);
			uRmidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc],rightSegment.splits[pozMaxDimSc]);

			if (pozMaxDimSc == 0)
			{
				//stateE 
				newLstateE = nextstateE(s0, unnormU(uLmidle));
				memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

				//stateE  
				newRstateE =nextstateE(s0,unnormU(uRmidle));
				memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
			}
			else
			{
				//stateE 
				newLstateE = nextstateE((*toExpand).trajectory[pozMaxDimSc-1], unnormU(uLmidle));
				memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				leftSegment.trajectory[pozMaxDimSc].reward =  newLstateE.reward;

				//stateE  
				newRstateE =nextstateE((*toExpand).trajectory[pozMaxDimSc-1], unnormU(uRmidle));
				memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				rightSegment.trajectory[pozMaxDimSc].reward =  newRstateE.reward;
			}
			currentBudget = currentBudget +2;
			//int
			stepTmp=pozMaxDimSc + 1;

			while (stepTmp < (*toExpand).sequenceLenght)//(*it).splits[stepTmp]!=0 || stepTmp>=initlen) //recalculate trajectory for left, right, center segments
			{
				//cout << "while2 at: " << stepTmp << " "<< (*it).sequenceLenght <<endl;
				stateE newLstateE;// = new stateE;
				stateE newRstateE;// = new stateE;
				uLmidle = middleOfBox((*toExpand).startPoint[stepTmp],(*toExpand).splits[stepTmp]);
				uRmidle = middleOfBox((*toExpand).startPoint[stepTmp],(*toExpand).splits[stepTmp ]);
				// stateE newCstateE;// = new stateE;

				newLstateE =  nextstateE(leftSegment.trajectory[stepTmp-1], unnormU(uLmidle));
				newRstateE =  nextstateE(rightSegment.trajectory[stepTmp-1], unnormU(uRmidle));
				//newCstateE =  nextstateE((*it).trajectory[stepTmp-1], unnormU(uCmidle));

				memcpy(leftSegment.trajectory[stepTmp].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				leftSegment.trajectory[stepTmp].reward = newLstateE.reward;

				memcpy(rightSegment.trajectory[stepTmp].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
				rightSegment.trajectory[stepTmp].reward = newRstateE.reward;

				stepTmp++;
				currentBudget = currentBudget + 2;


			}
			//currentBudget=currentBudget+(stepTmp-pozMaxDimSc)*2;

		}
		leftSegment.rewardSum = recalculateRewardSum(leftSegment.trajectory, leftSegment.sequenceLenght);
		rightSegment.rewardSum = recalculateRewardSum(rightSegment.trajectory, rightSegment.sequenceLenght);

		data.push_back(leftSegment);
		data.push_back(rightSegment);




		if (currentBudget > budget) stopFlag = true;

#ifdef DEBUG_SPLIT
		//data.sort(compareRewardSum());
		//ofstream myfile;
		myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
		myfile << "list of SQ NOT EXP:" << data.size() << " loop "<< counter <<endl;
		myfile.close();
		for (std::list<soopDataSet>::iterator itTMP=data.begin(); itTMP != data.end(); ++itTMP)
		{
			myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
			(*itTMP).rewardSum = recalculateRewardSum((*itTMP).trajectory, (*itTMP).sequenceLenght);
			// cout << "RS " << (*itTMP).rewardSum << " ";
			myfile << "RS " << (*itTMP).rewardSum <<" sqLen " << (*itTMP).sequenceLenght;
			myfile.close();
			for (int itmp = 0; itmp<(*itTMP).sequenceLenght;itmp++ )
			{
				//if ((*itTMP).splits[itmp] != 0.0)
				{
					myfile.open ("soopTest.txt", std::ofstream::out | std::ofstream::app);
					//cout  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp] <<" ";
					myfile  << "  " <<itmp <<" SP " << (*itTMP).startPoint[itmp] << " S " << (*itTMP).splits[itmp]  ;
					myfile.close();
					// printStateE((*itTMP).trajectory[itmp]);
					printStatusToFile((*itTMP).trajectory[itmp]);
				}
			}
			//cout<<endl;
		}
#endif
		if (newDimensionAdded) maxLengthSequence++;
	}       //END while loop

	//update max length of sequences


	data.sort(compareRewardSum());
	list<soopDataSet>::iterator itmax = data.begin();


	//double  commandSequence[initlen];
	//commandSequence[0] = (*itmax).sequenceLenght;

	for (int i=0;i<initlen;i++)
	{
		commandSequence[i] = unnormU(middleOfBox((*itmax).startPoint[i], (*itmax).splits[i]));
	}

	//printf("%d, ", (*itmax).sequenceLenght);
#ifdef DEBUG_COMMAND
	cout<< "RS " <<(*itmax).rewardSum<< " start= "<< (*itmax).startPoint[0] <<" split " << (*itmax).splits[0]<< " seqLen " << (*itmax).sequenceLenght  <<" command= " << middleOfBox((*itmax).startPoint[0], (*itmax).splits[0]) << " U= " << commandSequence[0]<< endl;
	//cout << "MidBox " << (middleOfBox((*itmax).startPoint[0], (*itmax).splits[0])) << " for SP " << (*itmax).startPoint[0] << " spilts " << (*itmax).splits[0] <<endl;
	cout << "SOOP main return done max length seq: "<< maxLengthSequence<< endl;
#endif

	freeSoopDataList();
	return commandSequence;
}

// SOPC variables --- need to bemoved to the top!

list<soopDataSet>::iterator depthLinks[1000];  // the vector elements point to the first element at depth = array index; note the list is order based on the depths; depth  =  sum of all splits 

// init to the end of data 
void init_depthLinks_array()
{
	for (int i = 0; i < 1000; i++)
	{
		depthLinks[i] = data.end();
	}
}

// = H_max(ns) = ns^0.45
int H_max(int ns)
{		
	return (int)pow((double)ns, 0.45);
}

double * sopc(stateE& s0)
{ 
	//TODO:: check if true: SOPC algorithm, first element of data list has the highest rewardSum ???

	// maybe not nececarry, check before REAL experiments	
	unsigned int H_0 = 2;	// general starting depth for searching node to expand

	bool stopFlag = false;
	unsigned int counter = 0;
	unsigned int currentBudget = 3;
	unsigned int currentMaxDepth = 1;
	unsigned int listLenght = 0;
	unsigned int maxLengthSequence = 0;

	/*==========================================================
	=====================INIT BOXES=============================
	*///========================================================
	for (int i = 0; i<3; i++)
	{
		double sPtmp[initlen] = { 0 };
		unsigned int stmp[initlen] = { 0 };
		stateE newstateE[initlen];
		sPtmp[0] = ((double)i) / 3.0;
		stmp[0] = 1;
		newstateE[0] = nextstateE(s0, unnormU(middleOfBox(sPtmp[0], stmp[0])));
		soopDataSet newData;
		fillSoopData(newData, sPtmp, stmp, newstateE, newstateE[0].reward, 1);
		newData.sequenceLenght = 1;
		data.push_front(newData);
	}
	bool newDimensionAdded = false;
	init_depthLinks_array();
	depthLinks[1] = data.begin();
	

#ifdef DEBUG_TIME
	double timeBoxSelect = 0.0, timeDimSelect = 0.0, timeSplit = 0.0;
	int nrTimeSamples = 0;
#endif

	while (!stopFlag)         // main loop
	{
		ofstream myfile;
		counter++;

		if (currentMaxDepth == 1)
		{
			H_0 = 1;				
		}
		else
			H_0 = 2;
		
		list<soopDataSet>::iterator currentDepth_;
		// for each depth select a box to expand 
		for (int depthIter = H_0; depthIter <= H_max(currentBudget); depthIter++)
		{
			if (depthLinks[depthIter] == data.end())
			{
				//cerr << "Non exisiting dept link accessed at " << depthIter << endl;
				continue;
				//exit(-1);
			}
	
			currentDepth_ = depthLinks[depthIter];
			soopDataSet toExpand;
			list<soopDataSet>::iterator maxIter_ = currentDepth_;
			double maxR_ = currentDepth_->rewardSum;
			++currentDepth_;
			//=====================Select 1 BOXES at a DEPTH to EXP====================
			
			for (list<soopDataSet>::iterator it = currentDepth_; it != data.end(); ++it)
			{
				if (it->splitsSum > depthIter)
					break;
				if (maxR_ < it->rewardSum)
				{
					maxIter_ = it;
					maxR_ = it->rewardSum;
				}
			}

			toExpand = (*maxIter_);

			if (maxIter_ == depthLinks[depthIter])
			{
				depthLinks[depthIter] = ++maxIter_;

				if (maxIter_->splitsSum > depthIter)
					depthLinks[depthIter] = data.end();

				data.erase(--maxIter_);
			}
			else
				data.erase(maxIter_);

			//=====================SELECT DIMENTION to exp====================
			unsigned int pozMaxDimSc = dimentionSelectionSOPC(toExpand.sequenceLenght, toExpand.splits);			
			if (pozMaxDimSc > maxLengthSequence)
				newDimensionAdded = true;
			
			//=====================EXPAND a box====================
			soopDataSet leftSegment;// = new soopDataSet;
			soopDataSet rightSegment;// = new soopDataSet;
			fillSoopData(leftSegment, toExpand.startPoint, toExpand.splits, toExpand.trajectory, toExpand.rewardSum, toExpand.sequenceLenght);
			fillSoopData(rightSegment, toExpand.startPoint, toExpand.splits, toExpand.trajectory, toExpand.rewardSum, toExpand.sequenceLenght);

			toExpand.splits[pozMaxDimSc] = toExpand.splits[pozMaxDimSc] + 1;
			leftSegment.splits[pozMaxDimSc] = toExpand.splits[pozMaxDimSc];
			rightSegment.splits[pozMaxDimSc] = toExpand.splits[pozMaxDimSc];
			// startPoint setup L/R/O
			leftSegment.startPoint[pozMaxDimSc] = toExpand.startPoint[pozMaxDimSc];
			toExpand.startPoint[pozMaxDimSc] = leftSegment.startPoint[pozMaxDimSc] + pow(1.0 / 3.0, (int)leftSegment.splits[pozMaxDimSc]);
			rightSegment.startPoint[pozMaxDimSc] = toExpand.startPoint[pozMaxDimSc] + pow(1.0 / 3.0, (int)leftSegment.splits[pozMaxDimSc]);
			leftSegment.sequenceLenght = toExpand.sequenceLenght;
			rightSegment.sequenceLenght = toExpand.sequenceLenght;

			if (pozMaxDimSc >= toExpand.sequenceLenght - 1)//(toExpand.splits[pozMaxDimSc] == 1 || toExpand.splits[pozMaxDimSc+1] == 0) //split last or new dimension
			{
				//setup L/R/O splits count

				if (pozMaxDimSc == toExpand.sequenceLenght) // split new dim
				{   // model simulate LRO

					currentBudget = currentBudget + 3;

					double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
					stateE newLstateE = nextstateE(toExpand.trajectory[pozMaxDimSc - 1], unnormU(uMidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward = newLstateE.reward;

					uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
					stateE  newRstateE = nextstateE(toExpand.trajectory[pozMaxDimSc - 1], unnormU(uMidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward = newRstateE.reward;

					uMidle = middleOfBox(toExpand.startPoint[pozMaxDimSc], toExpand.splits[pozMaxDimSc]);
					stateE  newCstateE = nextstateE(toExpand.trajectory[pozMaxDimSc - 1], unnormU(uMidle));
					memcpy(toExpand.trajectory[pozMaxDimSc].variables, newCstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					toExpand.trajectory[pozMaxDimSc].reward = newCstateE.reward;

					toExpand.sequenceLenght++;
					leftSegment.sequenceLenght = toExpand.sequenceLenght;
					rightSegment.sequenceLenght = toExpand.sequenceLenght;


				}
				else // split last dim.
				{ // model simulate LR
					currentBudget = currentBudget + 2;

					if (pozMaxDimSc == 0)
					{ // split first dim => simulate with s0 new state // setup trajectory  L/R, O is the same
						double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
						stateE newLstateE = nextstateE(s0, unnormU(uMidle));
						memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						leftSegment.trajectory[pozMaxDimSc].reward = newLstateE.reward;

						uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
						stateE  newRstateE = nextstateE(s0, unnormU(uMidle));
						memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						rightSegment.trajectory[pozMaxDimSc].reward = newRstateE.reward;

					}
					else
					{ // split last dim,  it is not the first dim.
						double uMidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
						stateE newLstateE = nextstateE(toExpand.trajectory[pozMaxDimSc - 1], unnormU(uMidle));
						memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						leftSegment.trajectory[pozMaxDimSc].reward = newLstateE.reward;

						uMidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);
						stateE  newRstateE = nextstateE(toExpand.trajectory[pozMaxDimSc - 1], unnormU(uMidle));
						memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
						rightSegment.trajectory[pozMaxDimSc].reward = newRstateE.reward;
					}
				}
			}
			else // split dimension and recalculate trajectories
			{

				double uLmidle, uRmidle, uCmidle;
				uLmidle = middleOfBox(leftSegment.startPoint[pozMaxDimSc], leftSegment.splits[pozMaxDimSc]);
				uCmidle = middleOfBox(toExpand.startPoint[pozMaxDimSc], toExpand.splits[pozMaxDimSc]);
				uRmidle = middleOfBox(rightSegment.startPoint[pozMaxDimSc], rightSegment.splits[pozMaxDimSc]);

				if (pozMaxDimSc == 0)
				{
					stateE newLstateE = nextstateE(s0, unnormU(uLmidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward = newLstateE.reward;

					stateE  newRstateE = nextstateE(s0, unnormU(uRmidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward = newRstateE.reward;
				}
				else
				{
					stateE newLstateE = nextstateE(toExpand.trajectory[pozMaxDimSc - 1], unnormU(uLmidle));
					memcpy(leftSegment.trajectory[pozMaxDimSc].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[pozMaxDimSc].reward = newLstateE.reward;

					stateE  newRstateE = nextstateE(toExpand.trajectory[pozMaxDimSc - 1], unnormU(uRmidle));
					memcpy(rightSegment.trajectory[pozMaxDimSc].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[pozMaxDimSc].reward = newRstateE.reward;
				}
				currentBudget = currentBudget + 2;
				unsigned int stepTmp = pozMaxDimSc + 1;

				while (stepTmp < toExpand.sequenceLenght)//toExpand.splits[stepTmp]!=0 || stepTmp>=initlen) //recalculate trajectory for left, right, center segments
				{
					//cout << "while2 at: " << stepTmp << " "<< toExpand.sequenceLenght <<endl;
					stateE newLstateE;// = new stateE;
					stateE newRstateE;// = new stateE;
					uLmidle = middleOfBox(toExpand.startPoint[stepTmp], toExpand.splits[stepTmp]);
					uRmidle = middleOfBox(toExpand.startPoint[stepTmp], toExpand.splits[stepTmp]);
					// stateE newCstateE;// = new stateE;

					//{
					newLstateE = nextstateE(leftSegment.trajectory[stepTmp - 1], unnormU(uLmidle));
					newRstateE = nextstateE(rightSegment.trajectory[stepTmp - 1], unnormU(uRmidle));
					//newCstateE =  nextstateE(toExpand.trajectory[stepTmp-1], unnormU(uCmidle));
					//}
					memcpy(leftSegment.trajectory[stepTmp].variables, newLstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					leftSegment.trajectory[stepTmp].reward = newLstateE.reward;

					memcpy(rightSegment.trajectory[stepTmp].variables, newRstateE.variables, sizeof(double) * nbHiddenstateEVariables);
					rightSegment.trajectory[stepTmp].reward = newRstateE.reward;

					stepTmp++;
					currentBudget = currentBudget + 2;
				}
			}
			
			toExpand.rewardSum = recalculateRewardSum(toExpand.trajectory, toExpand.sequenceLenght);
			toExpand.splitsSum += 1;
			leftSegment.rewardSum = recalculateRewardSum(leftSegment.trajectory, leftSegment.sequenceLenght);
			leftSegment.splitsSum += 1;
			rightSegment.rewardSum = recalculateRewardSum(rightSegment.trajectory, rightSegment.sequenceLenght);
			rightSegment.splitsSum += 1;
			//=====================INSERT the expanded box at good depth range!====================
			
			

			if (depthLinks[toExpand.splitsSum] != data.end() )
			{
				list<soopDataSet>::iterator it_tmp = depthLinks[toExpand.splitsSum];
				++it_tmp;
				data.insert(it_tmp, toExpand);
				data.insert(it_tmp, leftSegment);
				data.insert(it_tmp, rightSegment);				
			}
			else {
				if (toExpand.splitsSum > currentMaxDepth)
				{
					data.push_back(toExpand);
					depthLinks[toExpand.splitsSum] = --data.end();
					data.push_back(leftSegment);
					data.push_back(rightSegment);
				}
				else
				{
					list<soopDataSet>::iterator it_tmp = depthLinks[toExpand.splitsSum + 1];
					data.insert(it_tmp, toExpand);
					data.insert(it_tmp, leftSegment);
					data.insert(it_tmp, rightSegment);
				}
			}
			// update max depth counter 
			if (currentMaxDepth < toExpand.splitsSum)
				currentMaxDepth = toExpand.splitsSum;
		}
		
		if (currentBudget > budget) stopFlag = true;
		if (newDimensionAdded) maxLengthSequence++;
	}       //END while loop

#ifdef DEBUG_TIME
	cout << "box select= " << timeBoxSelect / nrTimeSamples << " ,dimSelect= " << timeDimSelect / nrTimeSamples << " ,split= " << timeSplit / nrTimeSamples << " ,n= " << nrTimeSamples;

	__int64 frequencyS, startS, endtS, totalS;
	QueryPerformanceFrequency((LARGE_INTEGER *)&frequencyS);
	QueryPerformanceCounter((LARGE_INTEGER *)&startS);
#endif
	//update max length of sequences



	double maxRewardSum = 0.0, commandSignalToSend = 0.0;
	for (std::list<soopDataSet>::iterator itTMP = data.begin(); itTMP != data.end(); ++itTMP)
	{
		if ((*itTMP).rewardSum > maxRewardSum)
		{
			commandSignalToSend = unnormU(middleOfBox((*itTMP).startPoint[0], (*itTMP).splits[0]));
			maxRewardSum = (*itTMP).rewardSum;
		}
	}
	//cout << "myserach RS max " << maxRewardSum << " U " << commandSignalToSend << endl;


	data.sort(compareRewardSum());
	list<soopDataSet>::iterator itmax = data.begin();

	double * commandSequence = (double*)malloc(sizeof(double)*initlen);

	for (int i = 0; i<initlen; i++)
	{
		commandSequence[i] = unnormU(middleOfBox((*itmax).startPoint[i], (*itmax).splits[i]));
	}

	freeSoopDataList();

#ifdef DEBUG_TIME
	QueryPerformanceCounter((LARGE_INTEGER *)&endtS);
	double timeEnd = (endtS - startS) * 1000.0 / frequencyS;
	cout << " ,end=" << timeEnd << endl;
#endif

	return commandSequence;
}

>>>>>>> 732637a9a341cf88aaa5fd50228eb4cf9e105093
