#ifndef SOOP_H
#define SOOP_H

#define DEBUG_//FN

extern double gamma;// = 0.95;                     //Discount factor
extern double alpha;// = 0.7;                      //An initial max length guess
extern unsigned int budget;// = 20;                //Number of actions calculated in advanced
extern double L_lp;// = 0.95;                     //Discount factor

double * sopc(stateE& s0);
double * soop(stateE& s0);
double * OPC_lp(stateE& s0);
double * OPC(stateE& s0);
double * soop_soo(stateE& s0);


#ifdef DEBUG_FN
//#include "stdafx.h"
const unsigned int initlen = 50;
typedef struct {
	double startPoint [initlen];       //starting points of U domains, for each depth and sequence
	unsigned int splits [initlen];     //number of splits in each sequences element
	stateE trajectory [initlen];       // stateE trajectory for center action samples: dimx X and # of sequences also,
	unsigned int sequenceLenght ;//= 0;                                   // rewards RESULTING from center action samples: # of sequences
	//double reward [initlen];         // rewards RESULTING from center action samples: # of sequences
	double rewardSum;                   // the sum of rewards in the sequence
	unsigned int ID;
}soopDataSet;

int dimentionSelectionOPC(int length, unsigned int * splits);
void initSoopData(soopDataSet& newData);
void fillSoopData(soopDataSet& destination, double *sP, unsigned int *s, stateE t[],/* double *r,*/ double rS, int sequneceLenght);
double middleOfBox(double startPoint, int splits);
double unnormU(double u);
double recalculateRewardSum(stateE t[], int nr);

int BoxSelection();

#endif

#endif
