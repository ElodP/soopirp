
//include "stdafx.h"
#include "testerART.h"
                      //Time step between two stateE

//const double maxVoltage = 15.0;
/*+-----------------------Model's parameters---------------------+
  |																 |
  |                                         					 |
  |																 |
  +--------------------------------------------------------------+*/

static double g = 9.80665;		// gravity
/* =========== Inverted Pendulum Parameters ==============*/
static double Beq = 0.004;      // Equivalent viscous damping coefficient
static double Jeq = 0.0035842;  // Moment of inertia of the arm and pendulum about the axis of θ
static double Jm = 3.87e-7;     // Moment of inertia of the rotor of the motor
static double Kg = 70;          // SRV02 sys. gear ratio (14*5)
static double Km = 0.00767;     // Back-emf const.
static double Kt = 0.00767;     // Motor-torque const.
static double L = 0.1685;       // half length of pendulum
static double m = 0.127;        // mass of pendulum
static double r = 0.216;        // rotating arm length
static double Rm = 2.6;         // armature resistance
static double NUg = 0.9;        // gearbox efficiency
static double NUm = 0.69;       // motor efficiency

static double  dThetaMAX = 11.0;
static double  dlambdaMAX = 11.0;

//static double qReward = 1.0;
//static double rReward = 0.01;

/* ========= Parameters for the model ==============*/
//double aP = Jeq+m*pow(r,2)+NUg*pow(Kg,2)*Jm;
double aP = Jeq+m*r*r+NUg*pow(Kg,2)*Jm;
double bP = m*L*r;
double cP = (4*m*(pow(L,2)))/3;
double dP = m*g*L;
double eP = Beq+(NUm*NUg*Kt*Km*pow(Kg,2))/Rm;
double fP = (NUm*NUg*Kt*Kg)/Rm;

double normalizeAngle(double a)
{
    if (a>=0) return fmod((double)(a+PI), double(2.0*PI))-PI;
    else return fmod((double)(a-PI), double(2.0*PI))+PI;

}

double normalizeVelo(double a)  /*k*/
{
    if ( a > veloSaturation )
        return veloSaturation;
    else if ( a < -veloSaturation )
        return -veloSaturation;
    else
        return a;
}

void initstateE(stateE& s) {

	//s = (stateE*)malloc(sizeof(stateE));
//	s.variables = (double*)malloc(sizeof(double) * nbHiddenstateEVariables);

	s.variables[0] = 0.0;	// lambda
	s.variables[1] = 0.0;	// dLambda
	s.variables[2] = 0.0;	// theta
	s.variables[3] = 0.0;	// dTheta
	s.isTerminal = 0;
	s.reward = 0;
	//return initial;

}

/* Returns a triplet containing the next stateE, the applied action and the reward given the current stateE and action. */

stateE nextstateE(stateE s, double voltageSignal) {
// =========== TO DO, care !!* /!!
 //   std::cout<< std::endl;
   // std::cout<< "in nextstateE"<< std::endl;
	stateE newstateE;// = (stateE*)malloc(sizeof(stateE));
//	newstateE.variables = (double*)malloc(sizeof(double) * nbHiddenstateEVariables);
	memcpy(newstateE.variables, s.variables, sizeof(double) * nbHiddenstateEVariables);
	newstateE.isTerminal = s.isTerminal;

    double Vm = voltageSignal;

    double lambda = s.variables[0], lambda_old = s.variables[0];
    double dLambda = s.variables[1], dLambda_old = dLambda;
    double theta = s.variables[2], theta_old =  theta;
    double dTheta = s.variables[3], dTheta_old = dTheta; 
	double partialTimeStep = timeStep/eulerStep;
	double lambda_tmp, dLambda_tmp, theta_tmp, dTheta_tmp;
	double tmp_inModel = 0.0;

	for(unsigned int iStep = 0; iStep < eulerStep; iStep++ )
	{
		 theta_tmp = theta + partialTimeStep*dTheta;
		 tmp_inModel = (1/(aP*cP-pow(bP,2)*pow(cos(lambda),2))) ;
		 //dTheta_tmp = dTheta + partialTimeStep*( (1/(aP*cP-pow(bP,2)*pow(cos(lambda),2))) *(-bP*cP*sin(lambda)*pow(dLambda,2)+bP*dP*sin(lambda)*cos(lambda)-cP*eP*dTheta+cP*fP*Vm ) );
		 dTheta_tmp = dTheta + partialTimeStep*( tmp_inModel *(-bP*cP*sin(lambda)*pow(dLambda,2)+bP*dP*sin(lambda)*cos(lambda)-cP*eP*dTheta+cP*fP*Vm ) );
		 lambda_tmp = lambda + partialTimeStep*dLambda;
		 //dLambda_tmp = dLambda + partialTimeStep*( (1/(aP*cP-pow(bP,2)*pow(cos(lambda),2))) *(aP*dP*sin(lambda)-pow(bP,2)*sin(lambda)*cos(lambda)*pow(dLambda,2)-bP*eP*cos(lambda)*dTheta+bP*fP*cos(lambda)*Vm ) );
		 dLambda_tmp = dLambda + partialTimeStep*( tmp_inModel *(aP*dP*sin(lambda)-pow(bP,2)*sin(lambda)*cos(lambda)*pow(dLambda,2)-bP*eP*cos(lambda)*dTheta+bP*fP*cos(lambda)*Vm ) );
		 
		 lambda = lambda_tmp;
		 dLambda = dLambda_tmp;
		 theta = theta_tmp;
		 dTheta = dTheta_tmp;
	}


    //========== Normalization of lambda and theta
  //   std::cout << "alpN " << lambdaNext << "thN " << thetaNext ;

	lambda=normalizeAngle(lambda);
	theta=normalizeAngle(theta);
	dLambda=normalizeVelo(dLambda); /*Koppany*/
	dTheta=normalizeVelo(dTheta);   /*Koppany*/

	/*
    if (lambda>0)
            lambda = fmod((double)(lambda+PI), (double)(2.0*PI))-PI;
    else
            lambda = fmod((double)(lambda-PI), (double)(2.0*PI))+PI;
    if (theta>0)
            theta = fmod((double)(theta+PI), (double)(2.0*PI))-PI;
    else
            theta = fmod((double)(theta-PI), (double)(2.0*PI))+PI;
			*/
    newstateE.variables[0] = lambda;
    newstateE.variables[1] = dLambda;
    newstateE.variables[2] = theta;
    newstateE.variables[3] = dTheta;

    double newReward;
    if(s.isTerminal) {
		newReward = 0.0;
	} else {
		//double tmp = fabs(s.variables[0]*qReward + voltageSignal*rReward);
        //if (tmp >1) {tmp=tmp/tmp;}

		newReward = 1.0 - (pow(lambda_old,2)*qReward + pow(dLambda_old,2)*qRewardd + pow(dTheta_old,2)*qRewarddTheta + pow(theta_old,2)*qRewardTheta + pow(voltageSignal,2)*rReward)/(pow(PI,2)*qReward + pow(dlambdaMAX,2)*qRewardd + pow(dThetaMAX,2)*qRewarddTheta + pow(PI,2)*qRewardTheta + pow(maxVoltage,2)*rReward);
	}
    newstateE.reward=newReward;
  //  std::cout<< "R "<< newReward << std::endl;
  //   std::cout<< std::endl;
   // std::cout.precision(10);
   // std::cout << "alp" << lambdaNext << "th" << thetaNext << "r" << newReward<< std::endl;
    return newstateE;

}

void copyStateE(stateE& destination, stateE original)
{
   // std::cout<< "   in copyStateE"<< std::endl;
   // std::cout<< "in copyStateE"<< destination.variables[0]<< std::endl;

    initstateE(destination);

   // std::cout<< "   in copyStateE after IF"<< std::endl;
	destination.variables[0] = original.variables[0];	// lambda
	//std::cout<< "   in copyStateE after IF1"<< std::endl;
	destination.variables[1] = original.variables[1];	// dLambda
	//std::cout<< "   in copyStateE after IF2"<< std::endl;
	destination.variables[2] = original.variables[2];	// theta
	//std::cout<< "   in copyStateE after IF3"<< std::endl;
	destination.variables[3] = original.variables[3];	// dTheta
   // std::cout<< "   in copyStateE after IF4"<< std::endl;
	destination.isTerminal = 0;
   // std::cout<< "   in copyStateE after IF5"<< std::endl;
	destination.reward = original.reward;
	//std::cout<< "   out copyStateE"<< std::endl;
}


//================ END my code ===========================
//========================================================

