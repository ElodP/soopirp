#include <acado_toolkit.hpp>
#include <acado_gnuplot.hpp>

int main( ) 
{  
    USING_NAMESPACE_ACADO

	static double g = 9.80665;	// gravity
	/* =========== Inverted Pendulum Parameters ==============*/
	static double Beq = 0.004;      // Equivalent viscous damping coefficient
	static double Jeq = 0.0035842;  // Moment of inertia of the arm and pendulum about the axis of θ
	static double Jm = 3.87e-7;     // Moment of inertia of the rotor of the motor
	static double Kg = 70;          // SRV02 sys. gear ratio (14*5)
	static double Km = 0.00767;     // Back-emf const.
	static double Kt = 0.00767;     // Motor-torque const.
	static double L = 0.1685;       // half length of pendulum
	static double m = 0.127;        // mass of pendulum
	static double r = 0.216;        // rotating arm length
	static double Rm = 2.6;         // armature resistance
	static double NUg = 0.9;        // gearbox efficiency
	static double NUm = 0.69;       // motor efficiency
	/* ========= Parameters for the model ==============*/
	//double aP = Jeq+m*pow(r,2)+NUg*pow(Kg,2)*Jm;
	double aP = Jeq+m*r*r+NUg*pow(Kg,2)*Jm;
	double bP = m*L*r;
	double cP = (4*m*(pow(L,2)))/3;
	double dP = m*g*L;
	double eP = Beq+(NUm*NUg*Kt*Km*pow(Kg,2))/Rm;
	double fP = (NUm*NUg*Kt*Kg)/Rm;



	// INTRODUCE THE VARIABLES:
	// -------------------------
	DifferentialState theta;
	DifferentialState lambda;
	DifferentialState thetaD;
	DifferentialState lambdaD;

	//Disturbance R;
	Control Vm;


	// DEFINE THE DYNAMIC SYSTEM:
	// --------------------------
	DifferentialEquation f;

	f << dot(theta) == thetaD;
	f << dot(lambda) == lambdaD;
	f << dot(thetaD) == (1/(aP*cP-pow(bP,2)*pow(cos(lambda),2))) *(-bP*cP*sin(lambda)*pow(lambdaD,2)+bP*dP*sin(lambda)*cos(lambda)-cP*eP*thetaD+cP*fP*Vm ) ;
	f << dot(lambdaD) == (1/(aP*cP-pow(bP,2)*pow(cos(lambda),2))) *(aP*dP*sin(lambda)-pow(bP,2)*sin(lambda)*cos(lambda)*pow(lambdaD,2)-bP*eP*cos(lambda)*thetaD+bP*fP*cos(lambda)*Vm );



   
    Parameter mB;
    double mW = 50.0;
    double kS = 20000.0;
    double kT = 200000.0;

/*  // if I dont define an output, all dif states are in g()
    OutputFcn g;
    g << xB;
    g << 500.0*vB + F;

    DynamicSystem dynSys( f,g );
*/
	    DynamicSystem dynSys( f );	

    // SETUP THE PROCESS:
    // ------------------
    Process myProcess;

    myProcess.setDynamicSystem( dynSys,INT_RK45 );
    myProcess.set( ABSOLUTE_TOLERANCE,1.0e-8 );

    Vector x0( 4 );
    x0.setZero( );
    x0( 0 ) = 0.01;

    myProcess.initializeStartValues( x0 );

// we dont have disturbance in the sys    
// myProcess.setProcessDisturbance( "road.txt" );

    myProcess.set( PLOT_RESOLUTION,HIGH );

    GnuplotWindow window;
        window.addSubplot( lambda, "l [deg]" );
        window.addSubplot( lambdaD, "lD [deg/sec]" );
        window.addSubplot( theta, "t Velocity [deg]" );
        window.addSubplot( thetaD, "tD [deg/s]" );

        window.addSubplot( Vm,"Damping Force [N]" );

    myProcess << window;


    // SIMULATE AND GET THE RESULTS:
    // -----------------------------
    VariablesGrid u( 1,0.0,1.0,6 );

    u( 0,0 ) = 0.0;
    u( 1,0 ) = 0.0;
    u( 2,0 ) = 0.0;
    u( 3,0 ) = 0.0;
    u( 4,0 ) = 0.0;
    u( 5,0 ) = 0.0;

    Vector p( 1 );
    p(0) = 350.0;

    myProcess.init( 0.0 );
    myProcess.run( u,p );


    VariablesGrid xSim, ySim;

    myProcess.getLast( LOG_SIMULATED_DIFFERENTIAL_STATES,xSim );
    xSim.print( "Simulated Differential States" );

    myProcess.getLast( LOG_PROCESS_OUTPUT,ySim );
    ySim.print( "Process Output" );

    return 0;
}
