/*
 *    This file is part of ACADO Toolkit.
 *
 *    ACADO Toolkit -- A Toolkit for Automatic Control and Dynamic Optimization.
 *    Copyright (C) 2008-2014 by Boris Houska, Hans Joachim Ferreau,
 *    Milan Vukov, Rien Quirynen, KU Leuven.
 *    Developed within the Optimization in Engineering Center (OPTEC)
 *    under supervision of Moritz Diehl. All rights reserved.
 *
 *    ACADO Toolkit is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 3 of the License, or (at your option) any later version.
 *
 *    ACADO Toolkit is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with ACADO Toolkit; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */



/**
 *    \file   examples/ocp/time_optimal_rocket.cpp
 *    \author Boris Houska, Hans Joachim Ferreau
 *    \date   2009
 */


#include <acado_optimal_control.hpp>
#include <acado_gnuplot.hpp>


int main( ){

	USING_NAMESPACE_ACADO

		/* =========== Inverted Pendulum Parameters ==============*/
		static double g = 9.80665;		// gravity
	static double Beq = 0.004;      // Equivalent viscous damping coefficient
	static double Jeq = 0.0035842;  // Moment of inertia of the arm and pendulum about the axis of θ
	static double Jm = 3.87e-7;     // Moment of inertia of the rotor of the motor
	static double Kg = 70;          // SRV02 sys. gear ratio (14*5)
	static double Km = 0.00767;     // Back-emf const.
	static double Kt = 0.00767;     // Motor-torque const.
	static double L = 0.1685;       // half length of pendulum
	static double m = 0.127;        // mass of pendulum
	static double r = 0.216;        // rotating arm length
	static double Rm = 2.6;         // armature resistance
	static double NUg = 0.9;        // gearbox efficiency
	static double NUm = 0.69;       // motor efficiency
	static double  dThetaMAX = 11.0;
	static double  dlambdaMAX = 11.0;

	/* ========= Parameters for the model ==============*/
	//double aP = Jeq+m*pow(r,2)+NUg*pow(Kg,2)*Jm;
	double aP = Jeq+m*r*r+NUg*pow(Kg,2)*Jm;
	double bP = m*L*r;
	double cP = (4*m*(pow(L,2)))/3;
	double dP = m*g*L;
	double eP = Beq+(NUm*NUg*Kt*Km*pow(Kg,2))/Rm;
	double fP = (NUm*NUg*Kt*Kg)/Rm;

	// INTRODUCE THE VARIABLES:
	// -------------------------

	DifferentialState     theta,lambda,thetaD,lambdaD;
	Control               u    ;
	Parameter             T    ;

	DifferentialEquation  f( 0.0, T );


	// DEFINE A DIFFERENTIAL EQUATION:
	// -------------------------------

	f << dot(theta) == thetaD;
	f << dot(lambda) == lambdaD;

	f << dot(thetaD) == (1/(aP*cP-pow(bP,2)*pow(cos(lambda),2))) *(-bP*cP*sin(lambda)*pow(lambdaD,2)+bP*dP*sin(lambda)*cos(lambda)-cP*eP*thetaD+cP*fP*u ) ;
	f << dot(lambdaD) == (1/(aP*cP-pow(bP,2)*pow(cos(lambda),2))) *(aP*dP*sin(lambda)-pow(bP,2)*sin(lambda)*cos(lambda)*pow(lambdaD,2)-bP*eP*cos(lambda)*thetaD+bP*fP*cos(lambda)*u ) ;


	// DEFINE AN OPTIMAL CONTROL PROBLEM:
	// ----------------------------------
	OCP ocp( 0, T, 50 );

	ocp.minimizeMayerTerm( T );
	ocp.subjectTo( f );

	ocp.subjectTo( AT_START, theta ==  0.0 );
	ocp.subjectTo( AT_START, lambda ==  0.0 );
	ocp.subjectTo( AT_START, thetaD ==  0.0 );
	ocp.subjectTo( AT_START, lambdaD ==  0.0 );


	ocp.subjectTo( AT_END  , lambda == M_PI/2.0 );
	ocp.subjectTo( AT_END  , lambdaD ==  0.0 );

	ocp.subjectTo( -9.0 <= u <=  9.0  );
	ocp.subjectTo(  5.0 <= T <= 15.0  );


	// VISUALIZE THE RESULTS IN A GNUPLOT WINDOW:
	// ------------------------------------------
	GnuplotWindow window;
	window.addSubplot( lambda, "\lambda"      );
	window.addSubplot( theta, "\theta"      );
	window.addSubplot( lambdaD, "dot{\lambda}"  );
	window.addSubplot( u, "THE CONTROL INPUT [V]" );


	// DEFINE AN OPTIMIZATION ALGORITHM AND SOLVE THE OCP:
	// ---------------------------------------------------
	OptimizationAlgorithm algorithm(ocp);

	algorithm.set( MAX_NUM_ITERATIONS, 100 );
	// 	algorithm.set( HESSIAN_APPROXIMATION, EXACT_HESSIAN );
	// 	algorithm.set( HESSIAN_PROJECTION_FACTOR, 1.0 );

	algorithm << window;


	//     algorithm.initializeDifferentialStates("tor_states.txt");
	//     algorithm.initializeParameters("tor_pars.txt");
	//     algorithm.initializeControls("tor_controls.txt");

	algorithm.solve();

	//     algorithm.getDifferentialStates("tor_states.txt");
	//     algorithm.getParameters("tor_pars.txt");
	//     algorithm.getControls("tor_controls.txt");

	return 0;
}



