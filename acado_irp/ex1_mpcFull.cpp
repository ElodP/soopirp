#include <acado_toolkit.hpp>
#include <acado_gnuplot.hpp>
#undef ACADO_WITH_TESTING

int main( )
{
    USING_NAMESPACE_ACADO
            /* =========== Inverted Pendulum Parameters ==============*/
            static double g = 9.80665;		// gravity
    static double Beq = 0.004;      // Equivalent viscous damping coefficient
    static double Jeq = 0.0035842;  // Moment of inertia of the arm and pendulum about the axis of θ
    static double Jm = 3.87e-7;     // Moment of inertia of the rotor of the motor
    static double Kg = 70;          // SRV02 sys. gear ratio (14*5)
    static double Km = 0.00767;     // Back-emf const.
    static double Kt = 0.00767;     // Motor-torque const.
    static double L = 0.1685;       // half length of pendulum
    static double m = 0.127;        // mass of pendulum
    static double r = 0.216;        // rotating arm length
    static double Rm = 2.6;         // armature resistance
    static double NUg = 0.9;        // gearbox efficiency
    static double NUm = 0.69;       // motor efficiency
    static double  dThetaMAX = 11.0;
    static double  dlambdaMAX = 11.0;

    /* ========= Parameters for the model ==============*/
    //double aP = Jeq+m*pow(r,2)+NUg*pow(Kg,2)*Jm;
    double aP = Jeq+m*r*r+NUg*pow(Kg,2)*Jm;
    double bP = m*L*r;
    double cP = (4*m*(pow(L,2)))/3;
    double dP = m*g*L;
    double eP = Beq+(NUm*NUg*Kt*Km*pow(Kg,2))/Rm;
    double fP = (NUm*NUg*Kt*Kg)/Rm;

    double qReward = 1;								// Q reward for lambda
    double qRewardD = 0.001;	 //0.0							// Q revard for Dlambda
    double qRewardTheta = 0.1;		//0.07					// Q reward for theta
    double qRewardDTheta = 0.1;	//0.0					// Q reward for dtheta
    double rReward = 0.1;	 //0.03
    // INTRODUCE THE VARIABLES:
    // -------------------------

    DifferentialState     theta,lambda,thetaD,lambdaD;
    Control               u    ;
    Parameter             T    ;

    DifferentialEquation  f;//( 0.0, T ); // <-add pharanties if simulation


    // DEFINE A DIFFERENTIAL EQUATION:
    // -------------------------------

    f << dot(theta) == thetaD;
    f << dot(lambda) == lambdaD;
    f << dot(thetaD) == (1/(aP*cP-pow(bP,2)*pow(cos(lambda),2))) *(-bP*cP*sin(lambda)*pow(lambdaD,2)+bP*dP*sin(lambda)*cos(lambda)-cP*eP*thetaD+cP*fP*u ) ;
    f << dot(lambdaD) == (1/(aP*cP-pow(bP,2)*pow(cos(lambda),2))) *(aP*dP*sin(lambda)-pow(bP,2)*sin(lambda)*cos(lambda)*pow(lambdaD,2)-bP*eP*cos(lambda)*thetaD+bP*fP*cos(lambda)*u ) ;
    // if I dont define g then the output is identical with the state variables
    /*
    OutputFcn g;
    g << xB;
    g << 500.0*vB + F;
    DynamicSystem dynSys( f, g );
    */

    // SETTING UP THE (SIMULATED) PROCESS:
    // -----------------------------------
    OutputFcn identity;
    DynamicSystem dynamicSystem( f,identity );

    Process process( dynamicSystem,INT_RK45 );

    //VariablesGrid disturbance = readFromFile( "road.txt" );
    //process.setProcessDisturbance( disturbance );


    Function h;

    h << theta;
    h << thetaD;
    h << lambda;
    h << lambdaD;
    h << u;

    // LSQ coefficient matrix
    DMatrix Q(5,5);
    Q(0,0) = qRewardTheta;
    Q(1,1) = qRewardDTheta;
    Q(2,2) = qReward;
    Q(3,3) = qRewardD;
    Q(4,4) = rReward;

    // Reference
    DVector ref(5);
    ref.setAll( 0.0 );
    ref(2) = M_PI/2.0;


    // DEFINE AN OPTIMAL CONTROL PROBLEM:
    // ----------------------------------
    const double tStart = 0.0;
    const double tEnd   = 25.0;

    OCP ocp( tStart, tEnd, 20 );

    ocp.minimizeLSQ( Q, h, ref );

    ocp.subjectTo( f );

    ocp.subjectTo( -9.0 <= u <= 9.0 );


    // SETTING UP THE REAL-TIME ALGORITHM:
    // -----------------------------------
    RealTimeAlgorithm alg( ocp,0.025 );
    alg.set( INTEGRATOR_TYPE, INT_RK78 );
    alg.set( MAX_NUM_ITERATIONS, 50 );
    alg.set( PLOT_RESOLUTION, MEDIUM );

    StaticReferenceTrajectory zeroReference;
    Controller controller( alg,zeroReference );

    /*
    GnuplotWindow window;
    window.addSubplot( lambda, "lambda" );
    window.addSubplot( theta, "theta" );
    window.addSubplot( u, "voltage" );

    window.plot();
    */
    // SETUP CONTROLLER AND PERFORM A STEP:
    // ------------------------------------
    //StaticReferenceTrajectory zeroReference( "ref_ex1.txt" );
   // Controller controller( alg ); //,zeroReference );

    SimulationEnvironment sim( 0.0,2.5,process,controller );

    DVector x0( 4 );
    x0.setZero( );
    x0(0) = 0.01;

    sim.init(x0);
    sim.run();
    /*
    controller.init( x0 );
    controller.run( x0 );

    DVector u_;
    controller.getU( u_ );
    u_.print( "Feedback control" );
    */

    // ... AND PLOT THE RESULTS
    // ------------------------
    VariablesGrid diffStates;
    sim.getProcessDifferentialStates( diffStates );

    VariablesGrid feedbackControl;
    sim.getFeedbackControl( feedbackControl );

    GnuplotWindow window;
    window.addSubplot( lambda, "lambda" );
    window.addSubplot( theta, "theta" );
    window.addSubplot( u, "voltage" );
    /*
    window.addSubplot( diffStates(0),   "theta" );
    window.addSubplot( diffStates(1),   "theta velocity" );
    window.addSubplot( diffStates(2),   "lambda" );
    window.addSubplot( diffStates(3),   "lambdaVelocity" );
    window.addSubplot( feedbackControl, "voltage" );
    */
    window.plot();
    // part of the system simulation, not showing up anything u1 paparmeter must be checked
    // ====================================================================================
    /*
    DynamicSystem dynSys( f );

    // SETUP THE PROCESS:
    // ------------------
    Process myProcess;

    myProcess.setDynamicSystem( dynSys,INT_RK45 );
    myProcess.set( ABSOLUTE_TOLERANCE,1.0e-8 );

    DVector x0( 4 );
    x0.setZero( );
    x0( 0 ) = 0.01;

    //myProcess.initializeStartValues( x0 );
    //myProcess.setProcessDisturbance( "road.txt" );

    myProcess.set( PLOT_RESOLUTION,HIGH );

    GnuplotWindow window;
    window.addSubplot( lambda, "lambda" );
    window.addSubplot( theta, "theta" );
    window.addSubplot( u, "voltage" );


    myProcess << window;


    // SIMULATE AND GET THE RESULTS:
    // -----------------------------
    VariablesGrid u1( 0,0.0,0.0,0 );

    u1( 0,0 ) = 0;
    u1( 1,0 ) = -0.0;
    u1( 2,0 ) = 0.0;
    u1( 3,0 ) = 0.0;
    u1( 4,0 ) = 0.0;
    u1( 5,0 ) = 0.0;

    DVector p( 1 );
    p(0) = 350.0;

    myProcess.init( 0.0 );
    myProcess.run( u1,p );


    VariablesGrid xSim, ySim;

    myProcess.getLast( LOG_SIMULATED_DIFFERENTIAL_STATES,xSim );
    xSim.print( "Simulated Differential States" );

    myProcess.getLast( LOG_PROCESS_OUTPUT,ySim );
    ySim.print( "Process Output" );
*/
    return 0;
}


