#include <acado_toolkit.hpp>
#include <acado_gnuplot.hpp>
//#include <vector.hpp>

int main( ) 
{  
	USING_NAMESPACE_ACADO
		static double g = 9.80665;	// gravity
	/* =========== Inverted Pendulum Parameters ==============*/
	static double Beq = 0.004;      // Equivalent viscous damping coefficient
	static double Jeq = 0.0035842;  // Moment of inertia of the arm and pendulum about the axis of θ
	static double Jm = 3.87e-7;     // Moment of inertia of the rotor of the motor
	static double Kg = 70;          // SRV02 sys. gear ratio (14*5)
	static double Km = 0.00767;     // Back-emf const.
	static double Kt = 0.00767;     // Motor-torque const.
	static double L = 0.1685;       // half length of pendulum
	static double m = 0.127;        // mass of pendulum
	static double r = 0.216;        // rotating arm length
	static double Rm = 2.6;         // armature resistance
	static double NUg = 0.9;        // gearbox efficiency
	static double NUm = 0.69;       // motor efficiency
	/* ========= Parameters for the model ==============*/
	//double aP = Jeq+m*pow(r,2)+NUg*pow(Kg,2)*Jm;
	double aP = Jeq+m*r*r+NUg*pow(Kg,2)*Jm;
	double bP = m*L*r;
	double cP = (4*m*(pow(L,2)))/3;
	double dP = m*g*L;
	double eP = Beq+(NUm*NUg*Kt*Km*pow(Kg,2))/Rm;
	double fP = (NUm*NUg*Kt*Kg)/Rm;



	// INTRODUCE THE VARIABLES:
	// -------------------------
	DifferentialState theta;
	DifferentialState lambda;
	DifferentialState thetaD;
	DifferentialState lambdaD;

	//Disturbance R;
	Control Vm;


	// DEFINE THE DYNAMIC SYSTEM:
	// --------------------------
	DifferentialEquation f;

	f << dot(theta) == thetaD;
	f << dot(lambda) == lambdaD;
	f << dot(thetaD) == (1/(aP*cP-pow(bP,2)*pow(cos(lambda),2))) *(-bP*cP*sin(lambda)*pow(lambdaD,2)+bP*dP*sin(lambda)*cos(lambda)-cP*eP*thetaD+cP*fP*Vm ) ;
	f << dot(lambdaD) == (1/(aP*cP-pow(bP,2)*pow(cos(lambda),2))) *(aP*dP*sin(lambda)-pow(bP,2)*sin(lambda)*cos(lambda)*pow(lambdaD,2)-bP*eP*cos(lambda)*thetaD+bP*fP*cos(lambda)*Vm );
	/*
	   OutputFcn g;
	   g << theta;
	   g << lambda;
	   */
	DynamicSystem dynSys( f );


	// DEFINE LEAST SQUARE FUNCTION:
	// -----------------------------
	Function h;

	h << theta;
	h << lambda;
	h << thetaD;
	h << lambdaD;
	h << Vm;

	// LSQ coefficient matrix
	DMatrix Q(5,5);
	Q(0,0) = 10.0;
	Q(1,1) = 10.0;
	Q(2,2) = 1.0;
	Q(3,3) = 1.0;
	Q(4,4) = 1.0e-8;

	// Reference
	DVector r_(5); 
	r_.setAll( 0.0 );


	// DEFINE AN OPTIMAL CONTROL PROBLEM:
	// ----------------------------------
	const double tStart = 0.0;
	const double tEnd   = 1.0;

	OCP ocp( tStart, tEnd, 20 );

	ocp.minimizeLSQ( Q, h, r_ );

	ocp.subjectTo( f );
	ocp.subjectTo( AT_START, theta ==  M_PI/2.0 );
	ocp.subjectTo( AT_START, thetaD ==  0.0 );
	ocp.subjectTo( AT_START, lambda ==  M_PI/2.0 );
	ocp.subjectTo( AT_START, lambdaD ==  0.0 );

	ocp.subjectTo( AT_END  , lambda == 0.0 );

	ocp.subjectTo( -M_PI <= theta <=  M_PI  );
	ocp.subjectTo( -M_PI <= lambda <=  M_PI  );
	ocp.subjectTo( -11.0 <= thetaD <=  11.0  );

	ocp.subjectTo( -9.0 <= Vm <= 9.0 );
	// ocp.subjectTo( R == 0.0 );


	// SETTING UP THE REAL-TIME ALGORITHM:
	// -----------------------------------
	RealTimeAlgorithm alg( ocp,0.025 );
	alg.set( MAX_NUM_ITERATIONS, 1 );
	alg.set( PLOT_RESOLUTION, MEDIUM );

	GnuplotWindow window;
	window.addSubplot( theta, "Body Position [m]" );
	window.addSubplot( lambda, "Wheel Position [m]" );
	window.addSubplot( thetaD, "Body Velocity [m/s]" );
	window.addSubplot( lambdaD, "Wheel Velocity [m/s]" );
	window.addSubplot( Vm,  "Damping Force [N]" );
	//        window.addSubplot( R,  "Road Excitation [m]" );

	alg << window;

//	alg.solve();
	// SETUP CONTROLLER AND PERFORM A STEP:
	// ------------------------------------
	//    StaticReferenceTrajectory zeroReference( "ref.txt" );
	/*
	   Controller controller( alg,zeroReference );

	   DVector y(4);
	   y.setZero( );
	   y(0) = 0.01;

	   controller.init( 0.0,y );
	   controller.step( 0.0,y );
	   */
	DVector y(4);
	y.setZero( );
	y(0) = 0.01;

	Controller controller( alg );
	controller.init( 0.0,y );
	controller.step( 0.0,y );

	return 0;
}
