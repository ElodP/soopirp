#include "testerART.h"

#ifdef RUN_OPD

//#include "opd.h"

//const double U[M]={-maxVoltage, -maxVoltage/2.0, 0, maxVoltage/2.0, maxVoltage};		//--use here maxVoltage
const double U[M] = {-maxVoltage, 0, maxVoltage};		//--use here maxVoltage
const unsigned short nsim = 0;	//--compputational budget, use either this or the below one (set one of them to zero of not used)
const unsigned short n = budget;	//--computational budget


double maxR, prevnu;
unsigned short maxDepth, prevd;

//--vars used in recursive check of maxBval
double maxBval;
node* maxBnode;
node* maxRnode;


node* initMyNode( stateE& X_and_r, node* parent, unsigned short u, unsigned short depth, bool isLeaf, double R, unsigned short S )
{
	node* newNode = new node;
	newNode->X_and_r = X_and_r;
	newNode->parent = parent; 	//-- check whether this is the correct call for a pointer allocation
	newNode->u = u;
	newNode->depth = depth;
	newNode->isLeaf = isLeaf;
	newNode->R = R;
	newNode->bVal = R + pow( gamma, depth ) / ( 1 - gamma );
	return newNode;
}


void findNodeWithMaxB( node* currNode )
{
    if ( currNode->isLeaf && currNode->bVal > maxBval )
    {//--set new maxBval
        maxBval = currNode->bVal;
        maxBnode = currNode;
	}
	if ( !currNode->isLeaf )
	{//--this node has children, explore them
	    for ( unsigned short i = 0; i < M; i++ )
        {//--dig deeper
            findNodeWithMaxB( currNode->children[i] );
        }
	}
	return;
}


void findMaxRleaves( node* currNode )//--, bool searchSpace - maybe this is also needed even here for uneligible leaves due to S>Smax
{
	if ( currNode->isLeaf && currNode->R > maxR )
	{
		maxR = currNode->R;
		maxRnode = currNode;
	}
	if ( !currNode->isLeaf )
	{//--if this is not a leaf node, dig deeper
		for ( unsigned short i = 0; i < M; i++ )
		{//--iterate through each child of a node
			findMaxRleaves( currNode->children[i] );
		}
	}
	return;
}



double* opd(stateE& s0)
{

	//--init first node of the tree, with parent -1, and 0 bValue
	//% initialize the root node (note 0 = null pointer)
	node* root = new node;
	root = initMyNode( s0, NULL, -1, 0, true, 0, 0 );

	//% 2) Main tree construction loop
	unsigned int tau = 0;

	while ( tau < n )
	{
		//% 2a) select candidate leaves for expansion
		maxBval = 0;				//--reset the maxBval to search for
		findNodeWithMaxB( root );	//--after running this, maxBnode contains the node from leaves with maxBval

		//% 2b) expand the chosen node
		node* parent = maxBnode;

        parent->isLeaf = false;		//% no longer a leaf

		//% run through the discrete actions
		for ( unsigned short j = 0; j < M; j++ )
		{
			//% simulate transition and add data to tree
			stateE newState = nextstateE( parent->X_and_r, U[j] );

			//% create new node
			node* child = new node;
			child->X_and_r = newState;
			child->parent = parent;
			child->u = j;
			child->depth = parent->depth + 1;
			child->isLeaf = true;
			child->R = parent->R + pow( gamma, parent->depth ) * child->X_and_r.reward;
			child->bVal = child->R + pow( gamma, child->depth ) / ( 1 - gamma );

			parent->children[j] = child;
		}

	

        if   (nsim != 0)
        {
            tau = tau + M;
        }
        else if ( n != 0)
        {
            tau = tau + 1;
        }

	}//% end main tree construction loop


	//--should check what has to be returned, to match SOOP
	//% 4) Find action at the tree root
	//% choose a leaf with maximal nu-value (i.e., truncated return)
	maxR = 0;
	findMaxRleaves( root );
	node* greedy = maxRnode;


	double* commandSequence = (double*)malloc(sizeof(double)*initlen);
	//% navigate greedy path upwards in the tree until level 1, just after root
	while ( greedy->parent != root )
	{
	    greedy = greedy->parent;
		if ( greedy->depth <= initlen )
		{//--back from depth=initlen start to construct commandSequence
			commandSequence[ greedy->depth - 1 ] = U[ greedy->u ];
		}
	}

//	delete node;		//--remove node structure and thus the entire tree from the memory

	return commandSequence;
}

#endif